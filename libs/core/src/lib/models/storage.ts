export interface Storage {
    clear(): void;
    getItem(key: string): string | null;
    removeItem(key: string): void;
    setItem(key: string, value: string, options?: any): void;
    [name: string]: any;
}