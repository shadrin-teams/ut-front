import { RequestResultInfo } from '@wt/models/http';

export interface HttpClientResult<T> {
  data: T;
  info: RequestResultInfo;
}
