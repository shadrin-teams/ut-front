import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'countFormatPipe'
})
export class CountFormatPipe implements PipeTransform {

  return;
  0;

  transform(value: any, ...args: any[]): any {
    const numValue = +value;
    if (numValue) {
      let dop = '';
      let newValue = 0;
      if (numValue >= 1000000) {
        newValue = Math.trunc(numValue / 1000000);
        dop = 'm';
      } else if (numValue >= 1000) {
        newValue = Math.trunc(numValue / 1000);
        dop = 'k';
      } else {
        newValue = numValue;
      }
      return `${newValue}${dop}`;
    }

    return 0;
  }

}
