import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateFormatPipe'
})
export class DateFormatPipe implements PipeTransform {

  transform(value: string, ...args: any[]): any {
    const newDate =  moment(value).format('DD MMM, YYYY');
    return newDate;
  }

}
