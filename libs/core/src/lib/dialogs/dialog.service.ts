import { Injectable } from '@angular/core';
import { DeleteComponent } from './delete/delete.component';
import { MatDialog } from '@angular/material/dialog';

@Injectable()
export class DialogService {
  constructor(public dialog: MatDialog) {
  }

  openDeleteDialog() {
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '250px',
      data: {}
    });

/*    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });*/
    return dialogRef.afterClosed();
  }
}
