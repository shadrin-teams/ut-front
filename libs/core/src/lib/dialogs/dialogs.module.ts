import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DialogService } from '@wt/core/dialogs/dialog.service';
import { DeleteComponent } from './delete/delete.component';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule
  ],
  declarations: [DeleteComponent],
  entryComponents: [DeleteComponent],
  providers: [DialogService]
})
export class DialogsModule {
}
