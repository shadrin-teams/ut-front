import { ModuleWithProviders, NgModule } from '@angular/core';
import { MetaReducer, StoreModule, USER_PROVIDED_META_REDUCERS } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// import { coreReducerProvider, coreReducerToken } from './+state/core.reducer';
import { coreFacades, effects } from './+state/index';
import { services } from './services';
import { StorageProvider, StorageService } from './services/storage/storage.service';

import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { initUserReducer } from '@wt/core/+state/users';
import { coreReducer } from '@wt/core/+state/core.reducer';

// import { environment } from '@wt/app/environments/environment';
// import { environment } from '@wt/panel-container/src/environments/environment';

@NgModule({
  imports: [
    StoreModule.forRoot(coreReducer),
    EffectsModule.forRoot([...effects]),
    StoreRouterConnectingModule.forRoot(),
    StoreDevtoolsModule.instrument({
      maxAge: 5,
      logOnly: true
    })
  ],
  providers: []
})
export class CoreModule {
  static forRoot(): ModuleWithProviders<any> {
    return {
      providers: [
        ...coreFacades,
        ...services,
        {
          provide: USER_PROVIDED_META_REDUCERS,
          deps: [StorageService],
          useFactory: getMetaReducers
        },
        StorageProvider
      ],
      ngModule: CoreModule
    };
  }
}

export function getMetaReducers(storageService: StorageService): MetaReducer<any>[] {
  return [(reducer) => initUserReducer(reducer, storageService)];
}
