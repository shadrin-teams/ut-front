import { Action } from '@ngrx/store';
import {
  combineLatest,
  forkJoin,
  Observable,
  of,
  OperatorFunction,
  race,
  throwError,
} from 'rxjs';
import { catchError, filter, switchMap, take } from 'rxjs/operators';

export interface CorrelationParams {
  correlationId: string;
  parentActionType?: string;
}

export type AggregatableAction = Action & { correlationParams?: CorrelationParams };


export type FailActionForAggregation = Action & { error?: any, correlationParams?: CorrelationParams };

export function aggregate(
  actions$: Observable<AggregatableAction>[],
  failAction$: Observable<FailActionForAggregation>): OperatorFunction<AggregatableAction, [AggregatableAction, AggregatableAction[]]> {

  const filterAction = (sourceAction: AggregatableAction, t: AggregatableAction) =>
    t.correlationParams && sourceAction.correlationParams &&
    t.correlationParams.correlationId === sourceAction.correlationParams.correlationId;

  const getAggregatedActions = (sourceAction: AggregatableAction): Observable<[AggregatableAction, AggregatableAction[]]> => {
    const arr = actions$.map((action$) => {
      return action$
        .pipe(
          take(1),
          filter(a => {
            return filterAction(sourceAction, a);
          })
        );
    })

    const f$ = failAction$
      .pipe(
        take(1),
        filter(a => {
          return filterAction(sourceAction, a);
        }),
        switchMap(b => {
          return throwError(b);
        })
      );

    const sourceAction$ = of(sourceAction);
    const responses$ = race(forkJoin(arr), f$.pipe(catchError(e => of([e]))));
    return combineLatest(sourceAction$, responses$);
  };

  return (source: Observable<AggregatableAction>) => source.pipe(
    switchMap(sourceAction => getAggregatedActions(sourceAction)),
  );
}
