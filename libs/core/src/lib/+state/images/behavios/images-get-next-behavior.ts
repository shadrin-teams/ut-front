import { Behavior } from '@wt/core/+state/behavior';
import { HttpClientResult } from '@wt/core/models';
import { ImagesGetNext200, ImagesGetNextResponse } from '@wt/models/endpoints/images';
import { ResponseStatus } from '@wt/models/http';
import { ImageModel } from '@wt/models/image';
import { CategoryModelData, ImageModelData } from '@wt/models/data';
import { GetImageNextFail, ImagesLoadSuccess, SetNextImages } from '@wt/core/+state/images/images.actions';
import { FilesLoadSuccess } from '@wt/core/+state/files';
import { CategoryModel } from '@wt/models/category';
import { CategoriesLoadSuccess } from '@wt/core/+state/categories/categories.actions';
import { FileModel } from '@wt/models/file';
import { Action } from '@ngrx/store';

export class ImagesGetNextBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<ImagesGetNextResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<Action>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: ImagesGetNext200 = this.result.data;
        const { data } = response.body;
        const { id } = this.action;

        const { prevImage, nextImage } = data;
        const images: ImageModel[] = [];
        let files: FileModel[] = [];
        const categories: CategoryModel[] = [];
        if (prevImage) {
          images.push(this.modelAdapter.convertItem<ImageModel, ImageModelData>(ImageModel, prevImage));
        }
        if (nextImage) {
          images.push(this.modelAdapter.convertItem<ImageModel, ImageModelData>(ImageModel, nextImage));
        }

        returnedActions = [
          SetNextImages({
            imageId: id,
            prevImageId: prevImage ? prevImage.id : 'null',
            nextImageId: nextImage ? nextImage.id : 'null'
          })
        ];

        if (images.length) {
          returnedActions.push(ImagesLoadSuccess({ images }));

          if (prevImage && prevImage.files) {
            files = [prevImage.files];
          }

          if (nextImage && nextImage.files) {
            files = [...files, nextImage.files];
          }

          if (files.length) {
            returnedActions.push(FilesLoadSuccess({ files }));
          }

          if (prevImage && prevImage.category) {
            categories.push(this.modelAdapter.convertItem<CategoryModel, CategoryModelData>(CategoryModel, prevImage.category));
          }

          if (nextImage && nextImage.category) {
            categories.push(this.modelAdapter.convertItem<CategoryModel, CategoryModelData>(CategoryModel, nextImage.category));
          }

          if (categories.length) {
            returnedActions.push(CategoriesLoadSuccess({ categories }));
          }
        }

        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          GetImageNextFail({ error })
        ];
        break;
      default: {
        returnedActions = [
          GetImageNextFail({ error: 'Unhandled error' })
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
