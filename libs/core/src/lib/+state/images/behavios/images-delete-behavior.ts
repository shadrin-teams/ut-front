import { Behavior } from '@wt/core/+state/behavior';
import { HttpClientResult } from '@wt/core/models';
import { ImagesDeleteFail, ImagesDeleteSuccess } from '@wt/core/+state/images/images.actions';
import { ImagesCreate200, ImagesCreateResponse } from '@wt/models/endpoints';
import { ResponseStatus } from '@wt/models/http';
import { ImageModel } from '@wt/models/image';
import { ImageModelData } from '@wt/models/data';
import { Action } from '@ngrx/store';

export class ImagesDeleteBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<ImagesCreateResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<Action>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: ImagesCreate200 = this.result.data;
        const { deleteId } = response.body.data;
        let imagesData: ImageModel[] = [];
        if (response.body.data.images) {
          const { data } = response.body.data.images;
          imagesData = data && data.length ? this.modelAdapter.convertItems<ImageModel, ImageModelData>(ImageModel, data) : [];
        }

        returnedActions = [
          ImagesDeleteSuccess({ images: imagesData, deleteId })
        ];

        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          ImagesDeleteFail({ error })
        ];
        break;
      default: {
        returnedActions = [
          ImagesDeleteFail({ error: 'Unhandled error' })
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
