import { Action, createReducer, on } from '@ngrx/store';

import * as imagesActions from './images.actions';
import { ImageModel } from '@wt/models/image';
import { ReducerService } from '@wt/core/services/reducer/reducer.service';

export const IMAGES_FEATURE_KEY = 'images';

export interface ImagesState {
  list: ImageModel[];
  ids: string[];
  selected: number;
  loaded: boolean; // has the Images list been loaded
  error?: any; // last none error (if any)
}

export interface ImagesStateData {
  images: ImagesState;
}

/*
export interface ImagesPartialState {
  readonly [IMAGES_FEATURE_KEY]: ImagesStateData;
}*/

export const initialImageState = {
  list: [],
  ids: [],
  selected: null,
  loaded: false
};

const newReducer = createReducer(
  initialImageState,
  on(imagesActions.ImagesLoad,
    imagesActions.ImageCreate, (state, payload) => {
      return {
        ...state,
        loaded: false
      };
    }),
  on(imagesActions.ImageCreateSuccess, (state, payload) => {
    return {
      ...ReducerService.upsertOne(state, payload.image),
      loaded: true
    };
  }),
  on(imagesActions.ImagesUpdateFail, (state, payload) => {
    return {
      ...state,
      error: payload && payload.error ? payload.error : '',
      loaded: true
    };
  }),
  on(imagesActions.ImagesLoadSuccess, (state, payload) => {
    return {
      ...ReducerService.upsertMeny(state, payload.images),
      loaded: true
    };
  }),
  on(imagesActions.ImagesDeleteSuccess, (state, payload) => {
    return {
      ...ReducerService.deleteOne(state, payload.deleteId),
      loaded: true
    };
  }),
  on(imagesActions.ImagesSetLoading, (state, payload) => {
    return {
      ...state,
      loaded: false
    };
  }),
  on(imagesActions.GetImage, (state, payload) => {
    return {
      ...state,
      selected: null
    };
  }),
  on(imagesActions.GetImageSuccess, (state, payload) => {
    const {image} = payload;
    const updatedState = ReducerService.upsertOne(state, image);
    return {
      ...state,
      ...updatedState,
      selected: image.id,
      loaded: true
    };
  }),
  on(imagesActions.ClearImages, (state, payload) => {
    return initialImageState;
  }),
  on(imagesActions.SetNextImages, (state, payload) => {
    const {imageId, prevImageId, nextImageId} = payload;
    const updateData = {prevImageId, nextImageId};
    return {
      ...state,
      ...ReducerService.upsertOne(state, updateData, imageId)
    };
  }),
);

export function imageReducer(state: ImagesState | undefined, action: Action) {
  return newReducer(state, action);
}

/*export function imagesReducer(
  state: ImagesState = initialImageState,
  action: ImagesAction
): ImagesState {
  switch (action.type) {
    case ImagesActionTypes.ImagesLoad:
    case ImagesActionTypes.ImageCreate: {
      state = {
        ...state,
        loaded: false
      };
      break;
    }

    case ImagesActionTypes.ImageCreateSuccess: {
      state = {
        ...ReducerService.upsertOne(state, action.payload.image),
        loaded: true
      };
      break;
    }
    case ImagesActionTypes.ImagesUpdateFail: {
      state = {
        ...state,
        error: action && action.error ? action.error.data : '',
        loaded: true
      };
      break;
    }
    case ImagesActionTypes.ImagesLoadSuccess: {
      state = {
        ...ReducerService.upsertMeny(state, action.payload.images),
        loaded: true
      };
      break;
    }
    case ImagesActionTypes.ImagesDeleteSuccess: {
      state = {
        ...ReducerService.deleteOne(state, action.payload.deleteId),
        loaded: true
      };
      break;
    }
    case ImagesActionTypes.ImagesSetLoading: {
      state = {
        ...state,
        loaded: false
      };
      break;
    }
    case ImagesActionTypes.GetImage: {
      state = {
        ...state,
        selected: null
      };
      break;
    }
    case ImagesActionTypes.GetImageSuccess: {
      const {image} = action.payload;
      const updatedState = ReducerService.upsertOne(state, image);
      state = {
        ...state,
        ...updatedState,
        selected: image.id,
        loaded: true
      };
      break;
    }
    case ImagesActionTypes.ClearImages: {
      state = {...state, list: [], ids: [], loaded: false};
      break;
    }
    case ImagesActionTypes.SetNextImages: {
      const {imageId, prevImageId, nextImageId} = action.payload;
      const updateData = {prevImageId, nextImageId};
      state = {
        ...state,
        ...ReducerService.upsertOne(state, updateData, imageId)
      };
      break;
    }
  }
  return state;
}*/
