import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, flatMap, map, mapTo, mergeMap, switchMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { HttpClientResult, MetaDataModel } from '@wt/core/models';
import { CoreState } from '@wt/core/+state/core.reducer';
import { LoginSendCreateResponse } from '@wt/models/endpoints/auth/login-send';
import { ImagesCreateBehavior } from '@wt/core/+state/images/behavios/images-create-behavior';
import { ImagesService } from '@wt/core/services/images/images.service';
import {
  ImagesCreateRequestConfig,
  ImagesDeleteRequestConfig,
  ImagesGetNextRequestConfig,
  ImagesGetNextResponse,
  ImagesGetRequestConfig,
  ImagesGetResponse,
  ImagesUpdateRequestConfig,
  ImagesUpdateResponse
} from '@wt/models/endpoints';
import { ImagesLoadBehavior } from '@wt/core/+state/images/behavios/images-load-behavior';
import { getUIUnit } from '@wt/core/+state/uiUnits';
import { UiUnitIds } from '@wt/models/ui-units';
import { AuthenticationService } from '@wt/core/services/auth/authentication.service';
import { GetImageBehavior } from '@wt/core/+state/images/behavios/images-get-behavior';
import { ImagesUpdateBehavior } from '@wt/core/+state/images/behavios/images-update-behavior';
import { ImagesGetNextBehavior } from '@wt/core/+state/images/behavios/images-get-next-behavior';
import * as imagesActions from './images.actions';
import {GetImageSuccess} from './images.actions';
import { ImagesLoadFail, ImagesLoadSuccess, ImagesLoadSuccessPayload } from './images.actions';
import { BaseEffects } from '@wt/core/+state/base.effects';
import { of } from 'rxjs';
import { UpdateMetaData } from '@wt/core/+state/main/main.actions';
import { NotificationAdd } from '@wt/core/+state/notifications/notifications.actions';
import { NotificationModel } from '@wt/models/notification';

@Injectable()
export class ImagesEffects extends BaseEffects {

  @Effect()
  loginSend$ = this.actions$.pipe(
    ofType(imagesActions.ImageCreate),
    switchMap((action) => {
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      const config: ImagesCreateRequestConfig = {
        ...action,
        headers: { token: headers.token, uid: headers.id }
      };
      return this.imagesService.imagesCreate(config)
        .pipe(
          flatMap((r: HttpClientResult<LoginSendCreateResponse>) => new ImagesCreateBehavior(action, r).resolve())
        )
    })
  );

  @Effect()
  imagesLoad$ = this.actions$.pipe(
    ofType(imagesActions.ImagesLoad),
    switchMap((action) => {
      const { where, fields, paging, sort, tagId, forPanel } = action;
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      return this.imagesService.imagesLoad({
        where,
        fields,
        paging,
        sort,
        tagId,
        forPanel,
        headers: { token: headers.token, uid: headers.id }
      })
        .pipe(
          flatMap((r: HttpClientResult<LoginSendCreateResponse>) => new ImagesLoadBehavior(action, r).resolve())
        )
    })
  );

  @Effect()
  imageLoadSuccess$ = this.actions$.pipe(
    ofType(imagesActions.GetImageSuccess),
    mergeMap((action) => {
      const { metaDescription, metaTitle, metaKeyWord } = action.image;
      const payload: MetaDataModel = {
        title: metaTitle,
        description: metaDescription,
        keyWords: metaKeyWord
      };
      return of(UpdateMetaData(payload))
    }),
    catchError((error) => {
      console.log('error', error);
      return of(UpdateMetaData(error));
    })
  );

  @Effect()
  imagesDelete$ = this.actions$.pipe(
    ofType(imagesActions.ImagesDelete),
    switchMap((action) => {
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      const itemId = action.itemId;
      const fields = {
        fields: []
      };

      return this.applySelector(getUIUnit(UiUnitIds.IMAGES))
        .pipe(
          switchMap((UIUnits) => {
            const config: ImagesDeleteRequestConfig = {
              headers: { token: headers.token, uid: headers.id },
              id: itemId,
              fields,
              paging: {
                perPage: UIUnits && UIUnits.perPage ? UIUnits.perPage : '',
                page: UIUnits && UIUnits.page ? UIUnits.page : ''
              }
            };

            return this.imagesService.imagesDelete(config);
          }),
          flatMap((r: HttpClientResult<LoginSendCreateResponse>) => new ImagesLoadBehavior(action, r).resolve())
        )
    })
  );

  @Effect()
  imagesDeleteSuccess$ = this.actions$.pipe(
    ofType(imagesActions.ImagesDeleteSuccess),
    mergeMap((action) => {
      const payload: ImagesLoadSuccessPayload = {
        images: action.images
      };
      return of(ImagesLoadSuccess(payload));
    }),
    catchError((error) => {
      return of(ImagesLoadFail(error));
    })
  );

  @Effect()
  getImage$ = this.actions$.pipe(
    ofType(imagesActions.GetImage),
    switchMap((action) => {
      const { fields, forPanel } = action;
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      const itemId = action.id;
      const config: ImagesGetRequestConfig = {
        headers: { token: headers.token, uid: headers.id },
        id: itemId,
        fields,
        forPanel
      };
      return this.imagesService.getImage(config)
        .pipe(
          flatMap((r: HttpClientResult<ImagesGetResponse>) => new GetImageBehavior(action, r).resolve())
        )
    })
  );

  @Effect()
  getImageNext$ = this.actions$.pipe(
    ofType(imagesActions.GetImageNext),
    switchMap((action) => {
      const { fields } = action;
      const itemId = action.id;
      const config: ImagesGetNextRequestConfig = {
        id: itemId,
        fields
      };
      return this.imagesService.getImageNext(config)
        .pipe(
          flatMap((r: HttpClientResult<ImagesGetNextResponse>) => new ImagesGetNextBehavior(action, r).resolve())
        )
    })
  );

  @Effect()
  imagesUpdate2$ = this.actions$.pipe(
    ofType(imagesActions.ImagesUpdate),
    switchMap((action) => {
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      const config: ImagesUpdateRequestConfig = {
        ...action,
        headers: { token: headers.token, uid: headers.id }
      };
      return this.imagesService.imagesUpdate(config)
        .pipe(
          flatMap((r: HttpClientResult<ImagesUpdateResponse>) => {
            return new ImagesUpdateBehavior(action, r).resolve();
          })
        )
    })
  );

  @Effect()
  getImageFail$ = this.actions$.pipe(
    ofType(imagesActions.ImagesUpdateFail),
    mergeMap((action) => {
      const payload: NotificationModel = {
        id: 'image-update',
        type: 'error',
        text: action && action.error ? action.error.data : '',
        title: 'Ошибка сохранения картинки'
      };
      return of(NotificationAdd({ payload }));
    })
  );

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected imagesService: ImagesService,
    protected authenticationService: AuthenticationService
  ) {
    super(actions$, store);
  }
}
