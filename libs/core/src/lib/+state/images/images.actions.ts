import { createAction, props } from '@ngrx/store';
import { ImageModel } from '@wt/models/image';
import { PagingType } from '@wt/models/entity';
import { MetaModel } from '@wt/models/others';

export interface GetImagePayload {
  id: number | string;
  fields?: any;
}

export const InitImagesPageUI = createAction(
  '[Images] Init Images Page UI',
  props<GetImagePayload>()
);

export const InitImagesPageUIFail = createAction(
  '[Images] Init Images Page UI Error',
  props<{ error }>()
);


export interface ImageCreatePayment extends ImageModel {
  forPanel?: boolean
}

export const ImageCreate = createAction(
  '[Images] Image Create',
  props<ImageCreatePayment>()
);

export interface ImageCreateSuccessPayload {
  image: ImageModel;
}

export const ImageCreateSuccess = createAction(
  '[Images] Image Create Success',
  props<ImageCreateSuccessPayload>()
);

export const ImageCreateFail = createAction(
  '[Images] Image Create Error',
  props<{ error }>()
);


export interface ImagesLoadPayload {
  where: { id?: number };
  fields?: any;
  paging?: PagingType;
  sort?: string;
  imagesType?: 'popular' | '';
  tagId?: number;
  forPanel?: boolean;
}

export const ImagesLoad = createAction(
  '[Images] Images Load',
  props<ImagesLoadPayload>()
);

export interface ImagesLoadSuccessPayload {
  images: ImageModel[];
  meta?: MetaModel;
}

export const ImagesLoadSuccess = createAction(
  '[Images] Images Load Success',
  props<ImagesLoadSuccessPayload>()
);

export const ImagesLoadFail = createAction(
  '[Images] Images Load Error',
  props<{ error }>()
);


export interface ImagesDeletePayload {
  itemId: number;
}

export const ImagesDelete = createAction(
  '[Images] Images Delete',
  props<ImagesDeletePayload>()
);

export interface ImagesLoadSuccessPayloadPayload {
  images: ImageModel[];
  deleteId: number;
  meta?: MetaModel;
}

export const ImagesDeleteSuccess = createAction(
  '[Images] Images Delete Success',
  props<ImagesLoadSuccessPayloadPayload>()
);

export const ImagesDeleteFail = createAction(
  '[Images] Images Delete Error',
  props<{ error }>()
);


export const ImagesUpdate = createAction(
  '[Images] Images Update',
  props<ImageCreatePayment>()
);

export const ImagesUpdateSuccess = createAction(
  '[Images] Images Update Success'
);

export const ImagesUpdateFail = createAction(
  '[Images] Images Update Error',
  props<{ error }>()
);


export const ImagesSetLoading = createAction(
  '[Images] Images Set Loading'
);

export interface GetImagePayload {
  id: number | string;
  fields?: any;
  forPanel?: boolean;
}

export const GetImage = createAction(
  '[Images] Get Image',
  props<GetImagePayload>()
);

export interface GetImageSuccessPayload {
  image: ImageModel;
}

export const GetImageSuccess = createAction(
  '[Images] Get Image Success',
  props<GetImageSuccessPayload>()
);

export const GetImageFail = createAction(
  '[Images] Get Image Error',
  props<{ error }>()
);

export interface GetImagePayload {
  id: number | string;
  fields?: any;
}

export const GetImageNext = createAction(
  '[Images] Get Image Next',
  props<GetImagePayload>()
);

export interface GetImageNextSuccessPayload {
  images: ImageModel[];
}

export const GetImageNextSuccess = createAction(
  '[Images] Get Image Next Success',
  props<GetImageNextSuccessPayload>()
);

export const GetImageNextFail = createAction(
  '[Images] Get Image Next Error',
  props<{ error }>()
);


export interface SetNextImagesPayment {
  imageId: string | number;
  prevImageId: number;
  nextImageId: number;
}

export const SetNextImages = createAction(
  '[Images] Set Next Images Success',
  props<SetNextImagesPayment>()
);


export const ClearImages = createAction(
  '[Images] Clear Images'
);


/*
import {
  AggregatableAction,
  CorrelationParams,
  FailActionForAggregation
} from '@wt/core/+state/aggregate';
import { ImageModel } from '@wt/models/image';
import { MetaModel } from '@wt/models/others';
import { PagingType } from '@wt/models/entity';

export enum ImagesActionTypes {
  InitImagesPageUI = '[Images] Init Images Page UI',
  InitImagesPageUIFail = '[Images] Init Images Page UI Error',

  ImageCreate = '[Images] Image Create',
  ImageCreateSuccess = '[Images] Image Create Success',
  ImageCreateFail = '[Images] Image Create Error',

  ImagesLoad = '[Images] Images Load',
  ImagesLoadSuccess = '[Images] Images Load Success',
  ImagesLoadFail = '[Images] Images Load Error',

  ImagesDelete = '[Images] Images Delete',
  ImagesDeleteSuccess = '[Images] Images Delete Success',
  ImagesDeleteFail = '[Images] Images Delete Error',

  ImagesUpdate = '[Images] Images Update',
  ImagesUpdateSuccess = '[Images] Images Update Success',
  ImagesUpdateFail = '[Images] Images Update Error',

  ImagesSetLoading = '[Images] Images Set Loading',

  GetImage = '[Images] Get Image',
  GetImageSuccess = '[Images] Get Image Success',
  GetImageFail = '[Images] Get Image Error',

  GetImageNext = '[Images] Get Image Next',
  GetImageNextSuccess = '[Images] Get Image Next Success',
  GetImageNextFail = '[Images] Get Image Next Error',

  SetNextImages = '[Images] Set Next Images Success',

  ClearImages = '[Images] Clear Images'
}

export interface ImageCreatePayment extends ImageModel {
  forPanel?: boolean
}

export class ImageCreate implements AggregatableAction {
  readonly type = ImagesActionTypes.ImageCreate;

  constructor(public payload: ImageCreatePayment, public correlationParams: CorrelationParams) {
  }
}

export interface ImageCreateSuccessPayload {
  image: ImageModel;
}

export interface EmptyPayload {
}

export class ImageCreateSuccess implements AggregatableAction {
  readonly type = ImagesActionTypes.ImageCreateSuccess;

  constructor(public payload: ImageCreateSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class ImageCreateFail implements FailActionForAggregation {
  readonly type = ImagesActionTypes.ImageCreateFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export interface ImagesLoadPayload {
  where: { id?: number };
  fields?: any;
  paging?: PagingType;
  sort?: string;
  imagesType?: 'popular' | '';
  tagId?: number;
  forPanel?: boolean;
}

export class ImagesLoad implements AggregatableAction {
  readonly type = ImagesActionTypes.ImagesLoad;

  constructor(public payload: ImagesLoadPayload, public correlationParams: CorrelationParams) {
  }
}

export interface ImagesLoadSuccessPayload {
  images: ImageModel[];
  meta?: MetaModel;
}

export interface EmptyPayload {
}

export class ImagesLoadSuccess implements AggregatableAction {
  readonly type = ImagesActionTypes.ImagesLoadSuccess;

  constructor(public payload: ImagesLoadSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class ImagesLoadFail implements FailActionForAggregation {
  readonly type = ImagesActionTypes.ImagesLoadFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export interface ImagesDeletePayload {
  itemId: number;
}

export class ImagesDelete implements AggregatableAction {
  readonly type = ImagesActionTypes.ImagesDelete;

  constructor(public payload: ImagesDeletePayload, public correlationParams: CorrelationParams) {
  }
}

export interface ImagesLoadSuccessPayloadPayload {
  images: ImageModel[];
  deleteId: number;
  meta?: MetaModel;
}

export class ImagesDeleteSuccess implements AggregatableAction {
  readonly type = ImagesActionTypes.ImagesDeleteSuccess;

  constructor(public payload: ImagesLoadSuccessPayloadPayload, public correlationParams: CorrelationParams) {
  }
}

export class ImagesDeleteFail implements FailActionForAggregation {
  readonly type = ImagesActionTypes.ImagesDeleteFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export class ImagesUpdate implements AggregatableAction {
  readonly type = ImagesActionTypes.ImagesUpdate;

  constructor(public payload: ImageCreatePayment, public correlationParams: CorrelationParams) {
  }
}

export interface ImagesUpdateSuccessPayload {
  // image: ImageModel
}

export interface EmptyPayload {
}

export class ImagesUpdateSuccess implements AggregatableAction {
  readonly type = ImagesActionTypes.ImagesUpdateSuccess;

  constructor(public payload: ImagesUpdateSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class ImagesUpdateFail implements FailActionForAggregation {
  readonly type = ImagesActionTypes.ImagesUpdateFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export class ImagesSetLoading implements AggregatableAction {
  readonly type = ImagesActionTypes.ImagesSetLoading;

  constructor(public payload: EmptyPayload, public correlationParams: CorrelationParams) {
  }
}

export interface EmptyPayload {

}

export interface GetImagePayload {
  id: number | string;
  fields?: any;
  forPanel?: boolean;
}

export class GetImage implements AggregatableAction {
  readonly type = ImagesActionTypes.GetImage;

  constructor(public payload: GetImagePayload, public correlationParams: CorrelationParams) {
  }
}

export interface GetImageSuccessPayload {
  image: ImageModel;
}

export class GetImageSuccess implements AggregatableAction {
  readonly type = ImagesActionTypes.GetImageSuccess;

  constructor(public payload: GetImageSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class GetImageFail implements FailActionForAggregation {
  readonly type = ImagesActionTypes.GetImageFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export interface GetImagePayload {
  id: number | string;
  fields?: any;
}

export class GetImageNext implements AggregatableAction {
  readonly type = ImagesActionTypes.GetImageNext;

  constructor(public payload: GetImagePayload, public correlationParams: CorrelationParams) {
  }
}

export interface GetImageNextSuccessPayload {
  images: ImageModel[];
}

export class GetImageNextSuccess implements AggregatableAction {
  readonly type = ImagesActionTypes.GetImageNextSuccess;

  constructor(public payload: GetImageNextSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class GetImageNextFail implements FailActionForAggregation {
  readonly type = ImagesActionTypes.GetImageNextFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export class InitImagesPageUI implements AggregatableAction {
  readonly type = ImagesActionTypes.InitImagesPageUI;

  constructor(public payload: GetImagePayload, public correlationParams: CorrelationParams) {
  }
}

export class InitImagesPageUIFail implements FailActionForAggregation {
  readonly type = ImagesActionTypes.InitImagesPageUIFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export interface SetNextImagesPayment {
  imageId: string | number;
  prevImageId: number;
  nextImageId: number;
}

export class SetNextImages implements AggregatableAction {
  readonly type = ImagesActionTypes.SetNextImages;

  constructor(public payload: SetNextImagesPayment, public correlationParams: CorrelationParams) {
  }
}

export interface ClearImagesPayload {
}

export class ClearImages implements AggregatableAction {
  readonly type = ImagesActionTypes.ClearImages;

  constructor(public payload: ClearImagesPayload, public correlationParams: CorrelationParams) {
  }
}

export type ImagesAction =
  | ImageCreate
  | ImageCreateSuccess
  | ImageCreateFail
  | ImagesLoad
  | ImagesLoadSuccess
  | ImagesLoadFail
  | ImagesDelete
  | ImagesDeleteSuccess
  | ImagesDeleteFail
  | ImagesUpdate
  | ImagesUpdateSuccess
  | ImagesUpdateFail
  | ImagesSetLoading
  | GetImage
  | GetImageSuccess
  | GetImageFail
  | GetImageNext
  | GetImageNextSuccess
  | GetImageNextFail
  | InitImagesPageUI
  | InitImagesPageUIFail
  | SetNextImages
  | ClearImages;
*/
