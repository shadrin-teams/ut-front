import { Location } from '@angular/common';
import { Injectable } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, tap } from 'rxjs/operators';

import { BaseEffects } from '../base.effects';
import { CoreState } from '../core.reducer';
import * as routerActions from './router.actions';
import { GoFail, GoPayload, SetRouterQueryParamsPayload } from './router.actions';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class RouterEffects extends BaseEffects {

  @Effect({ dispatch: false })
  go$ = this.actions$.pipe(
    ofType(routerActions.Go),
    tap((action) => {
      const payload: GoPayload = action;
      const path = this.preparePath(payload.path);
      let extras: NavigationExtras;
      if (payload.extras) {
        extras = payload.extras;
      }

      this.router.navigate(path, extras)
        .then(data => {
          if (!data) {
            console.log('router.navigate - error', {path, extras});
          }
        });
    }),
    catchError(error => {
      return of(GoFail(error));
    })
  );

  @Effect({ dispatch: false })
  setRouterQueryParams$ = this.actions$.pipe(
    ofType(routerActions.SetRouterQueryParams),
    tap((action) => {
      const payload: SetRouterQueryParamsPayload = action;
      const extras: NavigationExtras = payload.extras;
      const path = [this.location.path().split('?')[0]];
      this.router.navigate(path, extras);
    })
  );

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    private router: Router,
    private location: Location
  ) {
    super(actions$, store);
  }

  private preparePath(params: any[]): any[] {
    const path = [];
    let i = 0;
    while (params.length > i) {
      const p = params[i];
      if (typeof p === 'object') {
        for (const key in p) {
          if (p.hasOwnProperty(key)) {
            path.push(p[key]);
          }
        }
      } else {
        path.push(p);
      }
      i++;
    }
    return path;
  }
}
