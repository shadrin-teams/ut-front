import { createAction, props } from '@ngrx/store';
import { NavigationExtras } from '@angular/router';

export interface GoPayload {
  path: any[];
  extras?: NavigationExtras
}

export const Go = createAction(
  '[Router] Add',
  props<GoPayload>()
);

export interface SetRouterQueryParamsPayload {
  extras: NavigationExtras
}

export const SetRouterQueryParams = createAction(
  '[Router] Add',
  props<SetRouterQueryParamsPayload>()
);

export const GoFail = createAction(
  '[Router] Add',
  props<{ error }>()
);

export const Back = createAction(
  '[Router] Add'
);

export const Forward = createAction(
  '[Router] Add'
);

/*
import { NavigationExtras } from '@angular/router';
import { Action } from '@ngrx/store';

import {
  AggregatableAction,
  CorrelationParams,
  FailActionForAggregation,
} from '../aggregate';

export enum RouterActionTypes {
  Go = '[Router] Go',
  SetRouterQueryParams = '[Router] Set Router Query Params',
  GoFail = '[Router] Go Fail',
  Back = '[Router] Back',
  Forward = '[Router] Forward',
}

export interface GoPayload {
  path: any[];
  extras?: NavigationExtras
}

export class Go implements AggregatableAction {
  readonly type = RouterActionTypes.Go;
  constructor(public payload: GoPayload, public correlationParams?: CorrelationParams) { }
}

export interface SetRouterQueryParamsPayload {
  extras: NavigationExtras
}

export class SetRouterQueryParams implements AggregatableAction {
  readonly type = RouterActionTypes.SetRouterQueryParams
  constructor(public payload: SetRouterQueryParamsPayload, public correlationParams: CorrelationParams) { }
}


export interface GoFailPayload {
}

export class GoFail implements FailActionForAggregation {
  readonly type = RouterActionTypes.GoFail;
  constructor(public payload: GoFailPayload, public error: any, public correlationParams: CorrelationParams) { }
}

export class Back implements Action {
  readonly type = RouterActionTypes.Back;
}

export class Forward implements Action {
  readonly type = RouterActionTypes.Forward;
}

export type RouterActions = Go | Back | Forward;
*/
