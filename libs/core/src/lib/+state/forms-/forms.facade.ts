/*
import { Injectable } from '@angular/core';
import {
  CreateForm,
  RemoveForm,
  SendAction,
  SendActionPayload,
  UpdateForm
} from '@wt/core/+state/forms/forms.actions';
import { CoreState } from '@wt/core/+state/core.reducer';
import { select, Store } from '@ngrx/store';
import { guid } from '@wt/core/utils/guid';
import { getFormData } from '@wt/core/+state/forms/forms.selectors';

@Injectable()
export class FormsFacade {
  constructor(private store: Store<CoreState>) {
  }

  createForm(formId: string, initialFormData: any) {
    this.store.dispatch(
      new CreateForm(
        {formId, initialFormData},
        {
          correlationId: guid()
        }
      )
    );
  }

  getForm(formId: string) {
    return this.store.pipe(select(getFormData(formId)));
  }

  updateForm(params) {
    this.store.dispatch(UpdateForm(params));
  }

  sendAction(sendActionParams: SendActionPayload) {
    this.store.dispatch(SendAction(sendActionParams));
  }

  removeForm(formId: string) {
    this.store.dispatch(
      new RemoveForm(
        {formId},
        {
          correlationId: guid()
        }
      )
    );
  }
}
*/
