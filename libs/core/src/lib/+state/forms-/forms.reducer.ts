/*

import { emptyNumberValidate, emptyTextValidate } from '@wt/core/validations';
import { ImageFormValue } from '@wt/core/models';
import { Action } from '@ngrx/store';
import { UiUnitIds } from '@wt/models/ui-units';
import { required } from 'ngrx-forms/validation';

export interface FormValue {
}

function initialForm(formId, initialValue) {
  return createFormGroupState<FormValue>(formId, initialValue);
}

export interface FormsStateData {
  forms: EntityState<FormGroupState<FormValue>>;
}

export const formsAdapter: EntityAdapter<FormGroupState<FormValue>> = createEntityAdapter<FormGroupState<FormValue>>();

export const initialFormsState = formsAdapter.getInitialState();

const imageForm: StateUpdateFns<ImageFormValue> = {
  title: validate(required, emptyTextValidate),
  description: validate(required, emptyTextValidate),
  categoryId: validate(required, emptyNumberValidate),
  tagIds: validate(required, emptyTextValidate)
};

export function ngrxFromsReducerWithUpdate(
  state: EntityState<FormGroupState<FormValue>>,
  action: Action,
  name: string,
  validationRules
) {
  const formGroup = state.entities[name];
  if (formGroup) {
    let formState;
    if (action.type === SetErrorsAction.TYPE || action.type === SetAsyncErrorAction.TYPE) {
      formState = formGroupReducer(formGroup, action);
    } else {
      formState = updateGroup<FormValue>(validationRules)(formGroupReducer(formGroup, action));
    }
    return formsAdapter.updateOne({id: name, changes: formState}, {...state});
  }
  return state;
}

export function updateValue(state, formId, field, value) {
  let newState;
  const action = {
    controlId: `${formId}.${field}`,
    value: value,
    type: 'ngrx/forms/SET_VALUE'
  };
  newState = ngrxFromsReducerWithUpdate(state, action, UiUnitIds.IMAGE_FORM, imageForm);

  const dirtyAction = {
    controlId: `${formId}.${field}`,
    type: 'ngrx/forms/MARK_AS_DIRTY'
  };
  newState = ngrxFromsReducerWithUpdate(newState, dirtyAction, UiUnitIds.IMAGE_FORM, imageForm);

  const touchedAction = {
    controlId: `${formId}.${field}`,
    type: 'ngrx/forms/MARK_AS_TOUCHED'
  };

  return ngrxFromsReducerWithUpdate(newState, touchedAction, UiUnitIds.IMAGE_FORM, imageForm);
}

export function updateValues(state, formId, values) {
  let newState = {...state};
  values.forEach(item => {
    newState = updateValue(newState, formId, item.field, item.value);
  });
  return newState;
}

function sendAction(state, payload) {
  const {field, action, formId} = payload;
  let newState = state;
  const controlId = `${formId}.${field}`;

  const newAction = {
    controlId,
    type: `ngrx/forms/${action}`
  };
  newState = ngrxFromsReducerWithUpdate(newState, newAction, formId, imageForm);

  if (action === 'MARK_AS_TOUCHED') {
    const newAction = {
      controlId,
      type: `ngrx/forms/MARK_AS_DIRTY`
    };
    newState = ngrxFromsReducerWithUpdate(newState, newAction, formId, imageForm);
  }

  if (action === 'MARK_AS_DIRTY') {
    const newAction = {
      controlId,
      type: `ngrx/forms/MARK_AS_TOUCHED`
    };
    newState = ngrxFromsReducerWithUpdate(newState, newAction, formId, imageForm);
  }

  /!* @TODO: Hack, need to search best variants *!/
  if ('controls' in newState.entities[formId].controls[field]) {
    newState.entities[formId].controls[field].isDirty = true;
    newState.entities[formId].controls[field].isPristine = false;
    newState.entities[formId].controls[field].isTouched = true;
  }

  return newState;
}

export function formsReducer(
  state: EntityState<FormGroupState<FormValue>> = initialFormsState,
  action: FormsActions
): EntityState<FormGroupState<FormValue>> {
  if (action.type.indexOf('ngrx/forms') === 0) {
    state = ngrxFromsReducerWithUpdate(state, action, UiUnitIds.IMAGE_FORM, imageForm);
  }

  switch (action.type) {
    case FormsActionTypes.CreateForm:
      const {formId, initialFormData} = action.payload;
      const form = initialForm(formId, initialFormData);
      const myForm = formGroupReducer(form, action);

      return formsAdapter.upsertOne(myForm, state);
    case FormsActionTypes.UpdateForm:
      const {field, value, values} = action.payload;

      if (values) {
        return updateValues({...state}, action.payload.formId, values);
      }

      return updateValue({...state}, action.payload.formId, field, value);
    case FormsActionTypes.SendAction:
      return sendAction(state, action.payload);
    case FormsActionTypes.RemoveForm: {
      const {formId} = action.payload;
      return formsAdapter.removeOne(formId, state);
    }
    default: {
      return state;
    }
  }
}
*/
