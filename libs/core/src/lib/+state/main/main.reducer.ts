import { MetaDataModel } from '@wt/core/models';
import * as mainActions from './main.actions';
import { Action, createReducer, on } from '@ngrx/store';

export const MAIN_FEATURE_KEY = 'main';

export interface MainState {
  metaData: MetaDataModel
}

export const initialMainState = {
  metaData: null
};

export interface MainStateData {
  main: MainState;
}


const newReducer = createReducer(
  initialMainState,
  on(mainActions.UpdateMetaData, (state, payload) => {
    const metaData: MetaDataModel = payload
    return {
      ...state,
      metaData
    };
  }),
  on(mainActions.UpdateMetaDataSuccess, (state, payload) => {
    const metaData: MetaDataModel = payload
    return {
      ...state,
      metaData
    };
  })
);

export function mainReducer(state: MainState | undefined, action: Action) {
  return newReducer(state, action);
}

