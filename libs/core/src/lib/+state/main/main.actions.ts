import { createAction, props } from '@ngrx/store';
import { MetaDataModel } from '@wt/core/models';

export const UpdateMetaData = createAction(
  '[MetaData] Update MetaData',
  props<MetaDataModel>()
);

export const UpdateMetaDataSuccess = createAction(
  '[MetaData] Update MetaData Success',
  props<MetaDataModel>()
);

export const UpdateMetaDataFail = createAction(
  '[MetaData] Update MetaData Error',
  props<{ error }>()
);

