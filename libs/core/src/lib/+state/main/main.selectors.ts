import { createFeatureSelector, createSelector } from '@ngrx/store';

import { MAIN_FEATURE_KEY, MainState } from './main.reducer';

const getState = createFeatureSelector<MainState>(MAIN_FEATURE_KEY);

const getMetaData = createSelector(
  getState,
  (state: MainState) => {
    return state.metaData;
  }
);

export const mainQuery = {
  getMetaData
};
