import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { mainQuery } from './main.selectors';
import { MainStateData } from '@wt/core/+state/main/main.reducer';
import { Observable } from 'rxjs';
import { UpdateMetaDataSuccess } from '@wt/core/+state/main/main.actions';
import { MetaDataModel } from '@wt/core/models';

@Injectable()
export class MainFacade {
  constructor(private store: Store<MainStateData>) {
  }

  getMetadata(): Observable<any> {
    return this.store.pipe(select(mainQuery.getMetaData));
  }

  setMetadata(data: MetaDataModel): void {
    this.store.dispatch(UpdateMetaDataSuccess(data));
  }
}
