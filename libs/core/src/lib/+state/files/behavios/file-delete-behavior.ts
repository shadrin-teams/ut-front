import { Behavior } from '@wt/core/+state/behavior';
import { HttpClientResult } from '@wt/core/models';
import { FileDeleteFail, FileDeleteSuccess } from '@wt/core/+state/files/files.actions';
import { FilesDelete200, FilesDeleteResponse } from '@wt/models/endpoints';
import { ResponseStatus } from '@wt/models/http';
import { Action } from '@ngrx/store';

export class FileDeleteBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<FilesDeleteResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    let returnedActions: Array<Action>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: FilesDelete200 = this.result.data;
        const { data } = response.body;

        returnedActions = [
          FileDeleteSuccess({ deleteId: data.delete_id })
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          FileDeleteFail({ error })
        ];
        break;
      default: {
        returnedActions = [
          FileDeleteFail({ error: 'Unhandled error' })
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
