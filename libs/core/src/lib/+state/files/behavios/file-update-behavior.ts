import { Behavior } from '@wt/core/+state/behavior';
import { HttpClientResult } from '@wt/core/models';
import { FileUpdateFail, FileUpdateSuccess } from '@wt/core/+state/files/files.actions';
import { FilesUpdate200, FilesUpdateResponse } from '@wt/models/endpoints';
import { ResponseStatus } from '@wt/models/http';
import { FileModel } from '@wt/models/file';
import { FileModelData } from '@wt/models/data';
import { Action } from '@ngrx/store';

export class FileUpdateBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<FilesUpdateResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    let returnedActions: Array<Action>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: FilesUpdate200 = this.result.data;
        const { data } = response.body;
        const fileData: FileModel[] = this.modelAdapter.convertItems<FileModel, FileModelData>(FileModel, data);
        returnedActions = [
          FileUpdateSuccess({ files: fileData })
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          FileUpdateFail( { error })
        ];
        break;
      default: {
        returnedActions = [
          FileUpdateFail({ error: 'Unhandled error' })
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
