import { Behavior } from '@wt/core/+state/behavior';
import { HttpClientResult } from '@wt/core/models';
import { SetLastRequestResult } from '@wt/core/+state/last-request-result';
import { FilesLoad, FilesLoadFail, FilesLoadSuccess } from '@wt/core/+state/files/files.actions';
import { FilesLoad200, FilesLoadResponse } from '@wt/models/endpoints';
import { ResponseStatus } from '@wt/models/http';
import { FileModel } from '@wt/models/file';
import { FileModelData } from '@wt/models/data';
import { Action } from '@ngrx/store';

export class FilesLoadBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<FilesLoadResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<Action>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: FilesLoad200 = this.result.data;
        const { data } = response.body;
        const fileData: FileModel[] = this.modelAdapter.convertItems<FileModel, FileModelData>(FileModel, data);
        returnedActions = [
          FilesLoadSuccess({ files: fileData })
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors ? errorResponse.body.errors[0].title : 'Unknown error';
        returnedActions = [
          FilesLoadFail({ error })
        ];
        break;
      default: {
        returnedActions = [
          FilesLoadFail({ error: 'Unhandled error' })
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
