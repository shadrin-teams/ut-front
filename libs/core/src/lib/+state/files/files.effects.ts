import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { flatMap, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { HttpClientResult } from '@wt/core/models';
import { CoreState } from '@wt/core/+state/core.reducer';
import { FilesService } from '@wt/core/services/files/files.service';
import {
  FilesDeleteRequestConfig,
  FilesDeleteResponse,
  FilesLoadRequestConfig,
  FilesLoadResponse,
  FilesSortRequestConfig,
  FilesUpdateRequestConfig,
  FilesUpdateResponse
} from '@wt/models/endpoints';
import { FileDeleteBehavior } from '@wt/core/+state/files/behavios/file-delete-behavior';
import { UiUnitsFacade } from '@wt/core/+state/uiUnits';
import { AuthenticationService } from '@wt/core/services/auth/authentication.service';
import { FileUpdateBehavior } from '@wt/core/+state/files/behavios/file-update-behavior';
import { NotificationsFacade } from '@wt/core/+state/notifications';
import { FilesSortBehavior } from '@wt/core/+state/files/behavios/files-sort-behavior';
import { FilesLoadBehavior } from '@wt/core/+state/files/behavios/files-load-behavior';
import * as filesActions from './files.actions';

@Injectable()
export class FilesEffects {

  @Effect()
  fileDelete$ = this.actions$.pipe(
    ofType(filesActions.FileDelete),
    switchMap((action) => {
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      const id = action.id;
      const config: FilesDeleteRequestConfig = {
        headers: { token: headers.token, uid: headers.id },
        id
      };
      return this.filesService.fileDelete(config)
        .pipe(
          flatMap((r: HttpClientResult<FilesDeleteResponse>) => new FileDeleteBehavior(action, r).resolve())
        )
    })
  );

  @Effect()
  fileUpdate2$ = this.actions$.pipe(
    ofType(filesActions.FileUpdate),
    switchMap((action) => {
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      const { id, main, sort, title, section, itemId } = action;
      const config: FilesUpdateRequestConfig = {
        id,
        headers: { token: headers.token, uid: headers.id },
        section,
        itemId
      };
      if (main) config['main'] = main;
      if (sort) config['sort'] = sort;
      if (title) config['title'] = title;

      return this.filesService.fileUpdate(config)
        .pipe(
          flatMap((r: HttpClientResult<FilesUpdateResponse>) => new FileUpdateBehavior(action, r).resolve())
        )
    })
  );

  @Effect()
  filesSort$ = this.actions$.pipe(
    ofType(filesActions.FilesSort),
    switchMap((action) => {
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      const { files } = action;
      const config: FilesSortRequestConfig = {
        headers: { token: headers.token, uid: headers.id },
        files
      };

      return this.filesService.filesSort(config)
        .pipe(
          flatMap((r: HttpClientResult<FilesUpdateResponse>) => new FilesSortBehavior(action, r).resolve())
        )
    })
  );

  @Effect()
  filesLoad$ = this.actions$.pipe(
    ofType(filesActions.FilesLoad),
    switchMap((action) => {
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      const { section, itemId, fields } = action;
      const config: FilesLoadRequestConfig = {
        headers: { token: headers.token, uid: headers.id },
        section,
        itemId,
        fields
      };
      return this.filesService.filesLoad(config)
        .pipe(
          flatMap((r: HttpClientResult<FilesLoadResponse>) => new FilesLoadBehavior(action, r).resolve())
        )
    })
  );

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected filesService: FilesService,
    protected uiUnitsFacade: UiUnitsFacade,
    protected notificationsFacade: NotificationsFacade,
    protected authenticationService: AuthenticationService
  ) {
  }
}
