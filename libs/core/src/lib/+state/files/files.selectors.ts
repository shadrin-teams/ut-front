import { createFeatureSelector, createSelector } from '@ngrx/store';

import { FILES_FEATURE_KEY, FilesState } from './files.reducer';
import { FileModel } from '@wt/models/file';

// Lookup the 'Files' feature state managed by NgRx
const getFilesState = createFeatureSelector<FilesState>(FILES_FEATURE_KEY);

const getLoading = createSelector(
  getFilesState,
  (state: FilesState) => {
    return state.loading;
  }
);

const getError = createSelector(
  getFilesState,
  (state: FilesState) => state.error
);

const geSelected = createSelector(
  getFilesState,
  (state: FilesState) => state.selected
);

const getFiles = createSelector(
  getFilesState,
  getLoading,
  (state: FilesState, isLoading) => {
    return state.list ? state.list : [];
  }
);

const getFiles2 = createSelector(
  getFilesState,
  getLoading,
  (state: FilesState, isLoading) => {
    return !isLoading ? state.list : null;
  }
);

/*const getFilesWithoutLoaded = createSelector(
  getFilesState,
  getLoading,
  (state: FilesState, isLoading) => {
    console.log('getFiles2', state.list);
    return state ? state.list : [];
  }
);*/

const getFile = (id: number) => createSelector(
  getFiles,
  (list: FileModel[]) => {
    if (list && list.length) {
      return list.find((item) => item.id === id);
    }
    return {};
  }
);

const getFilteredFiles = (itemId: number, section: string) => createSelector(
  getFiles2,
  (list: FileModel[]) => {
    if (list && list.length) {
      return list.filter((item) => item.itemId === itemId && item.section === section );
    }
    return null;
  }
);
/*
const getFilesState = createSelector(
  getFilesState,
  getLoading,
  (state: FilesState, isLoading) => {
    console.log('state', state);
    return isLoading ? state : null;
  }
);*/

export const filesQuery = {
  getLoading,
  getError,
  getFiles,
  geSelected,
  getFile,
  getFilteredFiles
};
