import { createAction, props } from '@ngrx/store';
import { FieldsModel } from '@wt/models/fields';
import { FileModel } from '@wt/models/file';

export interface FilesLoadPayload {
  section: string;
  itemId: number;
  fields?: FieldsModel;
}
export const FilesLoad = createAction(
  '[Files] File Load',
  props<FilesLoadPayload>()
);

export interface FilesLoadSuccessPayload {
  files: FileModel[];
}
export const FilesLoadSuccess = createAction(
  '[Files] File Load Success',
  props<FilesLoadSuccessPayload>()
);

export const FilesLoadFail = createAction(
  '[Files] File Load Error',
  props<{ error }>()
);

export interface FileUpdatePayload {
  id: number;
  sort?: number;
  title?: string;
  main?: boolean;
  section: string;
  itemId: number;
}

export const FileUpdate = createAction(
  '[Files] File Update',
  props<FileUpdatePayload>()
);

export interface FileUpdateSuccessPayload {
  files: FileModel[];
}

export const FileUpdateSuccess = createAction(
  '[Files] File Update Success',
  props<FileUpdateSuccessPayload>()
);

export const FileUpdateFail = createAction(
  '[Files] File Update Error',
  props<{ error }>()
);

export interface FileDeletePayload {
  id: number;
}

export const FileDelete = createAction(
  '[Files] File Delete',
  props<FileDeletePayload>()
);

export interface FileDeleteSuccessPayload {
  deleteId: number;
}

export const FileDeleteSuccess = createAction(
  '[Files] File Delete Success',
  props<FileDeleteSuccessPayload>()
);

export const FileDeleteFail = createAction(
  '[Files] File Delete Error',
  props<{ error }>()
);

export interface FilesSortPayload {
  files: { id: number, order: number }[];
}

export const FilesSort = createAction(
  '[Files] File Sort',
  props<FilesSortPayload>()
);

export interface FilesSortSuccessPayload {
  files: FileModel[];
}

export const FilesSortSuccess = createAction(
  '[Files] File Sort Success',
  props<FilesSortSuccessPayload>()
);
export const FilesSortFail = createAction(
  '[Files] File Sort Error',
  props<{ error }>()
);

/*
import { AggregatableAction, CorrelationParams, FailActionForAggregation } from '@wt/core/+state/aggregate';
import { FileModel } from '@wt/models/file';
import {FieldsModel} from '@wt/models/fields';

export enum FilesActionTypes {
  FilesLoad = '[Files] File Load',
  FilesLoadSuccess = '[Files] File Load Success',
  FilesLoadFail = '[Files] File Load Error',

  FileUpdate = '[Files] File Update',
  FileUpdateSuccess = '[Files] File Update Success',
  FileUpdateFail = '[Files] File Update Error',

  FileDelete = '[Files] File Delete',
  FileDeleteSuccess = '[Files] File Delete Success',
  FileDeleteFail = '[Files] File Delete Error',

  FilesSort = '[Files] File Sort',
  FilesSortSuccess = '[Files] File Sort Success',
  FilesSortFail = '[Files] File Sort Error'
}

export interface FilesLoadPayload {
  section: string;
  itemId: number;
  fields?: FieldsModel;
}

export class FilesLoad implements AggregatableAction {
  readonly type = FilesActionTypes.FilesLoad;

  constructor(public payload: FilesLoadPayload, public correlationParams: CorrelationParams) {
  }
}

export interface FilesLoadSuccessPayload {
  files: FileModel[];
}

export interface EmptyPayload {
}

export class FilesLoadSuccess implements AggregatableAction {
  readonly type = FilesActionTypes.FilesLoadSuccess;

  constructor(public payload: FilesLoadSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class FilesLoadFail implements FailActionForAggregation {
  readonly type = FilesActionTypes.FilesLoadFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export interface FileUpdatePayload {
  id: number;
  sort?: number;
  title?: string;
  main?: boolean;
  section: string;
  itemId: number;
}

export class FileUpdate implements AggregatableAction {
  readonly type = FilesActionTypes.FileUpdate;

  constructor(public payload: FileUpdatePayload, public correlationParams: CorrelationParams) {
  }
}

export interface FileUpdateSuccessPayload {
  files: FileModel[];
}

export interface EmptyPayload {
}

export class FileUpdateSuccess implements AggregatableAction {
  readonly type = FilesActionTypes.FileUpdateSuccess;

  constructor(public payload: FileUpdateSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class FileUpdateFail implements FailActionForAggregation {
  readonly type = FilesActionTypes.FileUpdateFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export interface FileDeletePayload {
  id: number;
}

export class FileDelete implements AggregatableAction {
  readonly type = FilesActionTypes.FileDelete;

  constructor(public payload: FileDeletePayload, public correlationParams: CorrelationParams) {
  }
}

export interface FileDeleteSuccessPayload {
  deleteId: number;
}

export interface EmptyPayload {
}

export class FileDeleteSuccess implements AggregatableAction {
  readonly type = FilesActionTypes.FileDeleteSuccess;

  constructor(public payload: FileDeleteSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class FileDeleteFail implements FailActionForAggregation {
  readonly type = FilesActionTypes.FileDeleteFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export interface FilesSortPayload {
  files: { id: number, order: number }[];
}

export class FilesSort implements AggregatableAction {
  readonly type = FilesActionTypes.FilesSort;

  constructor(public payload: FilesSortPayload, public correlationParams: CorrelationParams) {
  }
}

export interface FilesSortSuccessPayload {
  files: FileModel[];
}

export interface EmptyPayload {
}

export class FilesSortSuccess implements AggregatableAction {
  readonly type = FilesActionTypes.FilesSortSuccess;

  constructor(public payload: FilesSortSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class FilesSortFail implements FailActionForAggregation {
  readonly type = FilesActionTypes.FilesSortFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export type FilesAction =
  | FilesLoad
  | FilesLoadSuccess
  | FilesLoadFail
  | FileUpdate
  | FileUpdateSuccess
  | FileUpdateFail
  | FileDelete
  | FileDeleteSuccess
  | FileDeleteFail
  | FilesSort
  | FilesSortSuccess
  | FilesSortFail;
*/
