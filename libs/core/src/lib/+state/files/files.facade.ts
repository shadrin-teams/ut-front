import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';

import { guid } from '@wt/core/utils';
import { FileDelete, FilesLoad, FilesSort, FileUpdate, FileUpdatePayload } from '@wt/core/+state/files/files.actions';
import { FilesStateData } from '@wt/core/+state/files/files.reducer';
import { filesQuery } from '@wt/core/+state/files/files.selectors';

@Injectable()
export class FilesFacade {
  loading$ = this.store.pipe(select(filesQuery.getLoading));
  getFiles$ = this.store.pipe(select(filesQuery.getFiles));
  selected$ = this.store.pipe(select(filesQuery.geSelected));

  constructor(private store: Store<FilesStateData>) {
  }

  waitLoading() {
    return this.store.pipe(
      select(filesQuery.getLoading),
      filter((item) => !!item)
    );
  }

  filesDelete(itemId: number) {
    const corelParams = { correlationId: guid() };
    this.store.dispatch(FileDelete({ id: itemId }));
  }

  filesUpdate(data: { title?: string, sort?: number, main?: boolean, id: number, section: string, itemId: number }) {
    const params: FileUpdatePayload = { ...data };
    this.store.dispatch(FileUpdate(params));
  }

  getFiles(data: { itemId?: number, section?: string }) {
    return this.store.pipe(select(filesQuery.getFilteredFiles(data.itemId, data.section)));
  }

  /*    getFilesOrLoad(data: { itemId: number, section: string }) {
          return this.store.pipe(
              select(filesQuery.getLoading),
              tap((files: FileModel[]) => {
                  if (!files || !files.loaded) {
                      this.loadFiles(data);
                  }
                  console.log('files', files);
              })
          );
      }*/

  filesSort(files: { id: number, order: number }[]) {
    const corelParams = { correlationId: guid() };
    this.store.dispatch(FilesSort({ files }));
  }

  loadFiles(data: { itemId: number, section: string }) {
    const corelParams = { correlationId: guid() };
    const params = {
      ...data,
      fields: {
        fields: [],
        versions: {}
      }
    };
    return this.store.dispatch(FilesLoad(params));
  }
}
