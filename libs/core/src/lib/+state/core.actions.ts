import { createAction, props } from '@ngrx/store';
import { RefreshTokenPayment } from '@wt/core/+state/users/users.actions';

export const LoadCore = createAction(
  '[Core] Load Core',
  props<RefreshTokenPayment>()
);

export const CoreLoaded = createAction(
  '[Core] Core Loaded',
  props<RefreshTokenPayment>()
);

export const CoreLoadError = createAction(
  '[Core] Core Load Error',
  props<RefreshTokenPayment>()
);


/*
import { Action } from '@ngrx/store';
import { Entity } from './core.reducer';

export enum CoreActionTypes {
  LoadCore = '[Core] Load Core',
  CoreLoaded = '[Core] Core Loaded',
  CoreLoadError = '[Core] Core Load Error'
}

export class LoadCore implements Action {
  readonly type = CoreActionTypes.LoadCore;
}

export class CoreLoadError implements Action {
  readonly type = CoreActionTypes.CoreLoadError;
  constructor(public payload: any) {}
}

export class CoreLoaded implements Action {
  readonly type = CoreActionTypes.CoreLoaded;
  constructor(public payload: Entity[]) {}
}

export type CoreAction = LoadCore | CoreLoaded | CoreLoadError;

export const fromCoreActions = {
  LoadCore,
  CoreLoaded,
  CoreLoadError
};
*/
