import { CoreEffects } from './core.effects';
import { UsersEffects } from './users/users.effects';
import { UsersFacade } from './users';
import { RouterEffects } from './router';
import { ImagesEffects } from '@wt/core/+state/images/images.effects';
import { ImagesFacade } from '@wt/core/+state/images/images.facade';
import { UiUnitsFacade } from '@wt/core/+state/uiUnits';
import { NotificationsEffects } from '@wt/core/+state/notifications/notifications.effects';
import { NotificationsFacade } from '@wt/core/+state/notifications';
import { RouterFacade } from '@wt/core/+state/router/router.facade';
import { FilesEffects, FilesFacade } from '@wt/core/+state/files';
import { UiUnitsEffects } from '@wt/core/+state/uiUnits/uiUnits.effects';
import { CategoriesEffects } from '@wt/core/+state/categories/categories.effects';
import { HashtagsEffects } from '@wt/core/+state/hashtags/hashtags.effects';
import { CategoriesFacade } from '@wt/core/+state/categories';
import { HashtagsFacade } from '@wt/core/+state/hashtags';
import { MainEffects } from '@wt/core/+state/main/main.effects';
import { MainFacade } from '@wt/core/+state/main';

/*
export * from './users';
export * from './tours';
*/

export const effects = [
  CoreEffects,
  UsersEffects,
  RouterEffects,
  ImagesEffects,
  NotificationsEffects,
  FilesEffects,
  UiUnitsEffects,
  CategoriesEffects,
  HashtagsEffects,
  MainEffects
];

export const coreFacades = [
  UsersFacade,
  ImagesFacade,
  UiUnitsFacade,
  NotificationsFacade,
  RouterFacade,
  FilesFacade,
  CategoriesFacade,
  HashtagsFacade,
  MainFacade
];
