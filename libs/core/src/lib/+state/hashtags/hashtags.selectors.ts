import { createFeatureSelector, createSelector } from '@ngrx/store';

import { HASHTAGS_FEATURE_KEY, HashtagsState } from './hashtags.reducer';
import { HashtagModel } from '@wt/models/hashtag/hashtagModel';

// Lookup the 'Hashtags' feature state managed by NgRx
export const getHashtagsState = createFeatureSelector<HashtagsState>(HASHTAGS_FEATURE_KEY);

const getLoaded = createSelector(
  getHashtagsState,
  (state: HashtagsState) => {
    return state.loaded;
  }
);

const getError = createSelector(
  getHashtagsState,
  (state: HashtagsState) => state.error
);

const geSelected = createSelector(
  getHashtagsState,
  (state: HashtagsState) => state.selected
);

const getHashtags = createSelector(
  getHashtagsState,
  getLoaded,
  (state: HashtagsState, isLoaded) => {
    return isLoaded ? state.list : null;
  }
);

const getHashtag = (slugOrId: number | string) => createSelector(
  getHashtags,
  (list: HashtagModel[]) => {
    if (list && list.length) {
      return list.find((item) => item.id === slugOrId || item.slug === slugOrId);
    }
  }
);

const getHashtagList = (slugOrIds: Array<number | string>) => createSelector(
  getHashtags,
  (list: HashtagModel[]) => {
    if (list && list.length) {
      return list.filter((item) => slugOrIds.includes(item.id) || slugOrIds.includes(item.slug));
    }
  }
);
/*
const getHashtagsState = createSelector(
  getHashtagsState,
  getLoaded,
  (state: HashtagsState, isLoaded) => {
    console.log('state', state);
    return isLoaded ? state : null;
  }
);*/

export const hashtagsQuery = {
  getLoaded,
  getError,
  getHashtags,
  geSelected,
  getHashtag,
  getHashtagList
};
