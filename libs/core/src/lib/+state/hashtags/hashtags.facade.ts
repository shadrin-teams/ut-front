import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { HashtagsLoad } from '@wt/core/+state/hashtags/hashtags.actions';
import { HashtagsStateData } from '@wt/core/+state/hashtags/hashtags.reducer';
import { hashtagsQuery } from '@wt/core/+state/hashtags/hashtags.selectors';
import { Observable } from 'rxjs';
import { HashtagModel } from '@wt/models/hashtag';

@Injectable()
export class HashtagsFacade {
  loaded$ = this.store.pipe(select(hashtagsQuery.getLoaded));
  getHashtags$ = this.store.pipe(select(hashtagsQuery.getHashtags));
  selected$ = this.store.pipe(select(hashtagsQuery.geSelected));

  constructor(private store: Store<HashtagsStateData>) {
  }

  loadHashtags(where = {}, fields = { fields: ['name', 'slug'] }, perPage?: number) {
    this.store.dispatch(HashtagsLoad({ where, fields, perPage }));
  }

  getHashtags(slugOrId: string | number): Observable<HashtagModel> {
    return this.store.pipe(select(hashtagsQuery.getHashtag(slugOrId)));
  }

  getHashtagsList(slugOrId: Array<string | number>): Observable<HashtagModel[]> {
    return this.store.pipe(select(hashtagsQuery.getHashtagList(slugOrId)));
  }

  waitLoading() {
    return this.store.pipe(
      select(hashtagsQuery.getLoaded),
      filter((item) => !!item)
    );
  }
}
