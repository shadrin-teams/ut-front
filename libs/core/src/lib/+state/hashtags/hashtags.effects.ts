import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { flatMap, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { HttpClientResult } from '@wt/core/models';
import { CoreState } from '@wt/core/+state/core.reducer';
import { LoginSendCreateResponse } from '@wt/models/endpoints/auth/login-send';
import { HashtagsService } from '@wt/core/services/hashtags/hashtags.service';
import { HashtagsLoadBehavior } from '@wt/core/+state/hashtags/behavios/hashtags-load-behavior';
import { AuthenticationService } from '@wt/core/services/auth/authentication.service';
import * as hashtagsActions from './hashtags.actions';

@Injectable()
export class HashtagsEffects {

  @Effect()
  hashtagsLoad$ = this.actions$.pipe(
    ofType(hashtagsActions.HashtagsLoad),
    switchMap((action) => {
      const { where, fields, perPage } = action;
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      return this.hashtagsService.hashtagsLoad({
        where,
        fields,
        perPage,
        headers: { token: headers.token, uid: headers.id }
      })
        .pipe(
          flatMap((r: HttpClientResult<LoginSendCreateResponse>) => new HashtagsLoadBehavior(action, r).resolve())
        )
    })
  );

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected hashtagsService: HashtagsService,
    protected authenticationService: AuthenticationService
  ) {
  }
}
