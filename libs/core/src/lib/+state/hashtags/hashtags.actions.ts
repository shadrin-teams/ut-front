import { createAction, props } from '@ngrx/store';
import { MetaModel } from '@wt/models/others';
import { HashtagModel } from '@wt/models/hashtag';

export interface HashtagsLoadPayload {
  where: { id?: number };
  fields?: any;
  perPage?: number;
}
export const HashtagsLoad = createAction(
  '[Hashtags] Hashtags Load',
  props<HashtagsLoadPayload>()
);

export interface HashtagsLoadSuccessPayload {
  hashtags: HashtagModel[];
  meta?: MetaModel;
}
export const HashtagsLoadSuccess = createAction(
  '[Hashtags] Hashtags Load Success',
  props<HashtagsLoadSuccessPayload>()
);

export const HashtagsLoadFail = createAction(
  '[Hashtags] Hashtags Load Error',
  props<{ error }>()
);

export const HashtagsSetLoading = createAction(
  '[Hashtags] Hashtags Set Loading'
);


/*
import {
  AggregatableAction,
  CorrelationParams,
  FailActionForAggregation
} from '@wt/core/+state/aggregate';
import {MetaModel} from '@wt/models/others';
import {HashtagModel} from '@wt/models/hashtag/hashtagModel';

export enum HashtagsActionTypes {
  HashtagsLoad = '[Hashtags] Hashtags Load',
  HashtagsLoadSuccess = '[Hashtags] Hashtags Load Success',
  HashtagsLoadFail = '[Hashtags] Hashtags Load Error',

  HashtagsSetLoading = '[Hashtags] Hashtags Set Loading'
}

export interface HashtagsLoadPayload {
  where: { id?: number };
  fields?: any;
  perPage?: number;
}

export class HashtagsLoad implements AggregatableAction {
  readonly type = HashtagsActionTypes.HashtagsLoad;

  constructor(public payload: HashtagsLoadPayload, public correlationParams: CorrelationParams) {
  }
}

export interface HashtagsLoadSuccessPayload {
  hashtags: HashtagModel[];
  meta?: MetaModel;
}

export interface EmptyPayload {
}

export class HashtagsLoadSuccess implements AggregatableAction {
  readonly type = HashtagsActionTypes.HashtagsLoadSuccess;

  constructor(public payload: HashtagsLoadSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class HashtagsLoadFail implements FailActionForAggregation {
  readonly type = HashtagsActionTypes.HashtagsLoadFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export class HashtagsSetLoading implements AggregatableAction {
  readonly type = HashtagsActionTypes.HashtagsSetLoading;

  constructor(public payload: EmptyPayload, public correlationParams: CorrelationParams) {
  }
}

export type HashtagsAction =
  | HashtagsLoad
  | HashtagsLoadSuccess
  | HashtagsLoadFail
  | HashtagsSetLoading;
*/
