import { ReducerService } from '@wt/core/services/reducer/reducer.service';
import { HashtagModel } from '@wt/models/hashtag';
import { Action, createReducer, on } from '@ngrx/store';
import * as hashtagsActions from './hashtags.actions';

export const HASHTAGS_FEATURE_KEY = 'hashtags';

export interface HashtagsState {
  list: HashtagModel[];
  ids: string[];
  selected: number;
  loaded: boolean; // has the Hashtags list been loaded
  error?: any; // last none error (if any)
}

export interface HashtagsStateData {
  [HASHTAGS_FEATURE_KEY]: HashtagsState;
}

/*
export interface HashtagsPartialState {
  readonly [CATEGORIES_FEATURE_KEY]: HashtagsStateData;
}*/

export const initialHashtagState = {
  list: [],
  ids: [],
  selected: null,
  loaded: false
};

const newReducer = createReducer(
  initialHashtagState,
  on(hashtagsActions.HashtagsLoad, state => ({ ...state, loaded: false })),
  on(hashtagsActions.HashtagsLoadSuccess, (state, payload) => ({
    ...ReducerService.upsertMeny(state, payload.hashtags),
    loaded: true
  }))
);

export function hashTagsReducer(state: HashtagsState | undefined, action: Action) {
  return newReducer(state, action);
}

/*
export function hashtagsReducer(
  state: HashtagsState = initialHashtagState,
  action: HashtagsAction
): HashtagsState {
  switch (action.type) {
    case HashtagsActionTypes.HashtagsLoad: {
      state = {
        ...state,
        loaded: false
      };
      break;
    }

    case HashtagsActionTypes.HashtagsLoadSuccess: {
      state = {
        ...ReducerService.upsertMeny(state, action.payload.hashtags),
        loaded: true
      };
      break;
    }
  }
  return state;
}
*/
