import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { CoreState } from '@wt/core/+state/core.reducer';
import { BaseEffects } from '@wt/core/+state/base.effects';
import {
  ImageCreate,
  ImageCreateFail,
  ImageCreateSuccess,
  ImagesLoadFail,
  ImagesLoadSuccess,
  ImagesUpdate,
  ImagesUpdateFail,
  ImagesUpdateSuccess
} from '@wt/core/+state/images/images.actions';
import { UiUnitsFacade } from '@wt/core/+state/uiUnits';
import { UiUnitIds, UiUnitsImagesData } from '@wt/models/ui-units';
import { of } from 'rxjs';

@Injectable()
export class UiUnitsEffects extends BaseEffects {

  @Effect({ dispatch: false })
  go$ = this.actions$.pipe(
    ofType(ImagesLoadSuccess),
    tap((action) => {
      this.uiUnitsFacade.setUIUnits(UiUnitIds.IMAGES, action.meta);
    }),
    catchError(error => {
      return of(ImagesLoadFail(error));
    })
  );

  @Effect({ dispatch: false })
  imagesUpdate$ = this.actions$.pipe(
    ofType(ImagesUpdate),
    tap(() => {
      const data: UiUnitsImagesData = {
        updating: true,
        lastCreatedId: null
      };
      this.uiUnitsFacade.setUIUnits(UiUnitIds.IMAGES, data);
    }),
    catchError(error => {
      return of(ImagesUpdateFail(error));
    })
  );

  @Effect({ dispatch: false })
  imagesUpdateSuccess$ = this.actions$.pipe(
    ofType(ImagesUpdateSuccess),
    tap(() => {
      this.uiUnitsFacade.setUIUnits(UiUnitIds.IMAGES, { updating: false });
    }),
    catchError(error => {
      return of(ImagesUpdateFail(error));
    })
  );

  @Effect({ dispatch: false })
  imagesUpdateFail$ = this.actions$.pipe(
    ofType(ImagesUpdateFail),
    tap((action) => {
      this.uiUnitsFacade.setUIUnits(UiUnitIds.IMAGES, {
        updating: false,
        error: action && action.error ? action.error.data : ''
      });
    }),
    catchError(error => {
      return of(ImagesUpdateFail(error));
    })
  );

  @Effect({ dispatch: false })
  imageCreate$ = this.actions$.pipe(
    ofType(ImageCreate),
    tap(() => {
      const data: UiUnitsImagesData = {
        updating: true,
        lastCreatedId: null
      };
      this.uiUnitsFacade.setUIUnits(UiUnitIds.IMAGES, data);
    }),
    catchError(error => {
      return of(ImageCreateFail(error));
    })
  );

  @Effect({ dispatch: false })
  imageCreateSuccess$ = this.actions$.pipe(
    ofType(ImageCreateSuccess),
    tap((action) => {
      const { id } = action.image;
      const data: UiUnitsImagesData = {
        updating: false,
        lastCreatedId: id
      };
      this.uiUnitsFacade.setUIUnits(UiUnitIds.IMAGES, data);
    })
  );

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected uiUnitsFacade: UiUnitsFacade
  ) {
    super(actions$, store);
  }
}
