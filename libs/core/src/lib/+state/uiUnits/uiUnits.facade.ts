import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { UiUnitsSet } from '@wt/core/+state/uiUnits/uiUnits.actions';
import { getUIUnit } from '@wt/core/+state/uiUnits/uiUnits.selectors';
import { UiUnitsStateData } from '@wt/core/+state/uiUnits/uiUnits.reducer';

@Injectable()
export class UiUnitsFacade {

  constructor(private store: Store<UiUnitsStateData>) {
  }

  setUIUnits(id: string, data: any) {
    this.store.dispatch(UiUnitsSet({ id, data }));
  }

  getUIUnits(id: string) {
    return this.store.pipe(select(getUIUnit(id)));
  }

}
