import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { UsersStateData } from './users.reducer';
import { usersQuery } from './users.selectors';
import { LoadUsers, LoginSend, LoginSendPayment, RefreshToken, SetLoaded } from './users.actions';
import { guid } from '@wt/core/utils';

@Injectable()
export class UsersFacade {
  loaded$ = this.store.pipe(select(usersQuery.getLoaded));
  getUser$ = this.store.pipe(select(usersQuery.getUser));

  constructor(private store: Store<UsersStateData>) {
  }

  loadAll() {
    const params = { correlationId: guid() };
    this.store.dispatch(LoadUsers());
  }

  sendLogin({ email, password, remember }) {
    const params: LoginSendPayment = {
      email, password, remember
    };

    this.store.dispatch(LoginSend(params));
  }

  logOut() {
    console.log('logOut');
  }

  refreshToken(refreshToken: string) {
    console.log('refreshToken');
    this.store.dispatch(RefreshToken({ refreshToken }));
  }

  setLoaded() {
    this.store.dispatch(SetLoaded());
  }
}
