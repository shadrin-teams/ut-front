import * as usersActions from './users.actions';
import { Action, ActionReducer, createReducer, on } from '@ngrx/store';

import { StorageService } from '../../services/storage/storage.service';
import { UserModel } from '@wt/models/user';

export const USERS_FEATURE_KEY = 'user';

export interface UsersState extends UserModel {
  loaded: boolean; // has the Users list been loaded
  error?: any; // last none error (if any)
}

export interface UsersStateData {
  user: UsersState
}
/*
export interface UsersPartialState {
  readonly [USERS_FEATURE_KEY]: UsersStateData;
}*/

export const initialUserState = {
  loaded: false,
  error: null
};

const newReducer = createReducer(
  initialUserState,
  on(usersActions.UsersLoaded, (state, payload) => {
    return {
      ...state,
      ...payload,
      loaded: true
    };
  }),
  on(usersActions.RefreshToken,
    usersActions.SetLoaded, (state, payload) => {
      return {
        ...state,
        loaded: false
      };
    }),
  on(usersActions.RefreshTokenSuccess,(state, payload) => {
      const { token, refreshToken, tokenExp } = payload;
      return {
        ...state,
        // data: { ...state.data, token, refreshToken, tokenExp },
        token,
        refreshToken,
        tokenExp,
        loaded: true
      };
    })
);

export function userReducer(state: UsersState | undefined, action: Action) {
  return newReducer(state, action);
}


/*export function usersReducer(
  state: UsersState = initialUserState,
  action: UsersAction
): UsersState {
  switch (action.type) {
    case UsersActionTypes.UsersLoaded: {
      state = {
        ...state,
        ...action.payload,
        loaded: true
      };
      break;
    }
    case UsersActionTypes.RefreshToken:
    case UsersActionTypes.SetLoaded: {
      console.log('RefreshToken');
      state = {
        ...state,
        loaded: false
      };
      break;
    }
    case UsersActionTypes.RefreshTokenSuccess: {
      console.log('RefreshTokenSuccess');
      const { token, refreshToken, tokenExp } = action.payload;
      state = {
        ...state,
        // data: { ...state.data, token, refreshToken, tokenExp },
        token,
        refreshToken,
        tokenExp,
        loaded: true
      };
      break;
    }
  }
  return state;
}*/

export function initUserReducer(
  reducer: ActionReducer<any>,
  storage: StorageService
): ActionReducer<any> {
  return function (state, action) {
    return reducer(state, action);
  };
}
