import { Behavior } from '@wt/core/+state/behavior';
import { HttpClientResult } from '@wt/core/models';
import { RefreshTokenFail, RefreshTokenSuccess } from '@wt/core/+state/users/users.actions';
import { ResponseStatus } from '@wt/models/http';
import { UserModel } from '@wt/models/user';
import { UserModelData } from '@wt/models/data';
import { RefreshToken200, RefreshTokenResponse } from '@wt/models/endpoints/auth/refresh-token';
import { Action } from '@ngrx/store';

export class RefreshTokenBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<RefreshTokenResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<Action>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: RefreshToken200 = this.result.data;
        const { user } = response.body;
        const userData: UserModel = this.modelAdapter.convertItem<UserModel, UserModelData>(UserModel, user);
        returnedActions = [
          RefreshTokenSuccess({
            token: userData.token,
            refreshToken: userData.refreshToken,
            tokenExp: userData.tokenExp
          })
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          RefreshTokenFail({ error })
        ];
        break;
      default: {
        returnedActions = [
          RefreshTokenFail({ error: 'Unhandled error' })
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
