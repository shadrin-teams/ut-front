import { Behavior } from '@wt/core/+state/behavior';
import { HttpClientResult } from '@wt/core/models';
import { LoginSendFail, LoginSendSuccess } from '@wt/core/+state/users/users.actions';
import { LoginSendCreate201, LoginSendCreateResponse } from '@wt/models/endpoints/auth/login-send';
import { ResponseStatus } from '@wt/models/http';
import { UserModel } from '@wt/models/user';
import { UserModelData } from '@wt/models/data';
import { Action } from '@ngrx/store';

export class LoginSendBehavior extends Behavior {
  constructor(
    protected action: any,
    protected result: HttpClientResult<LoginSendCreateResponse>
  ) {
    super(action);
  }

  resolve(): Array<Action> {
    const status = this.result.data.status;
    const correlationParams = this.action.correlationParams;
    let returnedActions: Array<Action>;
    const setLastRequestResponseAction = this.generateSetLastRequestResult(
      this.action.type,
      this.result.info,
      this.action.correlationParams
    );
    switch (status) {
      case ResponseStatus.STATUS_200: {
        const response: LoginSendCreate201 = this.result.data;
        const { user } = response.body;
        const userData: UserModel = this.modelAdapter.convertItem<UserModel, UserModelData>(UserModel, user);
        returnedActions = [
          LoginSendSuccess({ user: userData })
        ];
        break;
      }
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_401:
        const errorResponse = this.result.data;
        const error = errorResponse.body.errors[0].title;
        returnedActions = [
          LoginSendFail({ error })
        ];
        break;
      default: {
        returnedActions = [
          LoginSendFail({ error: 'Unhandled error' })
        ];
      }

    }
    return [setLastRequestResponseAction, ...returnedActions];
  }
}
