import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { NotificationAdd } from './notifications.actions';
import { NotificationModel } from '@wt/models/notification';
import { NotificationsStateData } from '@wt/core/+state/notifications/notifications.reducer';

@Injectable()
export class NotificationsFacade {

  constructor(private store: Store<NotificationsStateData>) {
  }

  addNotifications(params: NotificationModel) {
    this.store.dispatch(NotificationAdd({ payload: params }));
  }
}
