import { createFeatureSelector, createSelector } from '@ngrx/store';

import { NOTIFICATIONS_FEATURE_KEY, NotificationsState } from './notifications.reducer';

const getNotificationsState = createFeatureSelector<NotificationsState>(NOTIFICATIONS_FEATURE_KEY);

export const getAll = (id: string) =>
  createSelector(
    getNotificationsState,
    (state: NotificationsState) => {
      return state.list;
    }
  );

export const notificationQuery = {
  getAll
};
