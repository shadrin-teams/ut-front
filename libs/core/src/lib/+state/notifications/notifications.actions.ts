import { createAction, props } from '@ngrx/store';
import { NotificationModel } from '@wt/models/notification';

export const NotificationAdd = createAction(
  '[Notification] Add',
  props<{payload: NotificationModel}>()
);

export const NotificationAddFail = createAction(
  '[Notification] Add Fail',
  props<{ error }>()
);

/*
import { AggregatableAction, CorrelationParams, FailActionForAggregation } from '@wt/core/+state/aggregate';
import {NotificationModel} from '@wt/models/notification';

export enum NotificationsActionTypes {
  NotificationAdd = '[Notification] Add',
  NotificationAddFail = '[Notification] Add Fail'
}

export interface NotificationAddPayload extends NotificationModel {
}

export class NotificationAdd implements AggregatableAction {
  readonly type = NotificationsActionTypes.NotificationAdd;

  constructor(public payload: NotificationAddPayload, public correlationParams: CorrelationParams) {
  }
}

export class NotificationAddFail implements FailActionForAggregation {
  readonly type = NotificationsActionTypes.NotificationAddFail;

  constructor(public payload: {}, public error: any, public correlationParams: CorrelationParams) {
  }
}

export type NotificationsAction =
  | NotificationAdd
  | NotificationAddFail;
*/
