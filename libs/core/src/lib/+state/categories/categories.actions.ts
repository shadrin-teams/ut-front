import { createAction, props } from '@ngrx/store';
import { CategoryModel } from '@wt/models/category';
import { MetaModel } from '@wt/models/others';

export interface CategoriesLoadPayload {
  where: { id?: number };
  fields?: any;
  perPage?: number;
}

export const CategoriesLoad = createAction(
  '[Categories] Categories Load',
  props<CategoriesLoadPayload>()
);

export interface CategoriesLoadSuccessPayload {
  categories: CategoryModel[];
  meta?: MetaModel;
}

export const CategoriesLoadSuccess = createAction(
  '[Categories] Categories Load Success',
  props<CategoriesLoadSuccessPayload>()
);

export const CategoriesLoadFail = createAction(
  '[Categories] Categories Load Error',
  props<{ error }>()
);

export const CategoriesSetLoading = createAction(
  '[Categories] Categories Set Loading'
);


/*
import {
  AggregatableAction,
  CorrelationParams,
  FailActionForAggregation
} from '@wt/core/+state/aggregate';
import {MetaModel} from '@wt/models/others';
import {CategoryModel} from '@wt/models/category/categoryModel';

export enum CategoriesActionTypes {
  CategoriesLoad = '[Categories] Categories Load',
  CategoriesLoadSuccess = '[Categories] Categories Load Success',
  CategoriesLoadFail = '[Categories] Categories Load Error',

  CategoriesSetLoading = '[Categories] Categories Set Loading'
}

export interface CategoriesLoadPayload {
  where: { id?: number };
  fields?: any;
  perPage?: number;
}

export class CategoriesLoad implements AggregatableAction {
  readonly type = CategoriesActionTypes.CategoriesLoad;

  constructor(public payload: CategoriesLoadPayload, public correlationParams: CorrelationParams) {
  }
}

export interface CategoriesLoadSuccessPayload {
  categories: CategoryModel[];
  meta?: MetaModel;
}

export interface EmptyPayload {
}

export class CategoriesLoadSuccess implements AggregatableAction {
  readonly type = CategoriesActionTypes.CategoriesLoadSuccess;

  constructor(public payload: CategoriesLoadSuccessPayload, public correlationParams: CorrelationParams) {
  }
}

export class CategoriesLoadFail implements FailActionForAggregation {
  readonly type = CategoriesActionTypes.CategoriesLoadFail;

  constructor(public payload: EmptyPayload, public error: any, public correlationParams: CorrelationParams) {
  }
}

export class CategoriesSetLoading implements AggregatableAction {
  readonly type = CategoriesActionTypes.CategoriesSetLoading;

  constructor(public payload: EmptyPayload, public correlationParams: CorrelationParams) {
  }
}

export type CategoriesAction =
  | CategoriesLoad
  | CategoriesLoadSuccess
  | CategoriesLoadFail
  | CategoriesSetLoading;
*/
