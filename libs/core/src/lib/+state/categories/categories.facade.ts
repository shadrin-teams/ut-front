import { Injectable } from '@angular/core';
import { filter } from 'rxjs/operators';
import { select, Store } from '@ngrx/store';
import { CategoriesLoad } from '@wt/core/+state/categories/categories.actions';
import { CategoriesStateData } from '@wt/core/+state/categories/categories.reducer';
import { categoriesQuery } from '@wt/core/+state/categories/categories.selectors';
import { Observable } from 'rxjs';
import { CategoryModel } from '@wt/models/category';

@Injectable()
export class CategoriesFacade {
  loaded$ = this.store.pipe(select(categoriesQuery.getLoaded));
  getCategories$ = this.store.pipe(select(categoriesQuery.getCategories));
  selected$ = this.store.pipe(select(categoriesQuery.geSelected));

  constructor(private store: Store<CategoriesStateData>) {
  }

  loadCategories(where = {}, fields = {}, perPage?: number) {
    this.store.dispatch(CategoriesLoad({ where, fields, perPage }));
  }

  getCategory(slugOrId: string | number): Observable<CategoryModel> {
    return this.store.pipe(select(categoriesQuery.getCategory(slugOrId)));
  }

  waitLoading() {
    return this.store.pipe(
      select(categoriesQuery.getLoaded),
      filter((item) => !!item)
    );
  }
}
