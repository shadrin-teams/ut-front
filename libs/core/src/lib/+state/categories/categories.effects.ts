import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { flatMap, switchMap, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { HttpClientResult } from '@wt/core/models';
import { CoreState } from '@wt/core/+state/core.reducer';
import { LoginSendCreateResponse } from '@wt/models/endpoints/auth/login-send';
import { CategoriesService } from '@wt/core/services/categories/categories.service';
import { CategoriesLoadBehavior } from '@wt/core/+state/categories/behavios/categories-load-behavior';
import { AuthenticationService } from '@wt/core/services/auth/authentication.service';
import * as CategoriesActionTypes from './categories.actions';

@Injectable()
export class CategoriesEffects/* extends BaseEffects*/ {

  @Effect()
  categoriesLoad$ = this.actions$.pipe(
    ofType(CategoriesActionTypes.CategoriesLoad),
    switchMap((action) => {
      const { where, fields, perPage } = action;
      const headers = this.authenticationService.loadAuthDataFromLocalStore();
      return this.categoriesService.categoriesLoad({
        where,
        fields,
        perPage,
        headers: { token: headers.token, uid: headers.id }
      })
        .pipe(
          flatMap((r: HttpClientResult<LoginSendCreateResponse>) => new CategoriesLoadBehavior(action, r).resolve())
        )
    })
  );



  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>,
    protected categoriesService: CategoriesService,
    protected authenticationService: AuthenticationService
  ) {
  }
}
