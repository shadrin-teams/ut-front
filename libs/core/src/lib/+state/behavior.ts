import { Action } from '@ngrx/store';

import { AggregatableAction, CorrelationParams } from './aggregate';
import { SetLastRequestResult, SetLastRequestResultPayload } from '@wt/core/+state/last-request-result';
import { ModelAdapter } from '@wt/models/model-adapter';
import { RequestResultInfo } from '@wt/models/http';

export abstract class Behavior {
  protected modelAdapter: ModelAdapter = ModelAdapter.createInstance();

  constructor(protected action: AggregatableAction) { }

  abstract resolve(): Action[];

  protected generateSetLastRequestResult(
    actionType: string,
    requestResultInfo: RequestResultInfo,
    correlationParams: CorrelationParams
  ) {
    const lastRequestResult = {
      actionType,
      ...requestResultInfo
    };

    const setLastRequestResulPayload: SetLastRequestResultPayload = {
      lastRequestResult
    };
    return SetLastRequestResult(
      setLastRequestResulPayload
    );
  }
}
