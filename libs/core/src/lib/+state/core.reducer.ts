import { userReducer, UsersState } from '@wt/core/+state/users';
import { imageReducer, ImagesState } from '@wt/core/+state/images';
import { uiUnitsReducer, UiUnitsState } from '@wt/core/+state/uiUnits';
import { filesReducer, FilesState, initialFileState } from '@wt/core/+state/files';
import { CategoriesState, categoryReducer, initialCategoryState } from '@wt/core/+state/categories';
import { hashTagsReducer, HashtagsState, initialHashtagState } from '@wt/core/+state/hashtags';
import { mainReducer, MainState } from '@wt/core/+state/main';

export const CORE_FEATURE_KEY = 'core';

/**
 * Interface for the 'Core' data used in
 *  - CoreState, and
 *  - coreReducer
 *
 *  Note: replace if already defined in another module
 */

/* tslint:disable:no-empty-interface */
export interface Entity {
}

/*
export type CoreState =
  & UsersStateData
  & ImagesStateData
  & UiUnitsStateData
  & RouterState
  & NotificationsStateData
  & FilesStateData
  & FormsStateData
  & CategoriesStateData
  & HashtagsStateData
  & MainStateData
  & {};


export interface CorePartialState {
  readonly [CORE_FEATURE_KEY]: CoreState;
}

const complexFormsReducer = (state, action) => {
  return state;
};

const complexUiUnitsReducer = (state, action) => {
  state = {
    ...uiUnitsReducer(state, action)
  };
  state = {
    ...complexFormsReducer(state, action)
  };
  return state;
};
*/
/*export const initialState: CoreState = {
  list: [],
  loaded: false
};

export function coreReducer(
  state: CoreState = initialState,
  action: CoreAction
): CoreState {
  switch (action.type) {
    case CoreActionTypes.CoreLoaded: {
      state = {
        ...state,
        list: action.payload,
        loaded: true
      };
      break;
    }private _router: Router
  }
  return state;
}*/

/*export const initialCoreData: CoreState = {
  user: initialUserState,
  images: initialImageState,
  uiunits: initialUiUnitsState,
  router: initialRouterData,
  notifications: initialNotificationsState,
  files: initialFileState,
  forms: initialFormsState,
  categories: initialCategoryState,
  hashtags: initialHashtagState,
  main: initialMainState
};*/

/*export const coreReducer: ActionReducerMap<CoreState> = {
  user: usersReducer,
  images: imagesReducer,
  uiunits: complexUiUnitsReducer,
  router: routerReducer,
  notifications: notificationsReducer,
  files: filesReducer,
  forms: formsReducer,
  categories: categoriesReducer,
  hashtags: hashtagsReducer,
  main: mainReducer
};*/

export interface CoreState {
  user: UsersState,
  images: ImagesState,
  uiunits: UiUnitsState,
  files: FilesState,
  categories: CategoriesState,
  hashtags: HashtagsState,
  main: MainState
};

export const coreReducer = {
  user: userReducer,
  images: imageReducer,
  uiunits: uiUnitsReducer,
  files: filesReducer,
  categories: categoryReducer,
  hashtags: hashTagsReducer,
  main: mainReducer
};

// export const coreReducerToken = new InjectionToken<ActionReducerMap<CoreState>>('CoreReducers');
// export const coreReducerProvider = [{ provide: coreReducerToken, useValue: coreReducer }];
