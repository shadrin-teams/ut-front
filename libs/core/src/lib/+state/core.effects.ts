import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { CoreState } from './core.reducer';
import { LoadCore } from './core.actions';
import { BaseEffects } from '@wt/core/+state/base.effects';
import { ImageCreate, ImageCreateFail } from '@wt/core/+state/images/images.actions';
import { catchError, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

@Injectable()
export class CoreEffects extends BaseEffects {

  constructor(
    protected actions$: Actions,
    protected store: Store<CoreState>
  ) {
    super(actions$, store);
  }
}
