import { NgModule } from '@angular/core';


import { FuseNavigationModule } from '@fuse/components';
import { NavbarVerticalStyle1Component } from '@wt/core/layout/components/navbar/vertical/style-1/style-1.component';
import { FuseSharedModule } from '@wt/fuse';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
    declarations: [
        NavbarVerticalStyle1Component
    ],
    imports     : [
        MatButtonModule,
        MatIconModule,

        FuseSharedModule,
        FuseNavigationModule
    ],
    exports     : [
        NavbarVerticalStyle1Component
    ]
})
export class NavbarVerticalStyle1Module
{
}
