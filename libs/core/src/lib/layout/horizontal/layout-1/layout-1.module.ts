import { NgModule } from '@angular/core';

import { FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';
import { HorizontalLayout1Component } from '@wt/core/layout/horizontal/layout-1/layout-1.component';
import { ChatPanelModule } from '@wt/core/layout/components/chat-panel/chat-panel.module';
import { ContentModule } from '@wt/core/layout/components/content/content.module';
import { FooterModule } from '@wt/core/layout/components/footer/footer.module';
import { NavbarModule } from '@wt/core/layout/components/navbar/navbar.module';
import { QuickPanelModule } from '@wt/core/layout/components/quick-panel/quick-panel.module';
import { ToolbarModule } from '@wt/core/layout/components/toolbar/toolbar.module';
import { MatSidenavModule } from '@angular/material/sidenav';

@NgModule({
    declarations: [
        HorizontalLayout1Component
    ],
    imports     : [
        MatSidenavModule,

        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        ChatPanelModule,
        ContentModule,
        FooterModule,
        NavbarModule,
        QuickPanelModule,
        ToolbarModule
    ],
    exports     : [
        HorizontalLayout1Component
    ]
})
export class HorizontalLayout1Module
{
}
