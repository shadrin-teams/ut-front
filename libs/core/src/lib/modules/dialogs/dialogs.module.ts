import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeleteComponent } from './components/delete/delete.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DeleteComponent],
  entryComponents: [
    DeleteComponent
  ]
})
export class DialogsModule {
}
