import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FilesFacade } from '@wt/core/+state/files';
import {
  FileListMode,
  FileListModeSort,
  IFile
} from '@wt/core/modules/files-upload/files-upload-list-images/files-upload-list-images.component';
import { untilComponentDestroyed } from '@wt/core/utils';
import { DialogService } from '@wt/core/dialogs/dialog.service';
import { ClipboardService } from 'ngx-clipboard';

@Component({
  selector: 'wt-file-list-card',
  templateUrl: './file-list-card.component.html',
  styleUrls: ['./file-list-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileListCardComponent implements OnInit, OnDestroy {
  @Input() file: IFile;
  @Input() section: string;
  @Input() itemId: number;
  @Input() mode: FileListMode;
  @Input() main: boolean;

  ModeSort = FileListModeSort;

  constructor(private filesFacade: FilesFacade,
              private dialogService: DialogService,
              private _clipboardService: ClipboardService) {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
  }

  changeTitle(event, file) {
    file.title = event.target.value;
  }

  saveTitle(file) {
    this.fileUpdate(file.id, { title: file.title });
  }

  fileUpdate(id, params) {
    this.filesFacade.filesUpdate({ ...params, id, section: this.section, itemId: this.itemId });
  }

  fileDelete(id: number) {
    this.dialogService.openDeleteDialog()
      .pipe(
        untilComponentDestroyed(this)
      )
      .subscribe((result) => {
        if (result) {
          this.filesFacade.filesDelete(id);
        }
      });
  }

  setMain(id: number, main: boolean) {
    if (!main) {
      this.fileUpdate(id, { main: true });
    }
  }

  copyFileUrl(url: string): void {
    this._clipboardService.copy(url);
    alert('Ссылка скопирована');
  }
}
