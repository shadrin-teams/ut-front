import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { SortablejsModule } from 'angular-sortablejs';
import { FileUploadModule } from 'ng2-file-upload';

import { DragDropModule } from '@angular/cdk/drag-drop';

import { FilesUploadComponent } from './files-upload/files-upload.component';
//import { CoreModule } from '@wt/core';
import { FilesUploadListImagesComponent } from './files-upload-list-images/files-upload-list-images.component';
import { FileListCardComponent } from './file-list-card/file-list-card.component';
import { SpinnerModule } from '@wt/core/components/spinner/spinner.module';
import { ClipboardModule, ClipboardService } from 'ngx-clipboard';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';

@NgModule({
  imports: [
    CommonModule,

    MatIconModule,
    MatProgressBarModule,

    // CoreModule,
    FileUploadModule,

    DragDropModule,
    SpinnerModule,
    ClipboardModule
    // SortablejsModule
  ],
  declarations: [FilesUploadComponent, FilesUploadListImagesComponent, FileListCardComponent],
  exports: [FilesUploadComponent, DragDropModule],
  providers: [ClipboardService]
})
export class FilesUploadModule {
}
