import { BaseErrors } from '@shared/constants/base-errors.const';
import { AsyncValidatorsMessages } from './async-validators-messages';

export const GlobalErrorMessages = {
  ...AsyncValidatorsMessages,
  ...BaseErrors
}
