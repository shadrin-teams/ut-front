import { InjectionToken } from '@angular/core';

import { SocialAuthServiceConfig } from '../services';
import { AppConfig } from '@wt/models/config';

export const CORE_CONFIG: InjectionToken<AppConfig> = new InjectionToken<AppConfig>('CORE_CONFIG');

export const AUTH_SERVICE_CONFIG: InjectionToken<SocialAuthServiceConfig> = new InjectionToken<SocialAuthServiceConfig>('AUTH_SERVICE_CONFIG');
