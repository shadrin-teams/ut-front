export function trackById(index: number, item: { id: string }) {
  return item ? item.id : undefined;
}
