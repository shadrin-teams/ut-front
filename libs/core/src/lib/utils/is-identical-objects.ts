import { isIdenticalArray } from './is-identical-array';

export function isIdenticalObjects(a: {[key: string]: any}, b: {[key: string]: any}): boolean {
  if (!a || !b) {
    return false;
  }

  const aKeys = Object.keys(a);
  const bKeys = Object.keys(b);

  if (aKeys.length === bKeys.length) {
    return aKeys.every(key => {
      if (Array.isArray(a[key])) {
        return isIdenticalArray(a[key], b[key]);
      } else {
        return a[key] === b[key];
      }
    });
  }

  return false;
}
