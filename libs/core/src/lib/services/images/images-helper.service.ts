import { Injectable } from '@angular/core';

import { FileModel, VersionsModel } from '@wt/models/file';

@Injectable()
export class ImagesHelperService {
  getMainFileVersions(files : FileModel[]): VersionsModel {
    if (files && files.length) {
      let result;
      result = files.find(items => items.main);
      if (!result) {
        result = files[0];
      }

      if (result) {
        return result.versions;
      }
    }

    return null;
  }
}
