import {BaseAPI} from '../base/base-api';
import {
  ImagesCreateRequest,
  ImagesCreateRequestConfig,
  ImagesCreateResponseFactory,
  ImagesDeleteRequest,
  ImagesDeleteRequestConfig,
  ImagesDeleteResponseFactory,
  ImagesEndpointTypes,
  ImagesGetNextRequest,
  ImagesGetNextRequestConfig, ImagesGetNextResponseFactory,
  ImagesGetRequest,
  ImagesGetRequestConfig,
  ImagesGetResponseFactory,
  ImagesRequestConfig,
  ImagesResponse
} from '@wt/models/endpoints';

import {Request} from '@wt/models/http';
import {ImagesLoadRequest} from '@wt/models/endpoints/images/images-load/request/images-load-request';
import {ImagesUpdateRequest} from '@wt/models/endpoints/images/images-update/request/images-update-request';
import {ImagesLoadRequestConfig} from '@wt/models/endpoints/images/images-load/request/images-load-request-config';
import {ImagesUpdateRequestConfig} from '@wt/models/endpoints/images/images-update/request/images-update-request-config';
import {ImagesLoadResponseFactory} from '@wt/models/endpoints/images/images-load/response/images-load-response-factory';
import {ImagesUpdateResponseFactory} from '@wt/models/endpoints/images/images-update/response/images-update-response-factory';

export class ImagesApi extends BaseAPI {
  constructor(protected domain: string) {
    super(domain);
  }

  createRequest(paramsConfig: ImagesRequestConfig, endPointType: string) {
    let request: Request;
    switch (endPointType) {
      case ImagesEndpointTypes.CREATE:
        request = new ImagesCreateRequest(this.domain, <ImagesCreateRequestConfig>paramsConfig);
        break;
      case ImagesEndpointTypes.LOAD:
        request = new ImagesLoadRequest(this.domain, <ImagesLoadRequestConfig>paramsConfig);
        break;
      case ImagesEndpointTypes.UPDATE:
        request = new ImagesUpdateRequest(this.domain, <ImagesUpdateRequestConfig>paramsConfig);
        break;
      case ImagesEndpointTypes.DELETE:
        request = new ImagesDeleteRequest(this.domain, <ImagesDeleteRequestConfig>paramsConfig);
        break;
      case ImagesEndpointTypes.GET:
        request = new ImagesGetRequest(this.domain, <ImagesGetRequestConfig>paramsConfig);
        break;
      case ImagesEndpointTypes.GETNEXT:
        request = new ImagesGetNextRequest(this.domain, <ImagesGetNextRequestConfig>paramsConfig);
        break;
      default:
        throw new Error(`Request for ${endPointType} is undefined `);
    }
    return request;
  }

  handleResponse(response: any, endPointType: string) {
    let courseResponse: ImagesResponse;
    switch (endPointType) {
      case ImagesEndpointTypes.CREATE:
        courseResponse = ImagesCreateResponseFactory.createResponse(response);
        break;
      case ImagesEndpointTypes.LOAD:
        courseResponse = ImagesLoadResponseFactory.createResponse(response);
        break;
      case ImagesEndpointTypes.UPDATE:
        courseResponse = ImagesUpdateResponseFactory.createResponse(response);
        break;
      case ImagesEndpointTypes.DELETE:
        courseResponse = ImagesDeleteResponseFactory.createResponse(response);
        break;
      case ImagesEndpointTypes.GET:
        courseResponse = ImagesGetResponseFactory.createResponse(response);
        break;
      case ImagesEndpointTypes.GETNEXT:
        courseResponse = ImagesGetNextResponseFactory.createResponse(response);
        break;
      default:
        throw new Error(`Response for ${endPointType} is undefined `);
    }
    return courseResponse;
  }
}
