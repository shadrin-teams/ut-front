import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClientResult } from '../../models';
import { CORE_CONFIG } from '../../tokens';
import { BaseAPIService } from '../base/base-api-service';
import { ImagesApi } from './images-api';
import { AppConfig } from '@wt/models/config';
import {
  ImagesCreateRequestConfig,
  ImagesDeleteRequestConfig,
  ImagesEndpointTypes, ImagesGetNextRequestConfig, ImagesGetRequestConfig,
  ImagesResponse
} from '@wt/models/endpoints';
import { ImagesUpdateRequestConfig } from '@wt/models/endpoints/images/images-update/request/images-update-request-config';
import { ImagesLoadRequestConfig } from '@wt/models/endpoints/images/images-load/request/images-load-request-config';

@Injectable({
  providedIn: 'root'
})
export class ImagesService extends BaseAPIService {
  constructor(protected httpClient: HttpClient, @Inject(CORE_CONFIG) private config: AppConfig) {
    super(httpClient);
    this.api = new ImagesApi(this.config.apiUrl);
  }

  imagesCreate(config: ImagesCreateRequestConfig): Observable<HttpClientResult<ImagesResponse>> {
    return this.doRequest(config, ImagesEndpointTypes.CREATE);
  }

  imagesLoad(config: ImagesLoadRequestConfig): Observable<HttpClientResult<ImagesResponse>> {
    return this.doRequest(config, ImagesEndpointTypes.LOAD);
  }

  imagesUpdate(config: ImagesUpdateRequestConfig): Observable<HttpClientResult<ImagesResponse>> {
    return this.doRequest(config, ImagesEndpointTypes.UPDATE);
  }

  imagesDelete(config: ImagesDeleteRequestConfig): Observable<HttpClientResult<ImagesResponse>> {
    return this.doRequest(config, ImagesEndpointTypes.DELETE);
  }

  getImage(config: ImagesGetRequestConfig): Observable<HttpClientResult<ImagesResponse>> {
    return this.doRequest(config, ImagesEndpointTypes.GET);
  }

  getImageNext(config: ImagesGetNextRequestConfig): Observable<HttpClientResult<ImagesResponse>> {
    return this.doRequest(config, ImagesEndpointTypes.GETNEXT);
  }
}
