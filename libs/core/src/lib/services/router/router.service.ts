import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { CoreState } from '@wt/core/+state/core.reducer';
import { Go } from '@wt/core/+state/router';

@Injectable({providedIn: 'root'})
export class RouterService {
  constructor(private store: Store<CoreState>,) {

  }

  goTo(src) {
    this.store.dispatch(Go({path: [src]}));
  }
}
