export class ReducerService {

  static clearAll(initState) {

  }

  static upsertOne(state, item, id?: string | number) {
    const newState = {
      ...state,
      ids: [...state.ids],
      list: [...state.list]
    };
    const itemId = id ? id : item.id;
    const foundIndex = this.findItemIndex(state, itemId);

    if (foundIndex || foundIndex === 0) {
      // newState.list[foundIndex] = {...item};
      newState.list[foundIndex] = {...newState.list[foundIndex],...item};
    } else {
      if (Array.isArray(newState.list)) {
        newState.list.push(item);
      } else {
        newState.list[item.id] = item;
      }
      newState.ids.push(itemId);
    }
    return newState;
  }

  static upsertMeny(state, items) {
    let newState = { ...state };
    items.forEach((item) => {
      newState = this.upsertOne(newState, {...item});
    });
    return newState;
  }

  static findItemIndex(state, itemId: number | string) {
    const itemIdChecked = itemId;
    let foundIndex = null;
    if (state.ids.indexOf(itemIdChecked) !== -1) {
      state.list.forEach((item: any, index: number) => {
        if (item.id === itemIdChecked) {
          foundIndex = index;
        }
      });
    }
    return foundIndex;
  }

  static deleteOne(state, itemId: number) {
    const itemIdChecked = +itemId;
    if (state.ids.indexOf(itemIdChecked) !== -1) {
      state.list = state.list.filter((item) => item.id !== itemIdChecked);
      state.ids = state.ids.filter((id) => id !== itemIdChecked);
    }
    return state;
  }
}
