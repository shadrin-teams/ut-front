import { Injectable } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';
import { IFormFieldFormat } from '@wt/core/components/auto-form/interfaces/form-field-format';

@Injectable({ providedIn: 'root' })
export class BaseAutoFormService {
  getPreparedFields(fields: IFormField[]): { [key: string]: any } {
    const result = {};
    fields.forEach((item: IFormField) => {

      const validators = [];
      let asyncValidators = [];
      if (item.require) {
        validators.push(Validators.required);
      }

      if (item.format) {
        const validator = this.getValidatorByFormat(item.format);
        if (validator) {
          validators.push(validator);
        }
      }

      if (item.asyncValidators && item.asyncValidators.length) {
        asyncValidators = this.getAsyncValidators(item.asyncValidators);
      }

      result[item.field] = [null, validators, asyncValidators];
    });

    return result;
  }

  getAsyncValidators(validators) {
    return [];
  }

  getValidatorByFormat(format: IFormFieldFormat) {
    let validator;
    if (format === IFormFieldFormat.Email) {
      validator = Validators.email;
    }

    if (format === IFormFieldFormat.Phone) {
      validator = Validators.pattern(/[\+0-9]/);
    }

    if (format === IFormFieldFormat.Number) {
      validator = Validators.pattern(/[0-9]$/);
    }

    if (format === IFormFieldFormat.Date) {
      validator = Validators.pattern(/[0-9\-]/);
    }

    return validator;
  }

  setAllFieldTouched(form: FormGroup): void {
    form.markAllAsTouched();
/*    if (form && form.controls) {
      Object.values(form.controls)
        .forEach(control => {
          control.markAsTouched();
          control.markAsDirty();
          control.markAsPending();
        });
    }*/
  }
}
