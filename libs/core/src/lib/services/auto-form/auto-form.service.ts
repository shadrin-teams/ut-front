import { Injectable } from '@angular/core';
import { BaseAutoFormService } from '@wt/core/services/auto-form/base-auto-form.service';

@Injectable()
export class AutoFormService extends BaseAutoFormService {
  constructor() {
    super();
  }

  getAsyncValidators(validators: string[]) {
    return [];
  }

}
