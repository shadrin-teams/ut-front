import { LoginProvider } from './providers';

export interface AuthServiceConfigItem {
    id: string;
    provider: LoginProvider;
}


export class SocialAuthServiceConfig {
    providers: Map<string, LoginProvider> = new Map<string, LoginProvider>();

    constructor(providers: AuthServiceConfigItem[]) {
        for (let i = 0; i < providers.length; i++) {
            const element = providers[i];
            this.providers.set(element.id, element.provider);
        }
    }
}
