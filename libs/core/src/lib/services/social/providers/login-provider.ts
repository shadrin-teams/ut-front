import { LoginOptions, SocialUser } from '@wt/models/social';

export interface LoginProvider {
    initialize(): Promise<string>;
    getLoginStatus(): Promise<SocialUser>;
    signIn(opt?: LoginOptions): Promise<SocialUser>;
    signOut(): Promise<any>;
}
