import { BaseAPI } from '../base/base-api';
import {
  LoginSendCreateRequest,
  LoginSendCreateRequestConfig,
  LoginSendCreateResponseFactory
} from '@wt/models/endpoints/auth/login-send';
import { AuthEndpointTypes, AuthRequestConfig } from '@wt/models/endpoints';

import { Request } from '@wt/models/http/request';
import { Response } from '@wt/models/http/response';
import {
  RefreshTokenRequest,
  RefreshTokenRequestConfig,
  RefreshTokenResponseFactory
} from '@wt/models/endpoints/auth/refresh-token';

export class UsersApi extends BaseAPI {
  constructor(protected domain: string) {
    super(domain);
  }

  createRequest(paramsConfig: AuthRequestConfig, endPointType: string) {
    let request: Request;
    switch (endPointType) {
      case AuthEndpointTypes.LOGIN_SEND:
        request = new LoginSendCreateRequest(this.domain, <LoginSendCreateRequestConfig>paramsConfig);
        break;
      case AuthEndpointTypes.REFRESH_TOKEN:
        request = new RefreshTokenRequest(this.domain, <RefreshTokenRequestConfig>paramsConfig);
        break;
      default:
        throw new Error(`Request for ${endPointType} is undefined `);
    }
    return request;
  }

  handleResponse(response: any, endPointType: string) {
    let courseResponse: Response;
    switch (endPointType) {
      case AuthEndpointTypes.LOGIN_SEND:
        courseResponse = LoginSendCreateResponseFactory.createResponse(response);
        break;
      case AuthEndpointTypes.REFRESH_TOKEN:
        courseResponse = RefreshTokenResponseFactory.createResponse(response);
        break;
      default:
        throw new Error(`Response for ${endPointType} is undefined `);
    }
    return courseResponse;
  }
}
