import { Injectable, Injector, PLATFORM_ID, Provider } from '@angular/core';

import { Storage } from '../../models/storage';
import { cookiesProviderFactory } from '../cookies/cookies-provider';

@Injectable()
export class StorageService implements Storage {
    setItem(key: string, value: string, options?: any): void {
    }

    putObject(key: string, value: Object): void {
    }

    getItem(key: string): string {
        return null;
    }

    getObject(key: string): { [key: string]: string } | string {
        return '';
    }

    getAll(): { [key: string]: string } {
        return null;
    }

    removeItem(key: string): void {
    }

    clear(): void {
    }
}

export const StorageProvider: Provider = {
    provide: StorageService,
    deps: [PLATFORM_ID, Injector],
    useFactory: cookiesProviderFactory,
};
