import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClientResult } from '../../models';
import { CORE_CONFIG } from '../../tokens';
import { BaseAPIService } from '../base/base-api-service';
import { FilesApi } from './files-api';
import { AppConfig } from '@wt/models/config';
import {
  FilesDeleteRequestConfig,
  FilesEndpointTypes, FilesLoadRequestConfig,
  FilesResponse, FilesSortRequestConfig,
  FilesUpdateRequestConfig
} from '@wt/models/endpoints';

@Injectable({
  providedIn: 'root'
})
export class FilesService extends BaseAPIService {
  constructor(protected httpClient: HttpClient, @Inject(CORE_CONFIG) private config: AppConfig) {
    super(httpClient);
    this.api = new FilesApi(this.config.apiUrl);
  }

  filesLoad(config: FilesLoadRequestConfig): Observable<HttpClientResult<FilesResponse>> {
    return this.doRequest(config, FilesEndpointTypes.LOAD);
  }

  fileUpdate(config: FilesUpdateRequestConfig): Observable<HttpClientResult<FilesResponse>> {
    return this.doRequest(config, FilesEndpointTypes.UPDATE);
  }

  fileDelete(config: FilesDeleteRequestConfig): Observable<HttpClientResult<FilesResponse>> {
    return this.doRequest(config, FilesEndpointTypes.DELETE);
  }

  filesSort(config: FilesSortRequestConfig): Observable<HttpClientResult<FilesResponse>> {
    return this.doRequest(config, FilesEndpointTypes.SORT);
  }
}

