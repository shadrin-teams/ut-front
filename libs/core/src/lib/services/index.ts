import {UsersService} from './users/users.service';
import {RouterService} from '@wt/core/services/router/router.service';
import {ImagesService} from '@wt/core/services/images/images.service';
import {FilesService} from '@wt/core/services/files/files.service';
import {CategoriesService} from '@wt/core/services/categories/categories.service';
import {HashtagsService} from '@wt/core/services/hashtags/hashtags.service';
import { AutoFormService } from '@wt/core/services/auto-form/auto-form.service';

export const services = [
  UsersService,
  RouterService,
  ImagesService,
  FilesService,
  CategoriesService,
  HashtagsService,
  AutoFormService
];

export * from './social';
