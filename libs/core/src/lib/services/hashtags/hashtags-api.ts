import {BaseAPI} from '../base/base-api';
import {
  HashtagsEndpointTypes,
  HashtagsRequestConfig,
  HashtagsResponse
} from '@wt/models/endpoints';

import {Request} from '@wt/models/http';
import {HashtagsLoadRequest} from '@wt/models/endpoints/hashtags/hashtags-load/request/hashtags-load-request';
import {HashtagsLoadRequestConfig} from '@wt/models/endpoints/hashtags/hashtags-load/request/hashtags-load-request-config';
import {HashtagsLoadResponseFactory} from '@wt/models/endpoints/hashtags/hashtags-load/response/hashtags-load-response-factory';

export class HashtagsApi extends BaseAPI {
  constructor(protected domain: string) {
    super(domain);
  }

  createRequest(paramsConfig: HashtagsRequestConfig, endPointType: string) {
    let request: Request;
    switch (endPointType) {
      case HashtagsEndpointTypes.LOAD:
        request = new HashtagsLoadRequest(this.domain, <HashtagsLoadRequestConfig>paramsConfig);
        break;
      default:
        throw new Error(`Request for ${endPointType} is undefined `);
    }
    return request;
  }

  handleResponse(response: any, endPointType: string) {
    let courseResponse: HashtagsResponse;
    switch (endPointType) {
      case HashtagsEndpointTypes.LOAD:
        courseResponse = HashtagsLoadResponseFactory.createResponse(response);
        break;
      default:
        throw new Error(`Response for ${endPointType} is undefined `);
    }
    return courseResponse;
  }
}
