import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {HttpClientResult} from '../../models';
import {CORE_CONFIG} from '../../tokens';
import {BaseAPIService} from '../base/base-api-service';
import {HashtagsApi} from './hashtags-api';
import {AppConfig} from '@wt/models/config';
import {HashtagsEndpointTypes, HashtagsResponse} from '@wt/models/endpoints';
import {HashtagsLoadRequestConfig} from '@wt/models/endpoints/hashtags/hashtags-load/request/hashtags-load-request-config';

@Injectable({
  providedIn: 'root'
})
export class HashtagsService extends BaseAPIService {
  constructor(protected httpClient: HttpClient, @Inject(CORE_CONFIG) private config: AppConfig) {
    super(httpClient);
    this.api = new HashtagsApi(this.config.apiUrl);
  }

  hashtagsLoad(config: HashtagsLoadRequestConfig): Observable<HttpClientResult<HashtagsResponse>> {
    return this.doRequest(config, HashtagsEndpointTypes.LOAD);
  }
}
