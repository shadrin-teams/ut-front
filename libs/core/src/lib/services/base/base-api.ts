import { RequestConfig } from '@wt/models/http';
import { Request } from '@wt/models/http/request';
import { Response } from '@wt/models/http/response';

export abstract class BaseAPI {
  constructor(protected domain: string) {
  }

  abstract createRequest(paramsConfig: RequestConfig, endPoint: string): Request;

  abstract handleResponse(response: any, endPoint: string): Response
}
