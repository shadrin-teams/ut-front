import {HttpClient} from '@angular/common/http';
import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import {HttpClientResult} from '../../models';
import {CORE_CONFIG} from '../../tokens';
import {BaseAPIService} from '../base/base-api-service';
import {CategoriesApi} from './categories-api';
import {AppConfig} from '@wt/models/config';
import {CategoriesEndpointTypes, CategoriesResponse} from '@wt/models/endpoints';
import {CategoriesLoadRequestConfig} from '@wt/models/endpoints/categories/categories-load/request/categories-load-request-config';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends BaseAPIService {
  constructor(protected httpClient: HttpClient, @Inject(CORE_CONFIG) private config: AppConfig) {
    super(httpClient);
    this.api = new CategoriesApi(this.config.apiUrl);
  }

  categoriesLoad(config: CategoriesLoadRequestConfig): Observable<HttpClientResult<CategoriesResponse>> {
    return this.doRequest(config, CategoriesEndpointTypes.LOAD);
  }
}
