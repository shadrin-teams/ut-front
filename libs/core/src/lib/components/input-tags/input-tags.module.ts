import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InputTagsComponent} from '@wt/core/components/input-tags/input-tags.component';
import {ChipsInputModule} from '@wt/core/components/chips-input/chips-input.module';

@NgModule({
  declarations: [InputTagsComponent],
  exports: [InputTagsComponent],
  imports: [
    CommonModule,
    ChipsInputModule
  ]
})
export class InputTagsModule { }
