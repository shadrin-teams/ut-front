import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {CategoriesFacade} from '@wt/core/+state/categories';
import {Observable} from 'rxjs';
import {CategoryModel} from '@wt/models/category';
import {trackById} from '@wt/core/utils';
import {filter, map} from 'rxjs/operators';

@Component({
  selector: 'wt-post-categories',
  templateUrl: './post-categories.component.html',
  styleUrls: ['./post-categories.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostCategoriesComponent implements OnInit {
  loaded = false;
  categories$: Observable<CategoryModel[]>;
  trackByFn = trackById;

  constructor(private categoriesFacade: CategoriesFacade) {
  }

  ngOnInit() {
    const fields = {
      fields: ['name', 'slug'],
      count: {}
    };

    this.categoriesFacade.loadCategories({}, fields, 100);
    this.categories$ = this.categoriesFacade.getCategories$
      .pipe(
        filter(items => !!items),
        map(items => {
          const newItems = [...items];
          if (items.length) {
            this.loaded = true;
          }
          return newItems.sort(this.sortItems);
        })
      );
  }

  sortItems(a, b) {
    if (a.count > b.count) return -1;
    if (a.count < b.count) return 1;

    return -1;
  }

}
