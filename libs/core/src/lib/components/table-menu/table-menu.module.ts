import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TableMenuComponent} from '@wt/core/components/table-menu/table-menu.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
  declarations: [TableMenuComponent],
  imports: [
    CommonModule,
    MatIconModule,
    MatMenuModule
  ],
  exports: [
    TableMenuComponent,
    MatIconModule,
    MatMenuModule
  ]
})
export class TableMenuModule {
}
