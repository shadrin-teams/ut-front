import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  STICKY_SIDEBAR_OPTIONS,
  StickySidebarDirective,
  StickySidebarInnerDirective,
  StickySidebarOptions
} from './sticky-sidebar.directive';

@NgModule({
  declarations: [StickySidebarDirective, StickySidebarInnerDirective],
  imports: [
    CommonModule
  ],
  exports: [StickySidebarDirective, StickySidebarInnerDirective]
})
export class StickySidebarModule {
  static withOptions(options: Partial<StickySidebarOptions>): ModuleWithProviders<any>{
    return {
      ngModule: StickySidebarModule,
      providers: [
        {
          provide: STICKY_SIDEBAR_OPTIONS,
          useValue: options
        }
      ]
    };
  }
}
