import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith, tap} from 'rxjs/operators';

/**
 * @title Chips Autocomplete
 */
interface ChipsItem {
  id: number;
  title: string;
}

@Component({
  selector: 'wt-chips-input',
  templateUrl: 'chips-input.component.html',
  styleUrls: ['chips-input.component.scss']
})
export class ChipsInputComponent {
  @Input() title: string;
  @Input() items: ChipsItem[] = [];
  @Input() selectedItems: ChipsItem[] = [];
  @Output() changedList = new EventEmitter<ChipsItem[]>();
  @Output() changedText = new EventEmitter<string>();

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  itemsCtrl = new FormControl();
  filteredItems: Observable<ChipsItem[]>;

  @ViewChild('itemInput', {static: false}) itemInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

  constructor() {
    this.filteredItems = this.itemsCtrl.valueChanges.pipe(
      startWith(null),
      map((item: any) => { // ChipsItem | null
        this.changedText.emit(item);
        if (this.items) {
          return item ? this._filter(item) : this.items.slice();
        } else {
          return [];
        }
      })
    );
  }

  add(event: MatChipInputEvent): void {
    // Add item only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our item
      if ((value || '').trim()) {
        this.selectedItems.push({id: 3, title: value.trim()});
        this.changedList.emit(this.selectedItems);
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.itemsCtrl.setValue(null);
    }
  }

  remove(item: ChipsItem): void {
    this.selectedItems = this.selectedItems.filter(SItem => SItem.id !== item.id);
    this.changedList.emit(this.selectedItems);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.selectedItems.push({id: 1, title: event.option.viewValue});
    this.itemInput.nativeElement.value = '';
    this.itemsCtrl.setValue(null);
    this.changedList.emit(this.selectedItems);
  }

  private _filter(value: ChipsItem | string): ChipsItem[] {
    if (typeof value === 'string') {
      return this.items.filter(itemFilter => itemFilter.title.indexOf(value) === 0);
    } else {
      return this.items.filter(itemFilter => {
        return itemFilter.id !== value.id;
      });
    }

  }
}
