import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChipsInputComponent} from './chips-input.component';
import {ReactiveFormsModule} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [ChipsInputComponent],
  exports: [ChipsInputComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatIconModule,
    ReactiveFormsModule
  ]
})
export class ChipsInputModule {
}
