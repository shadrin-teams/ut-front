import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ERRORS } from '@wt/core/components/auto-form/constants/errors';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';

@Component({
  selector: 'wt-form-field-error',
  templateUrl: './form-field-error.component.html',
  styleUrls: ['./form-field-error.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldErrorComponent implements OnInit, OnDestroy {
  @Input() fieldData: IFormField;
  @Input() form: FormGroup;
  @Input() errorMessages: { [key: string]: string };

  error: string;
  private unSubscribeSubject$ = new Subject();

  constructor() {
  }

  ngOnInit() {
    this.getError();
    this.statusChanges();
  }

  ngOnDestroy() {
    this.unSubscribeSubject$.next();
    this.unSubscribeSubject$.complete();
  }

  private statusChanges(): void {
    this.form.get(this.fieldData.field).statusChanges
      .pipe(
        takeUntil(this.unSubscribeSubject$)
      )
      .subscribe(() => {
        console.log('FormFieldErrorComponent.statusChanges', this.fieldData);
        this.getError();
      });
  }

  private getError(): void {
    const control = this.form.get(this.fieldData.field);
    console.log('getError', this.fieldData.field, control);
    if (control && control.errors) {
      Object
        .keys(control.errors)
        .map(errorKey => {
          console.log('errorKey', errorKey);
          switch (errorKey) {
            case 'required':
              this.error = ERRORS['required'].message1;
              break;
            case 'email':
              this.error = ERRORS['email'].message1;
              break;
            case 'mask':
              this.error = ERRORS['mask'].message1;
              break;
            default :
              this.error = this.errorMessages && this.errorMessages[errorKey] ? this.errorMessages[errorKey] : errorKey;
              break;
          }
          console.log('this.error', this.error);
        });
    }
  }
}
