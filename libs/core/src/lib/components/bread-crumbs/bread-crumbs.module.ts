import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {BreadCrumpsComponent} from './bread-crumps.component';

@NgModule({
  declarations: [BreadCrumpsComponent],
  imports: [CommonModule, RouterModule],
  exports: [CommonModule, BreadCrumpsComponent]
})
export class BreadCrumbsModule {
}
