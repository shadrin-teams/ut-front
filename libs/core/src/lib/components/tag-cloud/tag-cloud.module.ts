import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagCloudComponent } from './tag-cloud.component';
import {SpinnerModule} from '@wt/core/components/spinner/spinner.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [TagCloudComponent],
  imports: [
    CommonModule,
    SpinnerModule,
    RouterModule
  ],
  exports: [TagCloudComponent]
})
export class TagCloudModule { }
