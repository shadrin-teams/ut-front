import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { trackById } from '@wt/core/utils';
import { HashtagsFacade } from '@wt/core/+state/hashtags';
import { Observable, Subject } from 'rxjs';
import { HashtagModel } from '@wt/models/hashtag';
import { filter, map, takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import { ImagesFacade } from '@wt/core/+state/images';
import { ImageFullModel } from '@wt/models/image';

@Component({
  selector: 'wt-tag-cloud',
  templateUrl: './tag-cloud.component.html',
  styleUrls: ['./tag-cloud.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagCloudComponent implements OnInit, OnDestroy {
  loaded = false;
  hashtags: HashtagModel[];
  trackByFn = trackById;
  unsubscribeSubject$ = new Subject();

  constructor(private hashtagsFacade: HashtagsFacade,
              private route: ActivatedRoute,
              private imagesFacade: ImagesFacade,
              private cd: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.initRouteSubscription();
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject$.next();
    this.unsubscribeSubject$.complete();
  }

  private getAllTags(): Observable<HashtagModel[]> {
    this.hashtagsFacade.loadHashtags();
    return this.hashtagsFacade.getHashtags$.pipe(
      filter(items => !!items),
      takeUntil(this.unsubscribeSubject$)
    );
  }

  private initRouteSubscription() {
    this.route.params
      .pipe(
        switchMap((data) => {
          if (data && data.slug && data.category) {
            const slugParams = data.slug.split('.');
            const slug = slugParams[0];

            return this.loadTagsFromImage(slug);
          } else {
            return this.getAllTags();
          }
        }),
        takeUntil(this.unsubscribeSubject$)
      )
      .subscribe((hashtags: HashtagModel[]) => {
        this.hashtags = hashtags;
        this.cd.markForCheck();
      });
  }

  private loadTagsFromImage(imageSlug: string): Observable<HashtagModel[]> {
    return this.imagesFacade.getFullImage(imageSlug)
      .pipe(
        filter(image => !!image),
        map((image: ImageFullModel) => {
          if (image && image.tags) {
            return image.tags;
          }

          return [];
        }),
        takeUntil(this.unsubscribeSubject$)
      )
  }
}
