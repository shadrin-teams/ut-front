import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ImagesFacade } from '@wt/core/+state/images';
import { ImageFullModel } from '@wt/models/image';
import { distinctUntilChanged, filter, tap } from 'rxjs/operators';
import { trackById, untilComponentDestroyed } from '@wt/core/utils';
import { FileModel, VersionsModel } from '@wt/models/file';
import { ImagesHelperService } from '@wt/core/services/images/images-helper.service';

interface ImageWithMainFileModel extends ImageFullModel {
  mainFile?: VersionsModel;
}

@Component({
  selector: 'wt-popular-posts',
  templateUrl: './popular-posts.component.html',
  styleUrls: ['./popular-posts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopularPostsComponent implements OnInit, OnDestroy {
  loaded = false;
  images: ImageWithMainFileModel[];
  trackByFn = trackById;

  constructor(private imagesFacade: ImagesFacade,
              private cd: ChangeDetectorRef,
              private imagesHelperService: ImagesHelperService) {
  }

  ngOnInit() {
    this.imagesFacade.loadImages({
      fields: this.imagesFacade.fields2,
      paging: { perPage: 4, page: 1 },
      sort: '-views',
      imagesType: 'popular'
    });
    this.imagesFacade.getFullImages$
      .pipe(
        filter(items => !!items),
        distinctUntilChanged((prev, cur) => {
          const prevCategoryCount = this.getCategoryCount(prev);
          const curCategoryCount = this.getCategoryCount(cur);
          return prev.length === cur.length && prevCategoryCount === curCategoryCount;
        }),
        tap(items => {
          if (items.length) {
            this.loaded = true;
          }
        }),
        untilComponentDestroyed(this)
      )
      .subscribe(data => {
        this.images = data.sort((a, b) => a.views > b.views ? -1 : a.views < b.views ? 1 : 0)
          .filter((item, index) => index < 4)
          .map(item => {
            return {
              ...item,
              mainFile: this.getMainFileVersions(item.files)
            }
          });


        this.cd.markForCheck();
      });
  }

  ngOnDestroy() {
  }

  getCategoryCount(images: ImageFullModel[]) {
    const res = images.filter(image => image && image.category && image.category.id);
    if (res && res.length) {
      return res.length;
    }

    return 0;
  }

  getMainFileVersions(files: FileModel[]): VersionsModel {
    return this.imagesHelperService.getMainFileVersions(files);
  }
}
