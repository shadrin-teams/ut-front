import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { untilComponentDestroyed } from '@wt/core/utils';
import { MainFacade } from '@wt/core/+state/main';

@Component({
  selector: 'wt-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit, OnDestroy {
  title = 'Uzbekistan Tours';
  description = '';
  subTitle = '';
  image = '';

  constructor(private mainFacade: MainFacade,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.loadMetaData();
  }

  // eslint-disable-next-line @angular-eslint/no-empty-lifecycle-method
  ngOnDestroy():void {
  }

  private loadMetaData(): void {
    this.mainFacade.getMetadata()
      .pipe(
        untilComponentDestroyed(this)
      )
      .subscribe(metadata => {
        if (metadata) {
          this.title = metadata.title || 'Туристический Узбекистан';
          this.subTitle = metadata.subTitle || 'Uzbekistan-tours';
          this.cd.markForCheck();
        }
      });
  }
}
