import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'wt-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerComponent implements OnInit {
  @Input() overlay = false;
  @Input() diameter = 30;
  @Input() color: 'blue' | 'grey' = 'blue';
  constructor() { }

  ngOnInit() {
  }
}
