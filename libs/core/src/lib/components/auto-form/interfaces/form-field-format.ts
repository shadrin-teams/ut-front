export enum IFormFieldFormat {
  Date = 'date',
  Email = 'email',
  Phone = 'phone',
  Number = 'number',
}
