import { IFormFieldFormat } from '@wt/core/components/auto-form/interfaces/form-field-format';
import { FieldType } from '@wt/core/components/auto-form/interfaces/field-type';
import { IFormFieldOption } from '@wt/core/components/auto-form/interfaces/form-field-option';


export interface IFormField {
  field: string;
  title: string;
  type?: FieldType;
  value?: any;
  loading?: boolean;
  require?: boolean;
  format?: IFormFieldFormat;
  notEditable?: boolean;
  options?: IFormFieldOption[];
  multiple?: boolean;
  hide?: boolean;
  loadOptionFromStore?: string;
  asyncValidators?: string[];
  groupName?: string;
}
