export interface IFormState {
  isSubmitted: boolean;
  error: string
}
