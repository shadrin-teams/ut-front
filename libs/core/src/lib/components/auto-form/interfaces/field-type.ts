export enum FieldType {
  FieldTypeInput = 'input',
  FieldTypeSelect = 'select',
  FieldTypeSelectMultiple = 'select-multiple',
  FieldTypeSlideToggle = 'slide-toggle',
  FieldTypeTextarea = 'textarea-editor',
  FieldTypeTextareaEditor = 'textarea',
  FieldTypeFile = 'file',
  FieldTypeFiles = 'files',
}
