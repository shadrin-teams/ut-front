import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output
} from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';
import { FieldType } from '@wt/core/components/auto-form/interfaces/field-type';
import { InputType } from '@wt/core/components/auto-form/interfaces/input-type';
import { CORE_CONFIG } from '@wt/core/tokens';
import { AppConfig } from '@wt/models/config';

@Component({
  selector: 'shared-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormFieldComponent {
  @Input() fieldData: IFormField;
  @Input() form: FormGroup;
  @Input() editorImageUploadUrl = '';
  @Input() errorMessages: { [key: string]: string };
  @Output() eventUpdateForm = new EventEmitter<any>();
  @Output() eventSendAction = new EventEmitter<any>();

  editorConfig = {};
  fieldTypes = FieldType;
  field: string;
  fieldControl: AbstractControl;
  inputType: InputType = InputType.Text;
  invalidStatus = 'INVALID';
  private unSubscribeSubject$ = new Subject();

  constructor(@Inject(CORE_CONFIG) private config: AppConfig,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.field = this.fieldData.field;
    this.fieldControl = this.form.get(this.field);

    this.setEditorConfig();
    this.subscribeToChangeStatus();
  }

  private subscribeToChangeStatus(): void {
    this.form.get(this.field).statusChanges
      .pipe(
        takeUntil(this.unSubscribeSubject$)
      )
      .subscribe((data) => {
        this.cd.markForCheck();
      });
  }

  private setEditorConfig(): void {
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '100px',
      maxHeight: '300px',
      width: '100%',
      minWidth: '200px',
      placeholder: 'Enter text here...',
      uploadUrl: `${this.config.apiUrl}/api/files${this.editorImageUploadUrl}`,
      sanitize: true,
      toolbarPosition: 'top'
    };
  }

  ngOnDestroy() {
    this.unSubscribeSubject$.next();
    this.unSubscribeSubject$.complete();
  }
}
