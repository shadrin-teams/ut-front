import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output
} from '@angular/core';
// import { CORE_CONFIG } from '@shared/core/tokens';
import { AbstractControl, FormGroup } from '@angular/forms';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';
import { FieldType } from '@wt/core/components/auto-form/interfaces/field-type';
import { InputType } from '@wt/core/components/auto-form/interfaces/input-type';
import { CORE_CONFIG } from '@wt/core/tokens';
import { AppConfig } from '@wt/models/config';
import { IFormFieldFormat } from '@wt/core/components/auto-form/interfaces/form-field-format';

@Component({
  selector: 'shared-form-field-show',
  templateUrl: './form-field-show.component.html',
  styleUrls: ['./form-field-show.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldShowComponent {
  @Input() fieldData: IFormField;
  @Input() form: FormGroup;

  editorConfig = {};
  fieldTypes = FieldType;
  field: string;
  fieldControl: AbstractControl;
  inputType: InputType = InputType.Text;
  fieldValue;

  constructor(@Inject(CORE_CONFIG) private config: AppConfig) {
  }

  ngOnInit() {
    this.setInputType();
    this.field = this.fieldData.field;
    this.fieldControl = this.form.get(this.field);
    this.fieldValue = this.fieldControl.value;
  }

  setInputType() {
    switch (this.fieldData.format) {
      case IFormFieldFormat.Email:
        this.inputType = InputType.Email;
        break;
      case IFormFieldFormat.Phone:
        this.inputType = InputType.Tel;
        break;
      default:
        this.inputType = InputType.Text;
    }
  }
}
