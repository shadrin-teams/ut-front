import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnChanges,
  OnInit, Output,
  SimpleChanges
} from '@angular/core';
import { CORE_CONFIG } from '@wt/core/tokens';
import { AppConfig } from '@wt/models/config';
// import { AngularEditorConfig } from '@kolkov/angular-editor';


export type FieldType =
  'input' |
  'select' |
  'select-multiple' |
  'slide-toggle' |
  'textarea-editor' |
  'textarea';

export interface IFormFieldOption {
  id: number;
  name: string;
}

export const FieldTypeInput = 'input';
export const FieldTypeSelect = 'select';
export const FieldTypeSelectMultiple = 'select-multiple';
export const FieldTypeSlideToggle = 'slide-toggle';
export const FieldTypeTextarea = 'textarea';
export const FieldTypeTextareaEditor = 'textarea-editor';

@Component({
  selector: 'shared-form-field-store-state',
  templateUrl: './form-field-store-state.component.html',
  styleUrls: ['./form-field-store-state.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldStoreStateComponent implements OnInit, OnChanges {
  @Input() title: string;
  @Input() type: FieldType;
  @Input() field: string;
  @Input() loading: boolean;
  @Input() formId: string;
  @Input() state: any;
  @Input() options: IFormFieldOption[] = [];
  @Input() isRequired = false;
  @Input() editorImageUploadUrl = '';
  @Output() eventUpdateForm = new EventEmitter<any>();
  @Output() eventSendAction = new EventEmitter<any>();

  modelControl: string | boolean = '';
  stateControl;
  editorConfig = {};

  constructor(@Inject(CORE_CONFIG) private config: AppConfig) {
  }

  ngOnInit() {
    this.editorConfig = {
      editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '100px',
      maxHeight: '300px',
      width: '100%',
      minWidth: '200px',
      placeholder: 'Enter text here...',
      uploadUrl: `${this.config.apiUrl}/api/files${this.editorImageUploadUrl}`,
      sanitize: true,
      toolbarPosition: 'top'
    };

    if (this.state.controls && this.state.controls[this.field]) {
      this.modelControl = this.state.controls[this.field].value;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.state.currentValue) {
      this.stateControl = changes.state.currentValue['controls'][this.field];
    }
  }

  changeField({ value, field }) {
    this.modelControl = value;
    const params/*: UpdateFormPayload*/ = {
      formId: this.formId,
      field,
      value
    };

    this.eventUpdateForm.emit(params);
    // this.formsFacade.updateForm(params);
  }

  editorChange(description: string) {
    this.changeField({ value: description, field: this.field });
  }

  sendAction(field, action) {
    const params = {
      formId: this.formId,
      field: this.field,
      action
    };

    this.eventSendAction.emit(params);
    // this.formsFacade.sendAction(params);
  }

  openedChange({ value, field }) {
    if (!value && (
      (!Array.isArray(this.modelControl) && !this.modelControl) ||
      (Array.isArray(this.modelControl) && !this.modelControl.length)
    )) {
      this.sendAction(field, 'MARK_AS_TOUCHED');
    }
  }
}
