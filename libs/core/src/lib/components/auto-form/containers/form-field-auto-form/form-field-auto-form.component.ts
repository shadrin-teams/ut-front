import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { filter } from 'rxjs/internal/operators/filter';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';

interface FormFieldGroup extends IFormField {
  showGroup?: boolean;
  hasGroup?: boolean;
}

@Component({
  selector: 'shared-form-field-auto-form',
  templateUrl: './form-field-auto-form.component.html',
  styleUrls: ['./form-field-auto-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldAutoFormComponent implements OnInit, OnDestroy {
  @Input() fields: IFormField[];
  @Input() formId: string;
  @Input() form: FormGroup;
  @Input() editorImageUploadUrl = '';
  @Input() errorMessages: { [key: string]: string };
  @Output() changeForm = new EventEmitter<{ [field: string]: string }>();

  value: any;
  groupedFields: FormFieldGroup[];
  unSubscribeSubject$ = new Subject();

  constructor(/*private formErrors: FormErrorsFacade,
              */private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.groupFields();
    this.subscribeToFormErrors();
  }


  ngOnDestroy() {
    this.unSubscribeSubject$.next();
    this.unSubscribeSubject$.complete();
  }

  private groupFields() {
    this.groupedFields = [];
    let currentGroup;
    this.fields.filter(formField => !formField.notEditable)
      .forEach(formField => {
        const newFormField: FormFieldGroup = { ...formField };

        if (formField.groupName && formField.groupName !== currentGroup) {
          newFormField.showGroup = true;
        }

        if (formField.groupName) {
          newFormField.hasGroup = true;
        }

        this.groupedFields.push(newFormField);
        currentGroup = formField.groupName;
      });
  }

  truckByFieldName(item: IFormField) {
    return item.field;
  }

  private subscribeToFormErrors() {
    /*this.formErrors.getFormErrors(this.formId)
      .pipe(
        takeUntil(this.unSubscribeSubject$),
        filter((errors: FormError[]) => !!(errors && errors.length))
      )
      .subscribe((errors: FormError[]) => {
        console.log('errors', errors, this.form);
        if (errors && errors.length) {
          errors.forEach(error => {
            const { field, code } = error;
            if (field && code) {
              const control = this.form.get(field);
              if (control) {
                console.log('errors+++', errors);
                control.setErrors({ [code]: code });
                control.markAsDirty();
                control.markAsTouched();
              }
            }
          });
          this.cd.markForCheck();
        }
      });*/
  }

}
