import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { NgrxFormsModule } from 'ngrx-forms';
// import { AngularEditorModule } from '@kolkov/angular-editor';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexModule } from '@angular/flex-layout';
import { NgxMaskModule } from 'ngx-mask';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';

import { FormFieldComponent } from './containers/form-field/form-field.component';
import { FormFieldSpinnerComponent } from './components/form-field-spinner/form-field-spinner.component';
import { FormFieldSelectComponent } from './components/form-field-select/form-field-select.component';
import { FormFieldSlideToggleComponent } from './components/form-field-slide-toggle/form-field-slide-toggle.component';
import { FormFieldAutoFormComponent } from './containers/form-field-auto-form/form-field-auto-form.component';
import { FormFieldShowAutoFormComponent } from './containers/form-field-show-auto-form/form-field-show-auto-form.component';
import { FormFieldShowComponent } from './containers/form-field-show/form-field-show.component';
import { FormFieldSelectShowComponent } from './components/form-field-select-show/form-field-select-show.component';
import { FormFieldInputComponent } from './components/form-field-input/form-field-input.component';
import { FormMessageBoxComponent } from './components/form-message-box/form-message-box.component';
import { FormFieldFileComponent } from './components/form-field-file/form-field-file.component';
import { FormFieldFilesComponent } from './components/form-field-files/form-field-files.component';
import { SpinnerModule } from '@wt/core/components/spinner/spinner.module';
import { FormFieldErrorModule } from '@wt/core/components/form-field-error/form-field-error.module';
import { CKEditorModule } from 'ckeditor4-angular';

@NgModule({
  declarations: [
    FormFieldComponent,
    FormFieldShowComponent,
    FormFieldSpinnerComponent,
    FormFieldSelectComponent,
    FormFieldSelectShowComponent,
    FormFieldSlideToggleComponent,
    FormFieldAutoFormComponent,
    FormFieldShowAutoFormComponent,
    FormFieldShowComponent,
    FormFieldInputComponent,
    FormMessageBoxComponent,
    FormFieldFileComponent,
    FormFieldFilesComponent
  ],
  imports: [
    CommonModule,
    FormFieldErrorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatSelectModule,
    // AngularEditorModule,
    FormsModule,
    SpinnerModule,
    ReactiveFormsModule,
    FlexModule,
    NgxMaskModule,
    MatIconModule,
    MatTooltipModule,
    CKEditorModule
  ],
  exports: [
    FormFieldAutoFormComponent,
    FormFieldShowAutoFormComponent
  ]
})
export class AutoFormModule {
}
