import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';

@Component({
  selector: 'shared-form-field-slide-toggle',
  templateUrl: './form-field-slide-toggle.component.html',
  styleUrls: ['./form-field-slide-toggle.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldSlideToggleComponent {
  @Input() fieldData: IFormField;
  @Input() form: FormGroup;
}
