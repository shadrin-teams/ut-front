import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';
import { CORE_CONFIG } from '@wt/core/tokens';
import { AppConfig } from '@wt/models/config';

@Component({
  selector: 'shared-form-field-files',
  templateUrl: './form-field-files.component.html',
  styleUrls: ['./form-field-files.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldFilesComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @Input() fieldData: IFormField;
  @Input() form: FormGroup;

  @ViewChild('input', { static: false }) input: ElementRef<HTMLInputElement>;

  unSubscribeSubject$ = new Subject();
  imageSource: string | ArrayBuffer;
  inputStatus = '1';
  publicUrl = this.config.publicUrl;

  constructor(private cd: ChangeDetectorRef,
              @Inject(CORE_CONFIG) private config: AppConfig) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      const field = this.fieldData.field;
      if (field && this.form) {
        this.imageSource = this.getControlValue() || '';

        if (this.imageSource) {
          this.inputStatus = '0';
        }
      }
    }
  }

  ngOnDestroy() {
    this.unSubscribeSubject$.next();
    this.unSubscribeSubject$.complete();
  }

  ngAfterViewInit() {
    this.form.get(this.fieldData.field).statusChanges
      .pipe(
        distinctUntilChanged(),
        takeUntil(this.unSubscribeSubject$)
      )
      .subscribe(() => {
        this.cd.markForCheck();
      });
  }

  private getControlValue(): string {
    const field = this.fieldData.field;
    let value: string;
    const controlValue = this.form.get(field).value;
    if (typeof controlValue === 'object') {
      if (controlValue[2]) {
        value = controlValue[2];
      }
    } else value = controlValue;

    return value;
  }

  onFileChange(event): void {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get(this.fieldData.field).setValue(file);
      const reader = new FileReader();

      reader.readAsDataURL(file);
      reader.onload = (_event) => {
        this.imageSource = reader.result;
        this.inputStatus = '0';
        this.cd.markForCheck();
      }
    }
  }

  deleteImage(): void {
    this.form.get(this.fieldData.field).setValue('');
    this.input.nativeElement.value = '';
    this.imageSource = '';
    this.inputStatus = '1';
    this.cd.markForCheck();
  }
}
