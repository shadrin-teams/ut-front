import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormFieldFileComponent } from './form-field-input.component';

describe('FormFieldInputComponent', () => {
  let component: FormFieldFileComponent;
  let fixture: ComponentFixture<FormFieldFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormFieldFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormFieldFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
