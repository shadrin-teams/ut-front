import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { createFeatureSelector, select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';
import { CoreState } from '@wt/core/+state/core.reducer';

interface Option {
  id: number;
  name: string;
}

@Component({
  selector: 'shared-form-field-select',
  templateUrl: './form-field-select.component.html',
  styleUrls: ['./form-field-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldSelectComponent implements OnChanges, OnDestroy {
  @Input() fieldData: IFormField;
  @Input() form: FormGroup;

  loaded = false;
  options: Option[] = [];
  private unSubscribeSubject$ = new Subject();

  constructor(protected store: Store<CoreState>,
              private cd: ChangeDetectorRef) {
  }

  ngOnDestroy() {
    this.unSubscribeSubject$.next();
    this.unSubscribeSubject$.complete();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.fieldData && changes.fieldData.currentValue) {
      const { loadOptionFromStore, options } = changes.fieldData.currentValue;

      if (options && options.length) {
        this.options = options;
      }

      if (loadOptionFromStore) {
        this.loadOptionsFromStore(loadOptionFromStore);
      } else {
        this.loaded = true;
      }
    }
  }

  private loadOptionsFromStore(loadOptionFromStore: string): void {
    const featureSelector = createFeatureSelector(loadOptionFromStore);
    this.store.pipe(select(featureSelector))
      .pipe(
        takeUntil(this.unSubscribeSubject$)
      )
      .subscribe((data: {list: Option[], ids: number[]}) => {
        if (data.list && data.list.length) {
          this.options = data.list;
          this.cd.markForCheck();
        }
        this.fieldData.loading = false;
      });
  }

}
