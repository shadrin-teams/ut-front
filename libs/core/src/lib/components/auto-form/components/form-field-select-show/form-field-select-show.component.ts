import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';
import { IFormFieldOption } from '@wt/core/components/auto-form/interfaces/form-field-option';

@Component({
  selector: 'shared-form-field-select-show',
  templateUrl: './form-field-select-show.component.html',
  styleUrls: ['./form-field-select-show.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldSelectShowComponent implements OnChanges {
  @Input() fieldData: IFormField;
  @Input() form: FormGroup;

  selectedValues: string[] = [];

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.fieldData.currentValue) {
      const params = this.form.get(this.fieldData.field).value;
      let values = !Array.isArray(params) ? [params] : params;

      this.setSelectedOptions(this.fieldData.options, values);
    }
  }

  trackById(item: IFormFieldOption) {
    return item.id;
  }

  private setSelectedOptions(options: IFormFieldOption[], values: number[]): void {
    this.selectedValues = options.filter(item => values.includes(item.id))
      .map(item => item.name);
  }
}
