import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OtherImagesComponent } from './other-images.component';



@NgModule({
    declarations: [OtherImagesComponent],
    exports: [
        OtherImagesComponent
    ],
    imports: [
        CommonModule
    ]
})
export class OtherImagesModule { }
