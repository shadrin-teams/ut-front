import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '@wt/core/services/auth/authentication.service';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {
  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // console.log('intercept');
    // add authorization header with jwt token if available
/*    request = request.clone({
      setHeaders: {
        Origin: `test test`
      }
    });*/
    return next.handle(request);
  }
}
