import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { LoadImagesResolver } from './resolvers/load-images.resolver';

import { CategoryLoadImagesResolver } from './resolvers/category-load-images.resolver';
import { LoadCategoryGuard } from './guards/load-category.guard';
import { TagLoadImagesResolver } from './resolvers/tag-load-images.resolver';
import { LoadTagGuard } from './guards/load-tag.guard';
import { NotFoundComponent } from './containers/not-found/not-found.component';
import { WtWebTemplateComponent } from './containers/wt-web-template/wt-web-template.component';


export const wtWebTemplateRoutes: Route[] = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        resolve: {
          loadImages: LoadImagesResolver
        },
        component: WtWebTemplateComponent
      },
      {
        path: ':slug',
        resolve: {
          loadImages: CategoryLoadImagesResolver
        },
        canActivate: [LoadCategoryGuard],
        component: WtWebTemplateComponent
      },
      {
        path: 'tag/:slug',
        resolve: {
          loadImages: TagLoadImagesResolver
        },
        canActivate: [LoadTagGuard],
        component: WtWebTemplateComponent
      },
      {
        path: ':category/:slug',
        loadChildren: () => import('@wt/web-image').then(m => m.WebImageModule)
      },
      {
        path: 'not-found',
        component: NotFoundComponent
      },
      {
        path: '404',
        component: NotFoundComponent
      },
      {
        path: '**',
        redirectTo: '/not-found'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(wtWebTemplateRoutes)],
  exports: [RouterModule],
  providers: [
    LoadImagesResolver,
    LoadCategoryGuard,
    CategoryLoadImagesResolver,
    TagLoadImagesResolver,
    LoadTagGuard
  ]
})
export class WtWebTemplateRoutingModule {
}
