import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ToasterModule } from 'angular2-toaster';

import { CoreModule } from '@wt/core';
import { AppConfig } from '@wt/models/config';
import { CORE_CONFIG } from '@wt/core/tokens';
import { ImageItemComponent } from './componets/image-item/image-item.component';
import { ImageListComponent } from './containers/image-list/image-list.component';
import { CountFormatModule } from '@wt/core/pipes/count-format/count-format.module';
import { DateFormatModule } from '@wt/core/pipes/date-format/date-format.module';
import { SpinnerModule } from '@wt/core/components/spinner/spinner.module';
import { NotFoundComponent } from './containers/not-found/not-found.component';
import { PopularPostsModule } from '@wt/core/components/popular-posts/popular-posts.module';
import { PostCategoriesModule } from '@wt/core/components/post-categories/post-categories.module';
import { TagCloudModule } from '@wt/core/components/tag-cloud/tag-cloud.module';
import { HeaderModule } from '@wt/core/components/header/header.module';
import { OtherImagesModule } from '@wt/core/components/other-images/other-images.module';
import { StickySidebarModule } from '@wt/core/components/sticky-sidebar';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { WindowRef } from '../../../../WindowRef';
import { HeadersInterceptor } from './interceptors/headers.interceptor';
import { WtWebTemplateComponent } from './containers/wt-web-template/wt-web-template.component';
import { MainComponent } from './main/main.component';
import { WtWebTemplateRoutingModule } from './wt-web-template-routing.module';
import { ImagesHelperService } from '@wt/core/services/images/images-helper.service';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    CoreModule.forRoot(),
    BrowserAnimationsModule,
    ToasterModule.forRoot(),
    WtWebTemplateRoutingModule,
    CountFormatModule,
    DateFormatModule,
    SpinnerModule,
    InfiniteScrollModule,
    PopularPostsModule,
    PostCategoriesModule,
    TagCloudModule,
    HeaderModule,
    OtherImagesModule,
    StickySidebarModule
  ],
  exports: [CountFormatModule],
  declarations: [
    WtWebTemplateComponent,
    ImageItemComponent,
    ImageListComponent,
    NotFoundComponent,
    MainComponent,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HeadersInterceptor, multi: true },
    WindowRef,
    ImagesHelperService
  ]
})
export class WtWebTemplateModule {
  static forApp(appConfig: AppConfig): ModuleWithProviders<WtWebTemplateModule> {
    return {
      ngModule: WtWebTemplateModule,
      providers: [
        { provide: CORE_CONFIG, useValue: appConfig }
      ]
    };
  }
}
