import { ChangeDetectionStrategy, Component, Inject, Input, OnInit } from '@angular/core';
import { ImageFullModel } from '@wt/models/image';
import { CategoryModel } from '@wt/models/category';
import { CORE_CONFIG } from '@wt/core/tokens';
import { AppConfig } from '@wt/models/config';

@Component({
  selector: 'wt-image-item',
  templateUrl: './image-item.component.html',
  styleUrls: ['./image-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageItemComponent implements OnInit {
  @Input() image: ImageFullModel;
  @Input() category: CategoryModel;
  mainImage = '';

  constructor() {
  }

  ngOnInit() {
    if (this.image.files && this.image.files.length) {
      const findMain = this.image.files.find(item => !!item.main);
      if (findMain) {
        this.mainImage = findMain.versions[2];
      } else {
        this.mainImage = this.image.files[0].versions[2];
      }
    }
  }

}
