import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { filter, first } from 'rxjs/operators';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { untilComponentDestroyed } from '@wt/core/utils';
import { CategoriesFacade } from '@wt/core/+state/categories';
import { HashtagsFacade } from '@wt/core/+state/hashtags';
import { MainFacade } from '@wt/core/+state/main';


@Component({
  selector: 'wt-web-template',
  templateUrl: './wt-web-template.component.html',
  styleUrls: ['./wt-web-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WtWebTemplateComponent implements OnInit, OnDestroy {
  categoryId: number;
  tagId: number;
  mainTitle = 'Uzbekistan Tours';
  pageData = {
    title: this.mainTitle,
    subTitle: '',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.',
    image: ''
  };
  loaded = false;

  constructor(private categoriesFacade: CategoriesFacade,
              private hashtagsFacade: HashtagsFacade,
              private route: ActivatedRoute,
              private router: Router,
              private cd: ChangeDetectorRef,
              private mainFacade: MainFacade) {
  }

  ngOnInit() {
    this.loadCategoryAndTags();
  }

  // eslint-disable-next-line @angular-eslint/no-empty-lifecycle-method
  ngOnDestroy(): void {
  }

  private loadCategoryAndTags(): void {
    this.route.params
      .pipe(
        untilComponentDestroyed(this)
      )
      .subscribe((params: Params) => {
        const slug = this.getUrl(params.slug);
        const isTag = params && params.slug && params.slug.indexOf('.html') !== -1;
        if (slug) {
          if (!isTag) {
            this.getCategory(slug);
          } else {
            this.getTag(slug);
          }
        }
      });
  }

  private getTag(slug): void {
    this.hashtagsFacade.getHashtags(slug)
      .pipe(
        filter(item => !!item && !!item.id),
        first(),
        untilComponentDestroyed(this)
      )
      .subscribe(tag => {
          this.pageData = {
            title: tag.name,
            subTitle: this.mainTitle,
            description: tag.description,
            image: tag.image
          };
          this.tagId = tag.id;
          this.loaded = true;
          this.mainFacade.setMetadata({title: tag.name, description: tag.description});
          this.cd.markForCheck();
        }
      );
  }

  private getCategory(slug): void {
    this.categoriesFacade.getCategory(slug)
      .pipe(
        filter(item => !!item && !!item.id),
        first(),
        untilComponentDestroyed(this)
      )
      .subscribe(category => {
        this.pageData = {
            title: category.name,
            subTitle: this.mainTitle,
            description: category.description,
            image: category.image
          };
          this.categoryId = category.id;
          this.loaded = true;

          this.mainFacade.setMetadata({title: category.name, description: category.description});
          this.cd.markForCheck();
        }
      );
  }

  private getUrl(slug): string {
    if (slug) {
      const patern = /^(?:\/)?((?:[\d\w-]*)+)(?:\.html)?$/gi;
      const params = patern.exec(slug);
      return params[1];
    }
  }
}
