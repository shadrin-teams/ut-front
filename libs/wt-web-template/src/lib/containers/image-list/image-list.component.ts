import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  PLATFORM_ID, Inject
} from '@angular/core';
import { ImagesFacade } from '@wt/core/+state/images';
import { Observable } from 'rxjs';
import { ImageFieldNames, ImageFullModel, ImageModel } from '@wt/models/image';
import { distinctUntilChanged } from 'rxjs/operators';
import { untilComponentDestroyed } from '@wt/core/utils';
import { isPlatformBrowser } from '@angular/common';
import { WindowRef } from '../../../../../../WindowRef';


@Component({
  selector: 'wt-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageListComponent implements OnInit, OnDestroy, OnChanges {
  @Input() categoryId: number;
  @Input() tagId: number;
  images$: Observable<ImageFullModel[]>;
  loaded$: Observable<boolean>;
  page = 1;
  perPage = 10;
  currentCountItems = 0;

  constructor(private imagesFacade: ImagesFacade,
              @Inject(PLATFORM_ID) private platformId: any,
              private window: WindowRef
              /*,
              private categoriesFacade: CategoriesFacade,
              private hashtagsFacade: HashtagsFacade,
              private route: ActivatedRoute,
              private router: Router*/
  ) {
  }

  ngOnInit() {
    this.loaded$ = this.imagesFacade.loaded$;
    this.loadImages();
  }

  loadImages() {
    this.images$ = this.getImages()
      .pipe(
        distinctUntilChanged((prev, cur) => {
          const prevTagCount = this.getTagsCount(prev);
          const curTagCount = this.getTagsCount(cur);
          const prevCategoryCount = this.getCategoryCount(prev);
          const curCategoryCount = this.getCategoryCount(cur);

          return prev.length === cur.length && prevTagCount === curTagCount && prevCategoryCount === curCategoryCount;
        }),
        untilComponentDestroyed(this)
      );
  }

  getImages(): Observable<ImageModel[]> {
    if (this.categoryId) {
      return this.imagesFacade.getImageByCategory(this.categoryId);
    }

    if (this.tagId) {
      return this.imagesFacade.getImageByTag(this.tagId);
    }

    return this.imagesFacade.getFullImages$;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.categoryId || changes.tagId) {
      if(isPlatformBrowser(this.platformId)) {
        this.window.nativeWindow().scroll(0, 0);
      }
      this.loadImages();
    }
  }

  getTagsCount(images: ImageFullModel[]): number {
    let count = 0;

    if (images && images.length) {
      images.forEach(items => {
        if (items.tags) {
          count += items.tags.length;
        }
        if (items.tagIds) {
          count += items.tagIds.length;
        }
      });
    }
    return count;
  }

  getCategoryCount(images: ImageFullModel[]): number {
    let count = 0;

    if (images && images.length) {
      images.forEach(items => {
        if (items.category && items.category.id) {
          count++;
        }
      });
    }
    return count;
  }

  ngOnDestroy(): void {
  }

  getUrl(slug) {
    if (slug) {
      const patern = /^(?:\/)?((?:.*)+)(?:\.html)$/gi;
      const params = patern.exec(slug);
      return params[1];
    }
  }

  onScrollDown() {
    this.page++;
    // this.currentCountItems = this.images ? this.images.length : 0;
    const where = { [ImageFieldNames.IS_PUBLIC]: true };
    if (this.categoryId) {
      where['category_id'] = this.categoryId;
    }
    this.imagesFacade.loadImages({
      where,
      fields: this.imagesFacade.fields2,
      paging: {
        page: this.page,
        perPage: this.perPage
      },
      tagId: this.tagId
    });
  }
}
