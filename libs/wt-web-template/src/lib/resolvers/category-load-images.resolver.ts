import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {first, switchMap} from 'rxjs/operators';
import {ImagesFacade} from '@wt/core/+state/images';
import {ImageFieldNames} from '@wt/models/image';
import {CategoryFieldNames, CategoryModel} from '@wt/models/category';
import {HashtagFieldNames} from '@wt/models/hashtag';
import {CategoriesFacade} from '@wt/core/+state/categories';

@Injectable()
export class CategoryLoadImagesResolver implements Resolve<Observable<boolean>> {
  constructor(private imagesFacade: ImagesFacade,
              private categoriesFacade: CategoriesFacade
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const url = state.url;
    const patern = /^(?:.*)(?:\/)((.*)+)?/gi;
    const params = patern.exec(url);
    const slug = params[1];
    return this.getFromStoreOrAPI(slug);
  }

  getFromStoreOrAPI(slug): Observable<boolean> {
    console.log('getFromStoreOrAPI');
    const where = {
    };
    const fields = {
      fields: [
        ImageFieldNames.TITLE,
        ImageFieldNames.DESCRIPTION,
        ImageFieldNames.SLUG,
        ImageFieldNames.CATEGORY_ID,
        ImageFieldNames.CREATEDAT,
        ImageFieldNames.VIEWS
      ],
      category: {
        fields: [CategoryFieldNames.NAME],
        count: {}
      },
      tags: {
        fields: [HashtagFieldNames.NAME]
      },
      files: {
        fields: ['file', 'folder'],
        versions: {}
      },
      paging: {
        page: 1,
        perPage: 100
      }
    };
    return this.categoriesFacade.getCategory(slug)
      .pipe(
        switchMap((category: CategoryModel) => {
          where['category_id'] = category.id;
          this.imagesFacade.loadImages({where, fields});
          // window.scrollTo(0, 0);
          return this.imagesFacade.waitLoading();
        }),
        first()
      );
  }
}
