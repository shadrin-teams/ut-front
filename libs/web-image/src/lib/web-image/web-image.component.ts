import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  PLATFORM_ID
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, switchMap, tap } from 'rxjs/operators';

import { ImageFullModel } from '@wt/models/image';
import { ImagesFacade } from '@wt/core/+state/images';
import { IBreadCrumpsItem } from '@wt/core/components/bread-crumbs/bread-crumps.component';
import { FileModel, VersionsModel } from '@wt/models/file';
import { isPlatformBrowser } from '@angular/common';
import { WindowRef } from '../../../../../WindowRef';
import { ImagesHelperService } from '@wt/core/services/images/images-helper.service';
import { MainFacade } from '@wt/core/+state/main';
import { MetaDataModel } from '@wt/core/models';

@Component({
  selector: 'wt-web-image',
  templateUrl: './web-image.component.html',
  styleUrls: ['./web-image.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WebImageComponent implements OnInit {
  image$: Observable<ImageFullModel | undefined>;
  files: FileModel[];
  breadCrumpsList: IBreadCrumpsItem[] = [];
  mainFile: VersionsModel;
  activeImageId: number;

  constructor(private imagesFacade: ImagesFacade,
              private route: ActivatedRoute,
              private router: Router,
              @Inject(PLATFORM_ID) private platformId: any,
              private window: WindowRef,
              private imagesHelperService: ImagesHelperService,
              private cd: ChangeDetectorRef,
              private mainFacade: MainFacade) {
  }

  ngOnInit() {
    this.loadImage();
  }

  loadImage() {
    this.image$ = this.route.params
      .pipe(
        switchMap((params: Params) => {
          const slug = this.getSlug(this.router.url);
          this.scrollTop();
          return this.imagesFacade.getFullImage(slug);
        }),
        filter(item => !!item && !!item['id']),
        tap((image: ImageFullModel) => {

          this.files = [...image.files];
          if (!this.mainFile || this.activeImageId !== image.id) {
            this.mainFile = this.getMainFileVersions(this.files);
          }

          this.activeImageId = image.id;
          this.breadCrumpsList = [{ title: 'Главная', url: '/' }];
          if (image.category) {
            this.breadCrumpsList.push({
              title: image.category.name,
              url: `/${image.category.slug}`
            });
            this.breadCrumpsList.push({ title: image.title });
          }

          const metaData: MetaDataModel = {
            title: `${image.title}${image.category ? ' ' + image.category.name : ''}`
          };
          this.mainFacade.setMetadata(metaData);
        })
      );
  }

  private scrollTop(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.window.nativeWindow().scroll(0, 0);
    }
  }

  getSlug(url: string): string {
    const patern = /^\/(?:[a-z-0-9_]*)\/((?:[a-z-_0-9]*)+)(?:\.html)$/gi;
    const params = patern.exec(url);
    if (params && params[1]) {
      return params[1];
    }
    return null;
  }

  openLightBox(index: number) {
    const selectedImage = this.files.find((image, imageIndex) => imageIndex === index);
    if (selectedImage) {
      this.mainFile = { ...selectedImage.versions };
      this.cd.markForCheck();
    }

    /*    const listImages = this.files.map((item) => {
          return {
            src: item.versions[3],
            caption: item.title,
            thumb: item.versions[1]
          };
        });*/

    /*    const imageItem = {
          src: this.versions[3],
          caption: this.title,
          thumb: this.versions[this.version]
        };*/
    /*
        this._lightbox.open(listImages, index);*/
  }

  getMainFileVersions(files: FileModel[]): VersionsModel {
    return this.imagesHelperService.getMainFileVersions(files);
  }
}
