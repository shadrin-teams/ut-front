import { ListOtherImagesComponent } from './list-other-images/list-other-images.component';
import { NextImagesComponent } from './next-images/next-images.component';

export const components = [
  ListOtherImagesComponent,
  NextImagesComponent
];
