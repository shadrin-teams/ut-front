import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {FileModel} from '@wt/models/file';

@Component({
  selector: 'wt-list-other-images',
  templateUrl: './list-other-images.component.html',
  styleUrls: ['./list-other-images.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListOtherImagesComponent implements OnInit {
  @Input() list: FileModel[] = [];
  @Output() clickEvent = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  clickHandler(index: number) {
    this.clickEvent.emit(index);
  }

}
