import { Route, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { WebImageComponent } from './web-image/web-image.component';
import { CheckImageGuard } from './guards/check-image.guard';
import { LoadImageResolver } from './resolvers/load-image.resolver';


export const imageRouting: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: WebImageComponent,
    canActivate: [CheckImageGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(imageRouting)],
  exports: [RouterModule],
  providers: [LoadImageResolver, CheckImageGuard]
})
export class WebImageRoutingModule {
}
