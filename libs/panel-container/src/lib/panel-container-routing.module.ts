import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { PanelContainerComponent } from './panel-container.component';

// import { PanelComponent } from '@wt/panel-container/src/lib/panel.component';
// import { HomeComponent } from '@wt/panel-container/src/lib/containers/home/home.component';

export const routes: Route[] = [
/*  {
    path: '', component: PanelComponent,
    children: [
      { path: '', redirectTo: 'auth', pathMatch: 'full' },
    ],
  },*/
//  { path: 'home', component: HomeComponent },
  { path: '', component: PanelContainerComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PanelContainerRoutingModule {
}
