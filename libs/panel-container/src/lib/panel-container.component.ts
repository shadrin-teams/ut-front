import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@wt/core/services/auth/authentication.service';
import { UserModel } from '@wt/models/user';

import { UsersFacade } from '@wt/core/+state/users';
import { untilComponentDestroyed } from '@wt/core/utils';

@Component({
  selector: 'wt-panel-container',
  templateUrl: './panel-container.component.html'
})
export class PanelContainerComponent implements OnInit, OnDestroy {
  currentUser: UserModel;

  constructor(private router: Router,
              private usersFacade: UsersFacade,
              private authenticationService: AuthenticationService,
              ) {
  }

  ngOnInit() {
    this.authenticationService.currentUser
      .pipe(
        untilComponentDestroyed(this)
      )
      .subscribe((currentUser: UserModel) => {
        this.currentUser = currentUser;
      });
  }

  ngOnDestroy() {
  }

  testFunction() {
    // this.toursFacade.loaded();
  }

  logout() {
    // UsersFacade.logOut();
    this.router.navigate(['/login']);
  }
}
