import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'wt-panel-layout',
  templateUrl: './panel-layout.component.html',
  styleUrls: ['./panel-layout.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PanelLayoutComponent implements OnInit{
  ngOnInit(): void {
  }
}

