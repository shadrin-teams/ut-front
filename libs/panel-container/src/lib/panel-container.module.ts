import { NgModule } from '@angular/core';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

import { FuseProgressBarModule, FuseSidebarModule, FuseWidgetModule } from '@fuse/components';
import { FuseSharedModule } from '@wt/fuse';
import { PanelContainerRoutingModule } from './panel-container-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { PanelContainerComponent } from './panel-container.component';
import { HomeComponent } from './containers/home/home.component';
import { ToursComponent } from './containers/tours/tours.component';
import { PanelLayoutComponent } from './containers/panel-layout/panel-layout.component';

@NgModule({
  imports: [
    PanelContainerRoutingModule,

/*        AgmCoreModule.forRoot({
          apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),*/

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,

        // Fuse modules
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,

        FuseWidgetModule
  ],
  providers: [],
  exports: [],
  declarations: [PanelContainerComponent, HomeComponent, PanelLayoutComponent, ToursComponent]
})
export class PanelContainerModule {
}
