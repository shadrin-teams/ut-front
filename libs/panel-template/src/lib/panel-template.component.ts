import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { UsersFacade } from '@wt/core/+state/users';

// import { UsersFacade } from '@wt/core/+state/users';

@Component({
  selector: 'wt-panel-template',
  templateUrl: './panel-template.component.html',
  styleUrls: ['./panel-template.component.css']
})
export class PanelTemplateComponent implements OnInit {

  private toasterService: ToasterService;

  constructor(private userFacade: UsersFacade,
              private _router: Router,
              toasterService: ToasterService) {
    this.toasterService = toasterService;
  }

  ngOnInit() {
  }

}
