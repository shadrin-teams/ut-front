import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { PanelTemplateComponent } from './panel-template.component';
import { FuseModule } from '@wt/fuse';
import { CoreModule } from '@wt/core';
import { ToasterModule } from 'angular2-toaster';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { fuseConfig } from '@wt/core/fuse-config';
import { AuthGuard } from './guards/auth.guard';
import { DialogsModule } from '@wt/core/dialogs/dialogs.module';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { HeadersInterceptor } from './interceptors/headers.interceptor';
import { AppConfig } from '@wt/models/config';
import { CORE_CONFIG } from '@wt/core/tokens';

@NgModule({
  imports: [
    CommonModule,

    CoreModule.forRoot(),

    MatButtonModule,
    MatCheckboxModule,

    // Fuse modules
    FuseModule.forRoot(fuseConfig),

    RouterModule.forChild([
      /* {path: '', pathMatch: 'full', component: InsertYourComponentHere} */
      {
        path: '',
        loadChildren: () => import('@wt/panel-container').then(m => m.PanelContainerModule),
        runGuardsAndResolvers: 'paramsOrQueryParamsChange',
        canActivate: [AuthGuard]
      },
      {
        path: 'auth',
        loadChildren: () => import('@wt/auth').then(m => m.AuthModule),
        runGuardsAndResolvers: 'paramsOrQueryParamsChange'
      },
      {
        path: 'images',
        loadChildren: () => import('@wt/images').then(m => m.ImagesModule),
        runGuardsAndResolvers: 'paramsOrQueryParamsChange'
      },
/*      { path: '', loadChildren: '@wt/panel-container#PanelContainerModule', canActivate: [AuthGuard] },
      { path: 'auth', loadChildren: '@wt/auth#AuthModule' },
      { path: 'images', loadChildren: '@wt/images#ImagesModule' },*/

      { path: '**', redirectTo: '' }
    ]),
    DialogsModule,
    ToasterModule.forRoot()
  ],
  exports: [MatButtonModule, MatCheckboxModule],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HeadersInterceptor, multi: true }
  ],
  declarations: [PanelTemplateComponent]
})
export class PanelTemplateModule {
  static forApp(appConfig: AppConfig): ModuleWithProviders<PanelTemplateModule> {
    return {
      ngModule: PanelTemplateModule,
      providers: [
        { provide: CORE_CONFIG, useValue: appConfig }
        //{ provide: AUTH_SERVICE_CONFIG, deps: [PLATFORM_ID], useFactory: authServiceConfigFactory },
      ]
    };
  }
}



/*export function authServiceConfigFactory(platform: Object) {
/!*  if (isPlatformBrowser(platform)) {
    return new SocialAuthServiceConfig([
      {
        id: FbLoginProvider.PROVIDER_ID,
        provider: new FbLoginProvider('296018684328219')
      }
    ]);
  } else {*!/
    return null;
  // }
}*/
