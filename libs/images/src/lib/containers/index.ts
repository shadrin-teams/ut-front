import { ImageListComponent } from '@wt/images/src/lib/containers/image-list/image-list.component';
import { ImageFormComponent } from '@wt/images/src/lib/containers/image-form/image-form.component';

export const containers = [
  ImageListComponent,
  ImageFormComponent
];
