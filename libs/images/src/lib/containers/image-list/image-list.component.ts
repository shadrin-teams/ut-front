import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';

import {ImagesFacade} from '@wt/core/+state/images/images.facade';
import {ImageFieldNames, ImageModel} from '@wt/models/image';
import {DeleteComponent} from '@wt/core/modules/dialogs/components/delete/delete.component';
import {RouterService} from '@wt/core/services/router/router.service';
import {trackById, untilComponentDestroyed} from '@wt/core/utils';
import {UiUnitIds} from '@wt/models/ui-units';
import {UiUnitsFacade} from '@wt/core/+state/uiUnits';
import {Observable} from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

/**
 * @title Data table with sorting, pagination, and filtering.
 */
@Component({
  selector: 'wt-image-list',
  styleUrls: ['image-list.component.scss'],
  templateUrl: 'image-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageListComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  dataSource: MatTableDataSource<ImageModel>;
  isLoaded = false;
  loaded$: Observable<boolean>;
  trackByFn = trackById;
  displayedColumns: string[] = ['id', 'files', 'title', 'public', 'action'];
  totalCount: number;
  perPage = 10;
  page = 1;
  searchText = '';
  fields = {
    fields: [
      ImageFieldNames.TITLE,
      ImageFieldNames.DESCRIPTION,
      ImageFieldNames.CATEGORY_ID,
      ImageFieldNames.IS_PUBLIC,
      ImageFieldNames.SLUG
    ],
    files: {
      fields: ['file', 'folder'],
      versions: {}
    }
  };

  constructor(private imagesFacade: ImagesFacade,
              public dialog: MatDialog,
              public routerService: RouterService,
              private uiUnitsFacade: UiUnitsFacade,
              private changeDetectorRef: ChangeDetectorRef,
              private router: Router
  ) {
  }

  ngOnInit() {
    /*    this.sort.sortChange.subscribe((data) => console.log('sortChange', data));
        this.paginator.page.subscribe((data) => console.log('paginator', data));*/

    this.uiUnitsFacade.getUIUnits(UiUnitIds.IMAGES)
      .pipe(
        untilComponentDestroyed(this)
      ).subscribe(data => {

      if (data) {
        this.totalCount = data.totalCount;
        this.perPage = data.perPage;
        this.page = data.page;
        this.changeDetectorRef.detectChanges();
      }
    });

    this.loaded$ = this.imagesFacade.loaded$;

    this.imagesFacade.getImages$
      .pipe(
        untilComponentDestroyed(this)
      )
      .subscribe((images: ImageModel[]) => {
        const newImages = images ? [...images] : [];
        if (images) {
          newImages
            .map(item => ({...item}))
            .map(item => {
              if (item.files) {
                item.files = [...item.files].filter((item, index) => index <= 2);
              }
              return {...item};
            });
        }

        this.dataSource = new MatTableDataSource(newImages);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.isLoaded = true;
        this.changeDetectorRef.detectChanges();
      });
  }

  ngOnDestroy(): void {
  }

  /*  selectAction(itemId: number, event: any) {
      console.log('selectAction', itemId, event.value);
      if (event.value === 'delete') {
        this.openDialog();
      }
    }*/

  openDeleteDialog(item): void {
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '250px',
      data: {name: 'name'}
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.imagesFacade.imagesDelete(item.id);
      }
    });
  }

  editEvent(item) {
    const id = item.id;
    // this.routerService.goTo(['images/form', id]);
    // this.routerService.goTo(['/images/new']);
    this.router.navigate(['images/form', id]);
  }

  deleteEvent(item) {
  }

  loadImages() {
    const where = {};
    if (this.searchText) {
      where['title'] = `*${this.searchText}*`;
    }
    this.imagesFacade.loadImages({
      where,
      fields: {...this.fields},
      paging: {
        page: this.page,
        perPage: this.perPage
      },
      forPanel: true
    });
  }

  changePage(data) {
    const {pageIndex, pageSize} = data;
    this.page = pageIndex + 1;
    this.perPage = pageSize;
    this.imagesFacade.clearImages();
    this.loadImages();
  }

  applyFilter(filterValue, clear = false) {
    if (!clear) {
      this.searchText = filterValue.value;
    } else {
      filterValue.value = '';
      this.searchText = '';
    }
    this.imagesFacade.clearImages();
    this.loadImages();
  }

  rowClick(data, test?) {
  }
}
