import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { forkJoin, of, Subject } from 'rxjs';
import { filter, first, switchMap } from 'rxjs/operators';
import { ImagesFacade } from '@wt/core/+state/images/images.facade';
import { ActivatedRoute, Params } from '@angular/router';
import { untilComponentDestroyed } from '@wt/core/utils';
import { ImageModel } from '@wt/models/image';
import { UiUnitsFacade } from '@wt/core/+state/uiUnits';
import { UiUnitIds, UiUnitsImagesData } from '@wt/models/ui-units';
import { RouterFacade } from '@wt/core/+state/router/router.facade';

// import { FormGroupState } from 'ngrx-forms';
import { CategoriesFacade } from '@wt/core/+state/categories';
import { HashtagsFacade } from '@wt/core/+state/hashtags';
// import { IFormField } from '@wt/core/components/form-field/containers/form-field-auto-form/form-field-auto-form.component';
import { IFormField } from '@wt/core/components/auto-form/interfaces/form-field';

import { HashtagModel } from '@wt/models/hashtag';
import { FieldType } from '@wt/core/components/auto-form/interfaces/field-type';
import { AutoFormService } from '@wt/core/services/auto-form/auto-form.service';
import { IFormFieldOption } from '@wt/core/components/auto-form/interfaces/form-field-option';


enum FirmGroups {
  ABOUT = 'Общие',
  DESCRIPTION = 'Описание',
  ADDITIONALLY = 'Дополнительно',
}

export const FirmFormFields: IFormField[] = [
  {
    field: 'title',
    title: 'Заголовок',
    type: FieldType.FieldTypeInput,
    require: true,
    groupName: FirmGroups.ABOUT
  },
  {
    field: 'metaTitle',
    title: 'Мета заголовок',
    type: FieldType.FieldTypeInput,
    groupName: FirmGroups.ABOUT
  },
  {
    field: 'categoryId',
    title: 'Категория',
    require: true,
    type: FieldType.FieldTypeSelect,
    groupName: FirmGroups.ABOUT
  },
  {
    field: 'tagIds',
    title: 'Теги',
    type: FieldType.FieldTypeInput,
    require: true,
    groupName: FirmGroups.ABOUT
  },
  {
    field: 'description',
    title: 'Описание',
    type: FieldType.FieldTypeTextarea,
    require: true,
    groupName: FirmGroups.DESCRIPTION
  },
  {
    field: 'descriptionFull',
    title: 'Полное описание',
    type: FieldType.FieldTypeTextareaEditor,
    require: true,
    groupName: FirmGroups.DESCRIPTION
  },
  {
    field: 'slug',
    title: 'Slug / Ключ',
    type: FieldType.FieldTypeInput,
    groupName: FirmGroups.DESCRIPTION
  },
  {
    field: 'isPublic',
    title: 'Опубликована',
    type: FieldType.FieldTypeSlideToggle,
    require: true,
    groupName: FirmGroups.DESCRIPTION
  },
  {
    field: 'metaDescription',
    title: 'Мена описание',
    type: FieldType.FieldTypeTextarea,
    groupName: FirmGroups.ADDITIONALLY
  },
  {
    field: 'metaKeyWord',
    title: 'Мета ключ. слова',
    type: FieldType.FieldTypeTextarea,
    groupName: FirmGroups.ADDITIONALLY
  }
];

@Component({
  selector: 'wt-image-form',
  templateUrl: './image-form.component.html',
  styleUrls: ['./image-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageFormComponent implements OnInit, OnDestroy {
  form: FormGroup;
  formId: string;
  fields: IFormField[] = FirmFormFields;
  imageId: number;
  image: ImageModel | {};
  isLoaded = false;
  htmlContent: any;

  // Horizontal Stepper
  horizontalStepperStep1: FormGroup;
  uiUnit$;
  editorConfig = {};
  /*editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
    customClasses: [ // optional
      {
        name: 'quote',
        class: 'quote'
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1'
      }
    ]
  };*/
  listTags: IFormFieldOption[] = [];
  listCategories: IFormFieldOption[] = [];
  //formState$: Observable<FormGroupState<FormValue>>;
  description = '';
  dopInfoLoaded = false;
  /*  initialFormData: FormValue = {
      title: '',
      categoryId: null,
      tagIds: '',
      description: '',
      descriptionFull: '',
      slug: '',
      isPublic: false,
      metaDescription: '',
      metaTitle: '',
      metaKeyWord: ''
    };*/
  // Private
  private _unsubscribeAll: Subject<any>;
  editorImageUploadUrl: string;
  dataStored: boolean;
  storedFormValues: boolean;

  /**
   * Constructor
   *
   * @param {FormBuilder} fb
   * @param {imagesFacade} imagesFacade
   * @param {route} route
   * @param {uiUnitsFacade} uiUnitsFacade
   * @param {RouterFacade} routerFacade
   * @param {ChangeDetectorRef} changeDetectorRef
   * @param {CategoriesFacade} categoriesFacade
   * @param {HashtagsFacade} hashtagsFacade
   * @param {AutoFormService} autoFormService
   */
  constructor(
    private fb: FormBuilder,
    private imagesFacade: ImagesFacade,
    private route: ActivatedRoute,
    private uiUnitsFacade: UiUnitsFacade,
    private routerFacade: RouterFacade,
    private changeDetectorRef: ChangeDetectorRef,
    // private formsFacade: FormsFacade,
    private categoriesFacade: CategoriesFacade,
    private hashtagsFacade: HashtagsFacade,
    private autoFormService: AutoFormService
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this.initForm();
    this.loadDopInfo();

    this.uiUnit$ = this.uiUnitsFacade.getUIUnits(UiUnitIds.IMAGES);

    this.uiUnitsFacade.getUIUnits(UiUnitIds.IMAGES)
      .pipe(
        untilComponentDestroyed(this)
      )
      .subscribe((data: UiUnitsImagesData) => {
        if (data && data.lastCreatedId && this.image && !this.image['id']) {
          this.image['id'] = data.lastCreatedId;
          this.routerFacade.goTo(['/images', 'form', data.lastCreatedId]);
        }
      });

    this.route.params
      .pipe(
        switchMap((params: Params) => {
          if (params && params.id) {
            return this.imagesFacade.getImage(+params.id)
              .pipe(
                filter(item => item && !!item.id)
              );
          }
          return of(null);
        }),
        switchMap((data: ImageModel) => {
          this.image = { ...data } || {};

          if (data && data['tagIds']) {
            return this.hashtagsFacade.getHashtagsList(data['tagIds'])
          }

          return of([]);
        }),
        untilComponentDestroyed(this)
      )
      .subscribe((tagsData: HashtagModel[]) => {
        if (tagsData) {
          const tagIds = Object.values(tagsData);
          if (tagIds && Array.isArray(tagIds)) {
            this.image['tagIds'] = tagIds.map(item => item.name).join(', ');
          }
        }

        if (this.image) {
          this.editorImageUploadUrl = `/image/${this.image['id'] ? this.image['id'] : 'new'}/editor`;
        }

        const formValue: any = this.image || {};
        this.form.patchValue(formValue);

        /*        Object.keys(this.initialFormData).forEach(field => {
                  if (!Array.isArray(this.initialFormData[field])) {
                    values.push({ field, value: formValue[field] });
                  } else {
                    values.push({
                      field,
                      value: formValue[field] ? formValue[field]/!*.map(item => item.id)*!/ : []
                    });
                  }
                });*/

        /*        const params: UpdateFormPayload = {
                  formId: this.formId,
                  values
                };*/

        this.isLoaded = true;
        this.changeDetectorRef.markForCheck();
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
    // this.formsFacade.removeForm(UiUnitIds.IMAGES);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  initForm() {
    this.imageId = this.route.snapshot.params.id;
    this.formId = `imageForm${this.imageId}`;

    const fields = this.autoFormService.getPreparedFields(this.fields);
    this.form = this.fb.group(fields);

    /*    this.form.valueChanges
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((data) => {
            this.dataStored = екгу;
          });*/

    /*this.fields = [
      { field: 'title', title: 'Заголовок', type: FieldTypeInput, require: true },
      { field: 'metaTitle', title: 'Мета заголовок', type: FieldTypeInput },
      {
        field: 'categoryId',
        title: 'Категория',
        type: FieldTypeSelect,
        require: true,
        options: this.listCategories,
        loading: this.dopInfoLoaded
      },
      {
        field: 'tagIds',
        title: 'Теги',
        type: FieldTagsText,
        require: true,
      },
      { field: 'description', title: 'Описание', type: FieldTypeTextarea, require: true },
      {
        field: 'descriptionFull',
        title: 'Полное описание',
        type: FieldTypeTextareaEditor,
        require: true
      },
      { field: 'slug', title: 'Slug/Ключ', type: FieldTypeInput },
      { field: 'isPublic', title: 'Опубликовать', type: FieldTypeSlideToggle },
      { field: 'metaDescription', title: 'Мета описание', type: FieldTypeTextarea },
      { field: 'metaKeyWord', title: 'Мета ключ. слова', type: FieldTypeTextarea }
    ];*/

    // this.formsFacade.createForm(UiUnitIds.IMAGE_FORM, this.initialFormData);
    // this.formState$ = this.formsFacade.getForm(UiUnitIds.IMAGE_FORM);
  }

  loadDopInfo() {
    const categories$ = this.categoriesFacade.getCategories$.pipe(first());
    const hashtags$ = this.hashtagsFacade.getHashtags$.pipe(first());

    forkJoin([categories$, hashtags$])
      .pipe(
        untilComponentDestroyed(this)
      )
      .subscribe(data => {

        if (data[0]) {
          this.listCategories = data[0].map(item => ({ id: +item.id, name: item.name }));
        }

        if (data[1]) {
          this.listTags = data[1].map(item => ({ id: +item.id, name: item.name }));
        }

        this.fields[2].options = this.listCategories;
        this.fields[3].options = this.listTags;
        this.dopInfoLoaded = true;
        this.changeDetectorRef.markForCheck();
      });
  }

  /**
   * Finish the horizontal stepper
   */
  finishHorizontalStepper(): void {
    alert('You have finished the horizontal stepper!');
  }

  formSubmit() {
    const formValues = { ...this.form.value };
    if (this.image && this.image['id']) {
      formValues['id'] = this.image['id'];
    }

    if (!this.form.invalid) {
      const params: ImageModel = {
        ...formValues
      };
      this.imagesFacade.imagesCreateOrUpdate(params);
    }
  }

  goBack() {
    this.routerFacade.goTo(['images']);
  }

}
