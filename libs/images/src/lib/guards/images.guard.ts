import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

import {ImagesFacade} from '@wt/core/+state/images/images.facade';

@Injectable()
export class ImagesGuard implements CanActivate {
  constructor(
    private imagesFacade: ImagesFacade) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.imagesFacade.loadImages({fields: this.imagesFacade.fields, forPanel: true});
    return this.imagesFacade.waitLoading();
  }
}
