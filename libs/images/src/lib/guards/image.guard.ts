import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';

import {ImagesFacade} from '@wt/core/+state/images/images.facade';
import { filter, map, tap } from 'rxjs/operators';
import {FieldsModel} from '@wt/models/fields';
import {ImageFieldNames} from '@wt/models/image';
import {FileFieldNames} from '@wt/models/file';

@Injectable()
export class ImageGuard implements CanActivate {
  imageId: number;

  constructor(
    private imagesFacade: ImagesFacade) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (route.params && route.params.id) {
      this.imageId = +route.params.id;
      const fields: FieldsModel = {
        fields: [
          ImageFieldNames.TITLE,
          ImageFieldNames.DESCRIPTION,
          ImageFieldNames.DESCRIPTION_FULL,
          ImageFieldNames.CATEGORY_ID,
          ImageFieldNames.IS_PUBLIC,
          ImageFieldNames.SLUG,
          ImageFieldNames.META_TITLE,
          ImageFieldNames.META_KEYWORD,
          ImageFieldNames.META_DESCRIPTION,
        ],
        files: {
          fields: [
            FileFieldNames.FILE,
            FileFieldNames.FOLDER,
            FileFieldNames.TITLE,
            FileFieldNames.MAIN,
            FileFieldNames.ORDER
          ],
          versions: {}
        },
        tags: {
          fields: ['name'],
          count: {}
        }
      };
      return this.imagesFacade.getLoadImage(this.imageId, fields, true)
        .pipe(
          filter(data => !!data),
          map(data => !!data)
        );
    } else {
      return of(false);
    }
  }
}
