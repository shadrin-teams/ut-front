import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable, of} from 'rxjs';
import {first, map, switchMap, tap} from 'rxjs/operators';
import {CoreState} from '@wt/core/+state/core.reducer';
import {CategoriesFacade} from '@wt/core/+state/categories';
import {CategoryFieldNames, CategoryModel} from '@wt/models/category';

@Injectable()
export class LoadCategoriesResolver implements Resolve<Observable<boolean>> {
  constructor(private store: Store<CoreState>,
              private categoriesFacade: CategoriesFacade
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.getFromStoreOrAPI();
  }

  getFromStoreOrAPI(): Observable<boolean> {
    return this.categoriesFacade.getCategories$
      .pipe(
        first(),
        switchMap((categories: CategoryModel[]) => {
          if (!categories || !categories.length) {
            this.categoriesFacade.loadCategories({}, {fields: [CategoryFieldNames.NAME, CategoryFieldNames.SLUG]}, 100 );
            return this.categoriesFacade.waitLoading();
          }
          return of(true);
        }),
        first()
      );
  }
}
