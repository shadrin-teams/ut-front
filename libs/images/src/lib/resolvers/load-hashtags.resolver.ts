import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { first, switchMap, tap } from 'rxjs/operators';
import { CoreState } from '@wt/core/+state/core.reducer';
import { HashtagsFacade } from '@wt/core/+state/hashtags';
import { HashtagFieldNames, HashtagModel } from '@wt/models/hashtag';

@Injectable()
export class LoadHashtagsResolver implements Resolve<Observable<boolean>> {
  constructor(private store: Store<CoreState>,
              private hashtagsFacade: HashtagsFacade
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.getFromStoreOrAPI();
  }

  getFromStoreOrAPI(): Observable<boolean> {
    return this.hashtagsFacade.getHashtags$
      .pipe(
        first(),
        switchMap((hashtags: HashtagModel[]) => {
          if (!hashtags || !hashtags.length) {
            this.hashtagsFacade.loadHashtags({}, { fields: [HashtagFieldNames.NAME] });
            return this.hashtagsFacade.waitLoading();
          }
          return of(true);
        }),
        first()
      );
  }
}
