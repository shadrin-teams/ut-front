import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Params, Resolve, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { FilesFacade } from '@wt/core/+state/files';
import { CoreState } from '@wt/core/+state/core.reducer';

@Injectable()
export class ImageLoadFilesResolver implements Resolve<Observable<boolean>> {
  constructor(private store: Store<CoreState>,
              private filesFacade: FilesFacade,
              private route: ActivatedRoute
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.getFromStoreOrAPI();
  }

  getFromStoreOrAPI(): Observable<boolean> {
    return this.route.params
      .pipe(
        tap((params: Params) => {
          if (params && params.id) {
            this.filesFacade.loadFiles({ itemId: params.id, section: 'image' });
          }
        }),
        switchMap(() => {
          return this.filesFacade.waitLoading();
        })
      );
  }
}
