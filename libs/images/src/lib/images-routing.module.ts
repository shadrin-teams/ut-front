import { Route, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { ImageListComponent } from '@wt/images/src/lib/containers/image-list/image-list.component';
import { ImagesGuard } from '@wt/images/src/lib/guards/images.guard';
import { ImageFormComponent } from '@wt/images/src/lib/containers/image-form/image-form.component';
import { ImageGuard } from '@wt/images/src/lib/guards/image.guard';
import { LoadCategoriesResolver } from '@wt/images/src/lib/resolvers/load-categories.resolver';
import { LoadHashtagsResolver } from '@wt/images/src/lib/resolvers/load-hashtags.resolver';

export const imageRouting: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    component: ImageListComponent,
    canActivate: [ImagesGuard]
  },
  { path: 'list', component: ImageListComponent, canActivate: [ImagesGuard] },
  {
    path: 'new',
    component: ImageFormComponent,
    resolve: {
      loadCategories: LoadCategoriesResolver,
      loadHashtagsResolver: LoadHashtagsResolver
    }
  },
  {
    path: 'form/:id',
    component: ImageFormComponent,
    canActivate: [ImageGuard],
    resolve: {
      loadCategories: LoadCategoriesResolver,
      loadHashtagsResolver: LoadHashtagsResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(imageRouting)],
  exports: [RouterModule],
  providers: [
    ImagesGuard,
    ImageGuard,
    LoadCategoriesResolver,
    LoadHashtagsResolver
  ]
})
export class ImagesRoutingModule {
}
