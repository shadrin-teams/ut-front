export interface PagingType {
  perPage: number;
  page?: number;
}
