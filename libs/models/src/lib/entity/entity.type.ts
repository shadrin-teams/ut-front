export enum EntityType {
  LESSON = 'lesson',
  STANDALONE_LESSON = 'standalone_lesson',
  COURSE = 'course',
  TOOL = 'tool'
}
