import { QueryFieldsNode } from '../query-fields-adapter';

export interface EntityQueryFields extends QueryFieldsNode {
  owner?: QueryFieldsNode;
}
