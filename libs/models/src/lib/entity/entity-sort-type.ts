export enum EntitySortType {
  LATEST = 'published_at',
  POPULAR = 'popularity',
  RELEVANCE = 'relevance',
  LATEST_CREATED = 'created_at',
}
