export class NotificationModel {
  id: number | string;
  type: 'success' | 'error';
  text: string;
  timeout?: number;
  title?: string;
}
