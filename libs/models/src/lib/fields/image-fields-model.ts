import {FieldsModel} from '@wt/models/fields/fields-model';

export interface ImageFieldsModel extends FieldsModel{
  versions: string[];
}
