import {UiUnitIds} from '@wt/models/ui-units/ui-unit-ids';
import {UiUnitTypes} from '@wt/models/ui-units/ui-unit-types';

export interface UiUnitItem {
  id: UiUnitIds;
  type?: UiUnitTypes;
  data: {
    [key: string]: any
  };
}
