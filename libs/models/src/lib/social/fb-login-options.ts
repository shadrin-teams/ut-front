import { LoginOptions } from './login-options';

export interface FbLoginOptions extends LoginOptions {
    /**
     * Facebook FB.login options: https://developers.facebook.com/docs/reference/javascript/FB.login/v3.1.
     */
    auth_type?: string; // Optional key, only supports one value: rerequest. Use this when re-requesting a declined permission.
    scope?: string; // Comma separated list of extended permissions
    return_scopes?: boolean; //When true, the granted scopes will be returned in a comma - separated list in the grantedScopes field of the authResponse
    enable_profile_selector?: boolean; // When true, prompt the user to grant permission for one or more Pages.
    profile_selector_ids?: string; // Comma separated list of IDs to display in the profile selector
}