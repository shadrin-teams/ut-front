export class HashtagModel {
  id?: number;
  name: string;
  slug: string;
  count: number;
  description?: string;
  image?: string;
}
