export interface QueryFieldsNode {
    fields: string[];
}

export class QueryFieldsAdapter {
    generateQueryFieldsString(fields: QueryFieldsNode): string {
        const result = this.getFieldsString(fields);
        return result;
    }

    private getFieldsString(fields: QueryFieldsNode): string {
        const result = [];
        for (const key in fields) {
            if (key === 'fields') {
                if (fields[key] && fields[key].length) result.push([...fields[key]].join(','))
            } else {
                result.push(`${key}(${this.getFieldsString(fields[key])})`);
            }
        }
        return result.join(',');
    }
}