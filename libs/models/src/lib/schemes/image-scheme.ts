import { mappet, Schema } from '../mapper';
import { ImageFieldNames, ImageModel } from '@wt/models/image';
import { entityScheme } from '@wt/models/schemes/entity-scheme';
import { fileMapper } from '@wt/models/schemes/file-scheme';
import {hashtagMapper} from '@wt/models/schemes/hashtag-scheme';

export const imageScheme: Schema<ImageModel> = {
  ...entityScheme,
  title: ImageFieldNames.TITLE,
  description: ImageFieldNames.DESCRIPTION,
  descriptionFull: ImageFieldNames.DESCRIPTION_FULL,
  slug: ImageFieldNames.SLUG,
  isPublic: ImageFieldNames.IS_PUBLIC,
  categoryId: ImageFieldNames.CATEGORY_ID,
  createdAt: ImageFieldNames.CREATEDAT,
  views: ImageFieldNames.VIEWS,
  metaTitle: ImageFieldNames.META_TITLE,
  metaDescription: ImageFieldNames.META_DESCRIPTION,
  metaKeyWord: ImageFieldNames.META_KEYWORD,
  files: {
    path: ImageFieldNames.FILES,
    modifier: files => files && files.length ? files.map((item) => fileMapper(item)) : []
  },
  tagIds: {
    path: ImageFieldNames.TAGS,
    modifier: tags => tags && tags.length ? tags.map(item => item.id) : []
  }
};

export const imageMapper = mappet(imageScheme);
