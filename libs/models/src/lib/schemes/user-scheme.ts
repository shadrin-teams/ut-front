import { mappet, Schema } from '../mapper';
import { entityScheme } from '@wt/models/schemes/entity-scheme';
import { UserFieldNames, UserModel } from '@wt/models/user';

export const userScheme: Schema<UserModel> = {
  ...entityScheme,
  email: 'email',
  token: UserFieldNames.TOKEN,
  tokenExp: UserFieldNames.TOKENEXP,
  refreshToken: UserFieldNames.REFRESHTOKEN,
  role: UserFieldNames.ROLE,
  lastName: UserFieldNames.LASTNAME,
  avatar: UserFieldNames.AVATAR,
  sex: UserFieldNames.SEX,
  rights: UserFieldNames.RIGHTS
};

export const userMapper = mappet(userScheme);
