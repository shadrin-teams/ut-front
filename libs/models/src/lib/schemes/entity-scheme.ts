import { Entity } from '../entity';
import { Schema } from '../mapper';

export const entityScheme: Schema<Entity> = {
  id: 'id'
};
