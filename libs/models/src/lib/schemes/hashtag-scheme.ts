import {mappet, Schema} from '../mapper';
import {entityScheme} from '@wt/models/schemes/entity-scheme';
import {HashtagFieldNames, HashtagModel} from '@wt/models/hashtag';

export const hashtagScheme: Schema<HashtagModel> = {
  ...entityScheme,
  name: HashtagFieldNames.NAME,
  slug: HashtagFieldNames.SLUG,
  description: HashtagFieldNames.DESCRIPTION,
  image: HashtagFieldNames.IMAGE,
  count: HashtagFieldNames.COUNT
};

export const hashtagMapper = mappet(hashtagScheme);
