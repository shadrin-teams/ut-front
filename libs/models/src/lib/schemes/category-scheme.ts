import {mappet, Schema} from '../mapper';
import {entityScheme} from '@wt/models/schemes/entity-scheme';
import {CategoryFieldNames, CategoryModel} from '@wt/models/category';

export const categoryScheme: Schema<CategoryModel> = {
  ...entityScheme,
  name: CategoryFieldNames.NAME,
  slug: CategoryFieldNames.SLUG,
  count: CategoryFieldNames.COUNT
};

export const categoryMapper = mappet(categoryScheme);
