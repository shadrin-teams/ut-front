import {mappet, Schema} from '../mapper';
import {entityScheme} from '@wt/models/schemes/entity-scheme';
import {FileModel} from '@wt/models/file';

export const fileScheme: Schema<FileModel> = {
  ...entityScheme,
  file: 'file',
  folder: 'folder',
  sort: 'order',
  main: 'main',
  title: 'title',
  itemId: 'item_id',
  section: 'section',
  versions: {
    path: 'versions',
    modifier: versions => {
        return versions ? {'1': versions['1'], '2': versions['2'], '3': versions['3']} : {};
    }
  }
};

export const fileMapper = mappet(fileScheme);
