import { RequestConfig } from '../../http/request/request-config';

export interface CategoriesRequestConfig extends RequestConfig { }
