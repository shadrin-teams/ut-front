import { ResponseStatus } from '../../../../http';
import { CategoriesLoadResponse } from './categories-load-response';
import { MetaModel } from '@wt/models/others';

export interface CategoriesLoad200 extends CategoriesLoadResponse {
  status: ResponseStatus.STATUS_200;

  body: {
    data: any;
    meta?: MetaModel
  };

}
