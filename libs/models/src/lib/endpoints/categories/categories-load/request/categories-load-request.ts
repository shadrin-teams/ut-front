import {GetRequest} from '../../../../http';
import {CategoriesLoadRequestConfig} from './categories-load-request-config';

export class CategoriesLoadRequest extends GetRequest {
  constructor(protected domain: string, private config: CategoriesLoadRequestConfig) {
    super(domain);
  }

  get url() {
    const where = this.convertWHereToString(this.config.where);
    const fields = this.convertFieldsToString(this.config.fields);
    const paging = this.convertPagingToString({perPage: this.config.perPage});
    return `${this.domain}/api/categories/?${where}${where ? '&' : ''}${fields}${where || fields ? '&' + paging : ''}`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
