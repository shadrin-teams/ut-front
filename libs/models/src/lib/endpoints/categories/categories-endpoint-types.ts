export enum CategoriesEndpointTypes {
    CREATE = 'create',
    DELETE = 'delete',
    LOAD = 'load',
    GET = 'get',
    UPDATE = 'update'
}
