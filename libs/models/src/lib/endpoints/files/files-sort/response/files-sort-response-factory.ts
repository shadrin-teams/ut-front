import { ResponseStatus } from '../../../../http';
import { FilesResponse } from '../../files-response';
import { FilesSortResponse } from './files-sort-response';
import { FilesSort200 } from './files-sort-200';

export class FilesSortResponseFactory {
    static createResponse(httpResponse: any): FilesResponse {
        let response: FilesResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_200:
                const response201: FilesSort200 = {
                    status: ResponseStatus.STATUS_200,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as FilesSortResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
