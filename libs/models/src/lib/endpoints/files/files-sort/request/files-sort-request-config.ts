import { UserAuthHeaders } from '../../../../user';
import { FilesRequestConfig } from '@wt/models/endpoints';

export interface FilesSortRequestConfig extends FilesRequestConfig {
  files: {id: number, order: number}[];
  headers: UserAuthHeaders;
}
