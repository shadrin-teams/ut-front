import { UserAuthHeaders } from '../../../../user';
import { FilesRequestConfig } from '@wt/models/endpoints';

export interface FilesUpdateRequestConfig extends FilesRequestConfig {
  id: number;
  title?: string;
  sort?: number;
  main?: boolean;
  section: string;
  itemId: number;
  headers: UserAuthHeaders;
}
