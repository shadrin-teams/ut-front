import { ResponseStatus } from '../../../../http';
import { FilesUpdateResponse } from './files-update-response';

export interface FilesUpdate200 extends FilesUpdateResponse {
  status: ResponseStatus.STATUS_200;

  body: {
    data: any;
  };
}
