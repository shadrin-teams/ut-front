import { ResponseStatus } from '../../../../http';
import { FilesLoadResponse } from './files-load-response';

export interface FilesLoad200 extends FilesLoadResponse {
  status: ResponseStatus.STATUS_200;

  body: {
    data: any;
  };
}
