import { FilesResponse } from '../../files-response';

export interface FilesLoadResponse extends FilesResponse { }
