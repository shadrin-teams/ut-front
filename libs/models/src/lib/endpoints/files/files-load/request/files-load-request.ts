import { GetRequest } from '../../../../http';
import { FilesLoadRequestConfig } from './files-load-request-config';

export class FilesLoadRequest extends GetRequest {
  constructor(protected domain: string, private config: FilesLoadRequestConfig) {
    super(domain);
  }

  get url() {
    const fields = this.convertFieldsToString(this.config.fields);
    return `${this.domain}/api/files/${this.config.section}/${this.config.itemId}?${fields}`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
