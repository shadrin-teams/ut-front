import { UserAuthHeaders } from '../../../../user';
import { FilesRequestConfig } from '@wt/models/endpoints';
import {FieldsModel} from '@wt/models/fields';

export interface FilesLoadRequestConfig extends FilesRequestConfig {
  section: string;
  itemId: number;
  headers: UserAuthHeaders;
  fields?: FieldsModel;
}
