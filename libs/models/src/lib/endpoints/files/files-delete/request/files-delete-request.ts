import { DeleteRequest } from '../../../../http';
import { FilesDeleteRequestConfig } from './files-delete-request-config';

export class FilesDeleteRequest extends DeleteRequest {
  constructor(protected domain: string, private config: FilesDeleteRequestConfig) {
    super(domain);
  }

  get url() {
    return `${this.domain}/api/files/${this.config.id}`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
