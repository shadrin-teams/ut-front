import { FilesResponse } from '../../files-response';

export interface FilesDeleteResponse extends FilesResponse { }
