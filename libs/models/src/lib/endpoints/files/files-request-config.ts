import { RequestConfig } from '../../http/request/request-config';

export interface FilesRequestConfig extends RequestConfig { }
