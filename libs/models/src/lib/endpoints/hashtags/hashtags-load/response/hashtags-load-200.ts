import { ResponseStatus } from '../../../../http';
import { HashtagsLoadResponse } from './hashtags-load-response';
import { MetaModel } from '@wt/models/others';

export interface HashtagsLoad200 extends HashtagsLoadResponse {
  status: ResponseStatus.STATUS_200;

  body: {
    data: any;
    meta?: MetaModel
  };

}
