import { ResponseStatus } from '../../../../http';
import { HashtagsResponse } from '../../hashtags-response';
import { HashtagsLoadResponse } from './hashtags-load-response';
import { HashtagsLoad200 } from './hashtags-load-200';

export class HashtagsLoadResponseFactory {
    static createResponse(httpResponse: any): HashtagsResponse {
        let response: HashtagsResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_200:
                const response201: HashtagsLoad200 = {
                    status: ResponseStatus.STATUS_200,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as HashtagsLoadResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
