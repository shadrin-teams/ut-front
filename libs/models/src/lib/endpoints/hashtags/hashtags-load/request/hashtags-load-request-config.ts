import {UserAuthHeaders} from '../../../../user';
import {HashtagsRequestConfig} from '../../../hashtags/hashtags-request-config';

export interface HashtagsLoadRequestConfig extends HashtagsRequestConfig {
  where: any;
  fields: any;
  perPage: number;
  headers: UserAuthHeaders;
}
