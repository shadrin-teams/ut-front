export enum HashtagsEndpointTypes {
    CREATE = 'create',
    DELETE = 'delete',
    LOAD = 'load',
    GET = 'get',
    UPDATE = 'update'
}
