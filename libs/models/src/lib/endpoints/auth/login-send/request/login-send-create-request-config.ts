import { AuthRequestConfig } from '../../auth-request-config';

export interface LoginSendCreateRequestConfig extends AuthRequestConfig {
  email: string;
  password: string;
}
