import { AuthResponse } from '../../auth-response';

export interface LoginSendCreateResponse extends AuthResponse { }
