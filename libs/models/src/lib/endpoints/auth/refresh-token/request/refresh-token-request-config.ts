import { AuthRequestConfig } from '../../auth-request-config';

export interface RefreshTokenRequestConfig extends AuthRequestConfig {
  refreshToken: string;
}
