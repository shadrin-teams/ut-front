import { ResponseStatus } from '../../../../http';
import { RefreshTokenResponse } from './refresh-token-response';

export interface RefreshToken200 extends RefreshTokenResponse {
    status: ResponseStatus.STATUS_201;

    body: {
        user: any;
    };
}
