import { RequestConfig } from '../../http/request/request-config';

export interface ImagesRequestConfig extends RequestConfig { }
