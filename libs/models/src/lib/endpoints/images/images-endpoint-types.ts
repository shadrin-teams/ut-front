export enum ImagesEndpointTypes {
  CREATE = 'create',
  DELETE = 'delete',
  LOAD = 'load',
  GET = 'get',
  GETNEXT = 'get-next',
  UPDATE = 'update'
}
