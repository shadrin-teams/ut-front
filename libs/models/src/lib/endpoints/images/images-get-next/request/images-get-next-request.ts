import { GetRequest } from '../../../../http';
import { ImagesGetNextRequestConfig } from './images-get-next-request-config';

export class ImagesGetNextRequest extends GetRequest {
  constructor(protected domain: string, private config: ImagesGetNextRequestConfig) {
    super(domain);
  }

  get url() {
    const fields = this.convertFieldsToString(this.config.fields);
    return `${this.domain}/api/images/${this.config.id}/next-images?${fields}`;
  }

  get headers() {
    return null;
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
