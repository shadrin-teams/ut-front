import {ResponseStatus} from '../../../../http';
import {ImagesResponse} from '../../images-response';
import {ImagesGetNextResponse} from './images-get-next-response';
import {ImagesGetNext200} from './images-get-next-200';

export class ImagesGetNextResponseFactory {
  static createResponse(httpResponse: any): ImagesResponse {
    let response: ImagesResponse;
    switch (httpResponse.status) {
      case ResponseStatus.STATUS_200:
        const response201: ImagesGetNext200 = {
          status: ResponseStatus.STATUS_200,
          body: {...httpResponse.body}
        };
        response = response201;
        break;
      case ResponseStatus.STATUS_422:
      case ResponseStatus.STATUS_400:
      case ResponseStatus.STATUS_404:
        response = {
          status: httpResponse.status,
          body: httpResponse.error
        } as ImagesGetNextResponse;
        break;
      default:
        response = null;
        break;

    }
    return response;
  }
}
