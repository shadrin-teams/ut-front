import { DeleteRequest } from '../../../../http';
import { ImagesDeleteRequestConfig } from './images-delete-request-config';

export class ImagesDeleteRequest extends DeleteRequest {
  constructor(protected domain: string, private config: ImagesDeleteRequestConfig) {
    super(domain);
  }

  get url() {
    const fields = this.convertFieldsToString(this.config.fields);
    const paging = this.convertPagingToString(this.config.paging);
    return `${this.domain}/api/images/${this.config.id}?${fields}${fields ? '&' : ''}${paging}`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
