import { ResponseStatus } from '../../../../http';
import { ImagesDeleteResponse } from './images-delete-response';

export interface ImagesDelete200 extends ImagesDeleteResponse {
    status: ResponseStatus.STATUS_200;

    body: {
        data: any;
    };
}
