import { GetRequest } from '../../../../http';
import { ImagesGetRequestConfig } from './images-get-request-config';

export class ImagesGetRequest extends GetRequest {
  constructor(protected domain: string, private config: ImagesGetRequestConfig) {
    super(domain);
  }

  get url() {
    const where = this.convertWHereToString(this.config.where);
    const fields = this.convertFieldsToString(this.config.fields);

    const apiDopAction = this.config.forPanel ? '/panel' : '';

    return `${this.domain}/api/images${apiDopAction}/${this.config.id}/?${where}${where ? '&' : ''}${fields}`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
