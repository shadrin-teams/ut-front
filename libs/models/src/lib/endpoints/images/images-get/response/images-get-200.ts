import { ResponseStatus } from '../../../../http';
import { ImagesGetResponse } from './images-get-response';

export interface ImagesGet200 extends ImagesGetResponse {
    status: ResponseStatus.STATUS_200;

    body: {
        data: any;
    };
}
