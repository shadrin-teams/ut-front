import { ResponseStatus } from '../../../../http';
import { ImagesResponse } from '../../images-response';
import { ImagesGetResponse } from './images-get-response';
import { ImagesGet200 } from './images-get-200';

export class ImagesGetResponseFactory {
    static createResponse(httpResponse: any): ImagesResponse {
        let response: ImagesResponse;
        switch (httpResponse.status) {
            case ResponseStatus.STATUS_200:
                const response201: ImagesGet200 = {
                    status: ResponseStatus.STATUS_200,
                    body: { ...httpResponse.body }
                };
                response = response201;
                break;
            case ResponseStatus.STATUS_422:
            case ResponseStatus.STATUS_400:
            case ResponseStatus.STATUS_404:
                response = {
                    status: httpResponse.status,
                    body: httpResponse.error
                } as ImagesGetResponse;
                break;
            default:
                response = null;
                break;

        }
        return response;
    }
}
