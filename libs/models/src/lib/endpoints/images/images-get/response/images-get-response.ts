import { ImagesResponse } from '../../images-response';

export interface ImagesGetResponse extends ImagesResponse { }
