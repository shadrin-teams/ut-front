import { ResponseStatus } from '../../../../http';
import { ImagesUpdateResponse } from './images-update-response';

export interface ImagesUpdate200 extends ImagesUpdateResponse {
    status: ResponseStatus.STATUS_200;

    body: {
        data: any;
    };
}
