import { ImagesResponse } from '../../images-response';

export interface ImagesUpdateResponse extends ImagesResponse { }
