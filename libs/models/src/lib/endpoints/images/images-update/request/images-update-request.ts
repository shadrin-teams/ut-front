import { PostRequest, PutRequest } from '../../../../http';
import { ImagesUpdateRequestConfig } from './images-update-request-config';

export class ImagesUpdateRequest extends PutRequest {
  constructor(protected domain: string, private config: ImagesUpdateRequestConfig) {
    super(domain);
  }

  get url() {
    return `${this.domain}/api/images/${this.config.id}`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    const {
      id,
      title,
      description,
      descriptionFull,
      slug,
      isPublic,
      categoryId,
      tagIds,
      metaDescription,
      metaKeyWord,
      metaTitle
    } = this.config;
    const comment = {
      id,
      title,
      description,
      description_full: descriptionFull,
      slug,
      tags: tagIds,
      category_id: categoryId,
      is_public: isPublic,
      meta_description: metaDescription,
      meta_keyword: metaKeyWord,
      meta_title: metaTitle,
    };
    return { ...this.cleanParams(comment) };
  }
}
