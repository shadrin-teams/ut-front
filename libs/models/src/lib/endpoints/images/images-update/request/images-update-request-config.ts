import {UserAuthHeaders} from '../../../../user';
import {ImagesRequestConfig} from '../../../images/images-request-config';
import {ImageModel} from '@wt/models/image';

export interface ImagesUpdateRequestConfig extends ImageModel {
  headers: UserAuthHeaders;
}
