import { ImageModel } from '@wt/models/image';
import { UserAuthHeaders } from '../../../../user';

export interface ImagesCreateRequestConfig extends ImageModel {
  headers: UserAuthHeaders;
}
