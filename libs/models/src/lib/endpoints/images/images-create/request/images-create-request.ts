import { PostRequest } from '../../../../http';
import { ImagesCreateRequestConfig } from './images-create-request-config';

export class ImagesCreateRequest extends PostRequest {
  constructor(protected domain: string, private config: ImagesCreateRequestConfig) {
    super(domain);
  }

  get url() {
    return `${this.domain}/api/images`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    const {
      title,
      description,
      descriptionFull,
      slug,
      isPublic,
      categoryId,
      tagIds,
      metaDescription,
      metaKeyWord,
      metaTitle
    } = this.config;
    const comment = {
      title,
      description,
      description_full: descriptionFull,
      slug,
      tags: tagIds,
      category_id: categoryId,
      is_public: isPublic,
      meta_description: metaDescription,
      meta_keyword: metaKeyWord,
      meta_title: metaTitle,
    };
    return { ...this.cleanParams(comment) };
  }
}
