import { ImagesResponse } from '../../images-response';

export interface ImagesCreateResponse extends ImagesResponse { }
