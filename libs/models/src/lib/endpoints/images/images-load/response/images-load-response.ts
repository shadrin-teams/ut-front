import { ImagesResponse } from '../../images-response';

export interface ImagesLoadResponse extends ImagesResponse { }
