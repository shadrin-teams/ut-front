import {UserAuthHeaders} from '../../../../user';
import {ImagesRequestConfig} from '../../../images/images-request-config';
import {PagingType} from '@wt/models/entity';

export interface ImagesLoadRequestConfig extends ImagesRequestConfig {
  where: any;
  fields: any;
  paging?: PagingType;
  headers: UserAuthHeaders;
  sort?: string;
  tagId?: number;
  forPanel?: boolean;
}
