import {GetRequest} from '../../../../http';
import {ImagesLoadRequestConfig} from './images-load-request-config';

export class ImagesLoadRequest extends GetRequest {
  constructor(protected domain: string, private config: ImagesLoadRequestConfig) {
    super(domain);
  }

  get url() {
    const where = this.convertWHereToString(this.config.where);
    const fields = this.convertFieldsToString(this.config.fields);
    const paging = this.convertPagingToString(this.config.paging);
    let url = where;
    if (fields) {
      url += `${url ? '&' : ''}${fields}`;
    }
    if (paging) {
      url += `${url ? '&' : ''}${paging}`;
    }
    if (this.config.sort) {
      url += `${url ? '&' : ''}sort=${this.config.sort}`;
    }
    if (this.config.tagId) {
      url += `${url ? '&' : ''}tag_id=${this.config.tagId}`;
    }

    const apiDopAction = this.config.forPanel ? '/panel' : '';

    return `${this.domain}/api/images${apiDopAction}/${url ? '?' : '' }${url}`;
  }

  get headers() {
    return this.addAuthHeaders(this.config.headers);
  }

  get params() {
    return null;
  }

  get body() {
    return null;
  }
}
