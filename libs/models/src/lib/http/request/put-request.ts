import { Request } from './request';
import { RequestMethod } from './request-method';

export abstract class PutRequest extends Request {
    httpMethod = RequestMethod.PUT;

    abstract get body(): any;
}
