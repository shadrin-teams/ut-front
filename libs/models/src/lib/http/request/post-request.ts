import { Request } from './request';
import { RequestMethod } from './request-method';

export abstract class PostRequest extends Request {
  httpMethod = RequestMethod.POST;

  abstract get body(): any;
}
