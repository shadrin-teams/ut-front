import {CategoryModel} from '@wt/models/category';
import {ImageModel} from '@wt/models/image/imageModel';
import {HashtagModel} from '@wt/models/hashtag';

export class ImageFullModel extends ImageModel {
  category?: CategoryModel;
  tags?: HashtagModel[];
  prevImage?: ImageFullModel;
  nextImage?: ImageFullModel;
}
