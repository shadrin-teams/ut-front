export interface UserModelData {
  id: string;
  token: string;
  token_exp: number;
  refresh_token: string;
  email: string;
  name: string;
  username: string;
  profileId: string;
}
