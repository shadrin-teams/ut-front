export interface CategoryModelData {
  id: string;
  name: string;
}
