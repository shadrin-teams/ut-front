export enum CategoryFieldNames {
  NAME = 'name',
  SLUG = 'slug',
  ID = 'id',
  COUNT = 'count',
  DESCRIPTION = 'description',
  IMAGE = 'image'
}
