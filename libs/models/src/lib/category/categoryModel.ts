export class CategoryModel {
  id?: number;
  slug: string;
  name: string;
  count?: number;
  description?: string;
  image?: string;
}
