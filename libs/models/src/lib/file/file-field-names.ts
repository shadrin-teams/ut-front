export enum FileFieldNames {
  FILE = 'file',
  FOLDER = 'folder',
  TITLE = 'title',
  MAIN = 'main',
  ORDER = 'order'
}
