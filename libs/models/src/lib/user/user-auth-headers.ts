export interface UserAuthHeaders {
  uid: string;
  token: string;
}
