export enum UserFieldNames {
  TOKEN = 'token',
  TOKENEXP = 'token_exp',
  REFRESHTOKEN = 'refresh_token',
  EMAIL = 'emmail',
  NAME = 'name',
  LASTNAME = 'last_name',
  AVATAR = 'avatar',
  SEX = 'sex',
  RIGHTS = 'rights',
  ROLE = 'role',
}
