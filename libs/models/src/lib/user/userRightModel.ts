export class UserRightModel {
  block: string;
  create: boolean;
  read: boolean;
  update: boolean;
  delete: boolean;
}
