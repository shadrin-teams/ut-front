export enum UserRole {
  GUEST = 'guest',
  ADMIN = 'admin',
  INSTRUCTOR = 'instructor',
  MODERATOR = 'moderator',
  STUDENT = 'student'
}
