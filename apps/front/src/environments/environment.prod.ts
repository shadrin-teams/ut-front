export const environment = {
  production: true,
  isServer: false,
  dev: true,
  publicUrl: 'http://uzbekistan-tours.ru:3005/',
  apiUrl: 'http://uzbekistan-tours.ru:3005'
};
