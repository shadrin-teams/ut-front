export const environment = {
  production: false,
  isServer: false,
  dev: true,
  publicUrl:'http://localhost:4200',
  apiUrl:'http://localhost:3005'
};
