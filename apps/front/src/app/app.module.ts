import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RouterModule } from '@angular/router';
import { WtWebTemplateComponent, WtWebTemplateModule } from '@wt/wt-web-template';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';

console.log('front-environment', environment);

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({ appId: 'wt-front' }),
    HttpClientModule,
    RouterModule.forRoot(
      [{ path: '**', component: WtWebTemplateComponent, pathMatch: 'full' }],
      { initialNavigation: 'enabled' }
    ),
    WtWebTemplateModule.forApp({
      publicUrl: environment.publicUrl,
      apiUrl: environment.apiUrl
    })
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '' } // {medium: 1200, large: 1400, url: ''}
    /*{provide: 'LOCALSTORAGE', useFactory: getLocalStorage}*/
  ]
})
export class AppModule {
}

/*export function getLocalStorage() {
  return (typeof window !== 'undefined') ? window.localStorage : null;
}*/
