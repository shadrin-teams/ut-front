import { NgModule } from '@angular/core';
//import { NxModule } from '@nrwl/nx';
import 'hammerjs';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ToasterModule } from 'angular2-toaster';
import { FileUploadModule } from 'ng2-file-upload';
// import { SortablejsModule } from 'angular-sortablejs';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { FuseModule, FuseSharedModule } from '@wt/fuse';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { PanelTemplateModule } from '@wt/panel-template';
// import { CoreModule } from '@wt/core';
import {environment} from '../environments/environment';
import { FakeDbService } from '@wt/core/fuse-fake-db/fake-db.service';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { fuseConfig } from '@wt/core/fuse-config';
import { PanelTemplateComponent } from '../../../../libs/panel-template/src/lib/panel-template.component';
import { LayoutModule } from '@wt/core/layout/layout.module';

console.log('panel-environment', environment);

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    PanelTemplateModule.forApp({
      publicUrl: environment.publicUrl,
      apiUrl: environment.apiUrl
    }),
    HttpClientModule,
    // NxModule.forRoot(),
    RouterModule.forRoot(
      [{ path: '**', component: PanelTemplateComponent, pathMatch: 'full' }],
      { initialNavigation: 'enabled' }
    ),

    TranslateModule.forRoot(),
    InMemoryWebApiModule.forRoot(FakeDbService, {
      delay: 0,
      passThruUnknownUrl: true
    }),

    // !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production // Restrict extension to log-only mode
    }),

    // Material moment date module
    MatMomentDateModule,

    // Material
    MatButtonModule,
    MatIconModule,

    // Fuse modules
    FuseModule.forRoot(fuseConfig),
    FuseProgressBarModule,
    FuseSharedModule,
    FuseSidebarModule,
    FuseThemeOptionsModule,

    // App modules
    LayoutModule,

    ToasterModule.forRoot(),
    FileUploadModule,
    // SortablejsModule.forRoot({ animation: 150 }),
    // CoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
