module.exports = {
  projects: [
    '<rootDir>/apps/front',
    '<rootDir>/libs/auth',
    '<rootDir>/libs/core',
    '<rootDir>/libs/fuse',
    '<rootDir>/libs/images',
    '<rootDir>/libs/models',
    '<rootDir>/libs/panel-container',
    '<rootDir>/libs/panel-template',
    '<rootDir>/libs/web-image',
    '<rootDir>/libs/web-template',
    '<rootDir>/libs/wt-web-template',
    '<rootDir>/apps/panel',
  ],
};
